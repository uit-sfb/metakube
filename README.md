**This project was discontinued** due to some technical issues with the workflow orchestrator.
The replacement project based on [Nextflow](https://www.nextflow.io/) can be found [here](https://gitlab.com/uit-sfb/metapipe).  
Note that while Metakube attempted to integrate workflow execution, dataset storing, job submission and status delivery via a web interface,
[Metapipe](https://gitlab.com/uit-sfb/metapipe) is only an implementation of the metagenomic pipeline itself and it is up to the user to provision the necessary
computing resources and services.

# Metakube

[![Documentation Status](https://readthedocs.org/projects/metakube/badge/?version=latest)](https://metakube.readthedocs.io/en/latest/?badge=latest)

Metakube is a generic pipeline executor over [Kubernetes](https://kubernetes.io/).
It's objective is to increase dramatically the lifecycle of development of complex workflow, from design and prototyping
straight into production on the cloud, local computing facility or simply your laptop.

Using Kubernetes as container orchestrator, leverage the power of any computer cluster
to execute your workflow, without the need to deal with the complexity of setting up the cluster.
In fact deploying a Metakube workflows is as simply as running a binary on your laptop!

## Features

## Getting started

## Documentation

[official documentation](https://metakube.readthedocs.io/en/latest/)


## Metakube workflows

It is easy to design a Metakube workflow.
The [Metakube workflow specification](doc/WorkflowSpec.md) along
with the included [example workflow](workflowExample) are useful to get started.

Some known implementations of Metakube workflows are:

  - [META-pipe](https://https://gitlab.com/uit-sfb/mk-metapipe-logic): a workflow for annotation and analysis of metagenomic samples, providing insight into phylogenetic diversity, as well as
metabolic and functional properties of environmental communities.

