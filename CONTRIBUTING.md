# Contributing guidelines

## Filing issues

If you have a question or have a problem using METAkube, please create an [issue](https://gitlab.com/uit-sfb/metakube/issues).


## Submitting patches (pull requests)

We'd love to accept your patches!


## Technical details

To learn more about implementation details and guidelines, please visit our [Wiki](https://gitlab.com/uit-sfb/metakube/wikis/Contributing).

## CI/CD

Gitlab-ci is heavily used in order to test, build and deploy automatically.
Git commit messages and tags can be used to trigger some actions as described below.

### Versioning

The project target version is the version towards which the project is moving to.
This version is defined in `targetVersion`. It should contain only one line `x.y.z` where
x, y and z are integers.

The project version is derived from the target version + the branch the commit was performed from (ignored if it is `master`) OR the tag that triggered the build + "-SNAPSHOT". For example if `targetVersion` contains `1.2.3` and the branch is `someBranch`, the project version will be `1.2.3-someBranch-SNAPSHOT`.  
The exception is if the tag triggering the build is a valid version number (`X.Y.Z`), then:
  - `targetVersion` MUST be incremented priory pushing the tag
  - the version is `X.Y.Z`.

### Test & Build

Each commit on any branch or tag pushed will trigger a full test/build pipeline.
The commit message can be used to filter out some stages:
  - by default the whole pipeline is executed,
  - if the commit message contains `{abc}{def}`, only sub-projects `abc` and `def` will be tested/built,
  - if the commit message contains `{}`, no sub-project will be tested/built,
  - the integration test is always performed except if the commit message contains `{!IT}`.

When descibing changes brought by a commit it is recommended to start a section with `[subProjectName]` to have information about which sub-project they refer to.

### Deployments

TBA
