# Metakube workflow example

This simple workflow is an example aiming at demonstrating how to implement a
Metakube workflow.

This directory contains the following elements:
  - [workflow.yaml](workflow.yaml): a workflow definition file
  - [templates directory](templates): a directory containing the pipeline template files.
  Each template defines one stage in the workflow.
  - [profiles directory](profiles): a directory containing the workflow profiles.
  A profile is a collection of input parameters that form a coherent context.
  - [pathways directory](pathways): a directory containing the workflow pathways.
  A pathway defines the primary inputs of the workflow (which will always be files).
  - [assets directory](assets): a optional directory containing companion files (such as logo, images, ..).
  Currently the only way to access those files is via their url once uploaded to git repository.
  - [wrappers](wrappers): a directory containing Metakube docker image definition wrappers.
  - [build.sh](build.sh): a script used to build the Helm chart.