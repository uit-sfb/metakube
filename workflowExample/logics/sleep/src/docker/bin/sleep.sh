#!/bin/bash

set -e

IN=""
OUT=""
POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
    -h|--help)
      echo "Dummy stage"
      echo ""
      echo "Usage"
      echo "--duration <sleep_in_seconds> --fail <true|false> --in <input_file> --out <output_file>"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
    --in)
      IN=$2
      shift
      shift
      ;;
    --out)
      OUT=$2
      shift
      shift
      ;;
    --duration)
      echo "Sleeping $2 seconds..."
      sleep "$2"
      shift
      shift
      ;;
    --fail)
      if [[ "$2" == true ]]; then ls notExistingFile; fi
      shift
      shift
      ;;
    *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

cp "$IN" "$OUT"
