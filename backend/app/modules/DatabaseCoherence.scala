package modules

import akka.Done
import akka.actor.{ActorSystem, Cancellable, CoordinatedShutdown}
import com.mongodb.client.model.Filters
import play.api.inject.SimpleModule
import play.api.inject._
import javax.inject._
import models.{AnalysisEntry, AnalysisState, StateController}
import no.uit.sfb.pachyderm.PachydermDriver.UnreachableException
import no.uit.sfb.scalautils.common.FutureUtils
import play.api.{Configuration, Logging}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import collection.JavaConverters._

class DatabaseCoherence
    extends SimpleModule(bind[CodeBlockTask].toSelf.eagerly())

@Singleton
class CodeBlockTask @Inject()(
    actorSystem: ActorSystem,
    mongodb: MongodbClient,
    as: AnalysisSupervisor,
    config: Configuration,
    mc: MetakubeClient,
    cs: CoordinatedShutdown)(implicit executionContext: ExecutionContext)
    extends Logging {
  private val stateController = StateController(mongodb)

  //Database coherence
  val coherenceScheduler: Cancellable = actorSystem.scheduler
    .schedule(initialDelay = 10.seconds, interval = 10.seconds) {
      try {
        //Todo: Cache Pachyderm data?
        //For each entry having a validity timestamp, we change the state if it expired
        val ts = System.currentTimeMillis()
        mongodb
          .collectAll[Option[AnalysisEntry]](
            mongodb.analyses,
            Filters.not(Filters.eq("validity", 0L)), {
              case doc =>
                if (ts > doc.getLong("validity"))
                  Some(mongodb.as[AnalysisEntry](doc))
                else
                  None
            }) map {
          _.flatten foreach { ae =>
            stateController.switchTo(
              ae.aid,
              ae.state.failedState.getOrElse(AnalysisState.Unknown))
          }
        }
        //Foreach analysis in a state backed by Pachyderm, we check if Pachyderm has any knowledge about them. If so we do nothing, otherwise we change to DataLost state
        mongodb
          .collectAll[String](mongodb.analyses,
                              Filters.in("status",
                                         AnalysisState
                                           .matching(_.isBackedByPachyderm)
                                           .map { _.name }
                                           .asJava), {
                                case doc => doc.getString("aid")
                              }) map { aids =>
          aids map { aid =>
            mc.isFailed(aid) recover {
              case UnreachableException() => //Pachyderm is not reachable. Do nothing
              case _ =>
                stateController.switchTo(aid, AnalysisState.DataLost)
            }
          } //Just to know if pachyderm is reachable and wid known from Pachyderm
        }
        //Foreach analysis in Running state, check if failed
        val fFailed: Future[Seq[String]] = mongodb.collectAll[String](
          mongodb.analyses,
          Filters.in("status", AnalysisState.Running.name), {
            case doc => doc.getString("aid")
          }) flatMap { aids =>
          FutureUtils.sequenceSuccessful(aids map { aid =>
            mc.isFailed(aid) map { failed =>
              (aid, failed)
            }
          }) map {
            _ collect { case (aid, true) => aid }
          }
        }
        fFailed map {
          _ foreach { aid =>
            /*mongodb.update(mongodb.analyses,
                           "aid",
                           aid,
                           Map(
                             "status" -> AnalysisState.ExecutionFailed.name,
                             "tsEnd" -> Some(System.currentTimeMillis())
                           ))*/
            mongodb.update[AnalysisEntry](
              mongodb.analyses,
              "aid",
              aid,
              t =>
                t.copy(status = AnalysisState.ExecutionFailed.name,
                       tsEnd = Some(System.currentTimeMillis())))
          }
        }
      } catch {
        case e: Throwable =>
          logger.warn("Database coherence task failed.", e)
      }
    }

  //Submit ready analyses
  val submitScheduler = actorSystem.scheduler
    .schedule(initialDelay = 10.seconds, interval = 10.seconds) {
      try {
        mongodb.count(
          mongodb.analyses,
          Filters.in("status", AnalysisState.Initializing.name)) flatMap {
          nbInitializing =>
            //If there is already one in Initialized state, we do nothing. Otherwise...
            if (nbInitializing <= 0) {
              mongodb.count(
                mongodb.analyses,
                Filters.in("status", AnalysisState.Running.name)) map {
                nbRunning =>
                  //We only add a new analysis if the number of running analyses is less than maxJobs
                  val maxJobs = config.get[Int]("app.pipeline.maxJobs")
                  if (maxJobs == 0 || nbRunning < maxJobs) {
                    mongodb.collectAll[AnalysisEntry](
                      mongodb.analyses,
                      Filters.in("status", AnalysisState.Queued.name), {
                        case doc => mongodb.as[AnalysisEntry](doc)
                      }) map { entries =>
                      //We submit only one of them
                      val oOldest =
                        entries.foldLeft[Option[AnalysisEntry]](None)(
                          (acc, v) => {
                            val previousMin =
                              acc.map(_.tsStart).getOrElse(Long.MaxValue)
                            if (previousMin <= v.tsStart)
                              acc
                            else
                              Some(v)
                          })
                      oOldest match {
                        case Some(oldest) =>
                          as.activate(oldest)
                        case None =>
                          Future.successful(())
                      }
                    }
                  }
              }
            } else
              Future.successful(())
        }
      } catch {
        case e: Throwable =>
          logger.warn("Submission task failed.", e)
      }
    }

  cs.addTask(CoordinatedShutdown.PhaseServiceRequestsDone,
             "Close databaseCoherence") { () =>
    Future.successful({
      coherenceScheduler.cancel()
      submitScheduler.cancel()
      Done
    })
  }
}
