package modules

import java.net.{URI, URL}
import java.nio.file.{Path, Paths}

import akka.Done
import akka.actor.CoordinatedShutdown
import javax.inject.{Inject, _}
import models.Analysis
import models.Analysis.AnalysisId
import models.auth.User
import models.Dataset.DatasetId
import no.uit.sfb.s3store.MinioS3SingleBucketClient
import play.api.{Configuration, Logging}
import play.api.inject._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class DatasetFactory @Inject()(config: Configuration, cs: CoordinatedShutdown)(
    implicit executionContext: ExecutionContext)
    extends Logging {

  private lazy val rawEndpoint = config.getOptional[String]("app.s3.endpoint").getOrElse("localhost")
  lazy val endpoint =
    if (new URI(rawEndpoint).getAuthority == null) s"http://$rawEndpoint"
    else rawEndpoint
  private lazy val bucketName = config.getOptional[String]("app.s3.bucket").getOrElse("bucket")

  logger.info(s"S3 client started: $endpoint")

  lazy val s3Client =
    MinioS3SingleBucketClient(
      new URL(endpoint),
      config.getOptional[String]("app.s3.accessKey").getOrElse("user"),
      config.getOptional[String]("app.s3.secretKey").getOrElse("changeme"),
      bucketName,
      true
    )

  cs.addTask(CoordinatedShutdown.PhaseServiceStop, "Close connection to S3") {
    () =>
      Future.successful(Done)
  }

  def check(): Future[Unit] = {
    Future.successful() flatMap { _ =>
      //Because s3Client can throw if not ready
        s3Client.listObjects("nobody") map { _ =>
          Unit
        }
    }
  }

  def url(oid: DatasetId): URL = {
    new URL(s"$endpoint/minio/$bucketName/${oid.print}")
  }

  def path(root: Path, id: DatasetId): Path = {
    root.resolve(id.print)
  }

  def tmpPath(id: DatasetId): Path = {
    path(Paths.get(config.get[String]("app.tmp")), id.print)
  }

  def datasetsDid(user: User): DatasetId = {
    s"datasets/${user.id}"
  }

  def analyseDid(user: User, analysis: Analysis): DatasetId = {
    s"analyses/${user.id}/${analysis.id}"
  }

  def analyseDid(user: User, analysisId: AnalysisId): DatasetId = {
    s"analyses/${user.id}/$analysisId"
  }

  def outputsDid(user: User, analysis: Analysis): DatasetId = {
    analyseDid(user, analysis).resolve("outputs")
  }
  def outputsDid(user: User, analysisId: AnalysisId): DatasetId = {
    analyseDid(user, analysisId).resolve("outputs")
  }
}
