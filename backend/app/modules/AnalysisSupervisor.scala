package modules

import javax.inject.{Inject, _}
import play.api.inject._
import models.{AnalysisEntry, Analysis, AnalysisState, StateController}
import models.auth.User
import no.uit.sfb.metakube.wm.WorkflowManagerLike.WorkflowId
import play.api.{Configuration, Logging}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

@Singleton
class AnalysisSupervisor @Inject()(emailer: Emailer,
                                   df: DatasetFactory,
                                   mongodb: MongodbClient,
                                   mk: MetakubeClient,
                                   config: Configuration,
                                   implicit val ec: ExecutionContext)
    extends Logging {
  private val stateController = StateController(mongodb)

  /**
    * Commit to master branch
    */
  def activate(entry: AnalysisEntry): Future[Unit] = {
    val wid = entry.wid
    val fRes = mk.activate(wid) flatMap { _ =>
      logger.info(s"Analysis '${entry.aid}' activated.")
      stateController.switchTo(entry.aid, AnalysisState.Running)
    }
    fRes recover {
      case NonFatal(e) =>
        logger.warn(s"Failed activation of '${entry.aid}'", e)
        stateController.switchTo(entry.aid, AnalysisState.InitializationFailed)
    }
  }

  /**
    * Save analysis
    */
  def save(wid: WorkflowId, analysis: Analysis, user: User): Future[Unit] = {
    val entry = AnalysisEntry.create(wid, analysis, user)
    mongodb.insert(mongodb.analyses, entry) map { _ =>
      logger.info(s"Successfully recorded analysis '${analysis.id}'")
      //Async because we don't want to wait for the analysis to be submitted to pachyderm to return
      // an answer to the user
      (mk.completeCommit(wid, analysis, user.id) flatMap { _ =>
        stateController.switchTo(entry.aid, AnalysisState.Queued) map { _ =>
          logger.info(
            s"Successfully submitted analysis '${analysis.id}' to Pachyderm")
        }
      }) recoverWith {
        case NonFatal(e) =>
          logger.warn(
            s"Failed to submit analysis '${analysis.id}' to Pachyderm",
            e)
          mk.delete(analysis.id) //async
          stateController.switchTo(entry.aid,
                                   AnalysisState.InitializationFailed)
      }
      ()
    }
  }
}
