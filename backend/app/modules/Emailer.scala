package modules

import java.net.URL

import javax.inject.{Inject, _}
import models.Analysis.AnalysisId
import models.auth.User
import play.api.inject._
import play.api.libs.mailer.{Email, MailerClient}
import play.api.{Configuration, Logging}

import scala.concurrent.{ExecutionContext, Future, blocking}

@Singleton
class Emailer @Inject()(mailerClient: MailerClient,
                        config: Configuration,
                        implicit val ec: ExecutionContext)
    extends Logging {
  protected def body(appName: String,
                     user: User,
                     title: String,
                     message: String): Option[String] = {
    val deskEmail = config.getOptional[String]("app.serviceDesk.email") match {
      case Some(email) =>
        s"""<p>For any inquiry, please send us an <a href="mailto:${email}">email</a>.</p>"""
      case None => ""
    }
    Some(s"""
       |<html>
       |  <head>
       |    <title>$title</title>
       |  </head>
       |  <body>
       |    <p>Hi ${user.name},</p>
       |    $message
       |    $deskEmail
       |    <p>Best regards,<br>$appName</p>
       |  </body>
       |</html>
       """.stripMargin)
  }

  protected def email(id: AnalysisId,
                      user: User,
                      title: String,
                      message: String): Future[Unit] = {
    if (config.getOptional[String]("play.mailer.host").isEmpty || config
          .getOptional[String]("play.mailer.password")
          .isEmpty)
      Future.successful {
        logger.warn(
          s"Did not send email as play.mailer.host or play.mailer.password are not set")
      } else {
      val appName = config.get[String]("app.name")
      val email = Email(
        title,
        s"$appName <${config.getOptional[String]("play.mailer.user").getOrElse("unknown")}>",
        Seq(s"${user.name} <${user.email}>"),
        bodyHtml = body(
          appName,
          user,
          title,
          message
        )
      )
      Future {
        blocking {
          mailerClient.send(email)
        }
      }
    }
  }

  def confirmation(id: AnalysisId, user: User): Future[Unit] = {
    val appName = config.get[String]("app.name")
    email(
      id,
      user,
      s"$appName analysis submitted",
      s"""
           |<p>Your analysis has been successfully submitted with the id '$id'.<br>
           |As soon as your results are ready, we will send you an email containing the download links.
           |</p>
           """.stripMargin
    )
  }

  def success(id: AnalysisId,
              user: User,
              urls: Map[String, URL]): Future[Unit] = {
    val appName = config.get[String]("app.name")
    email(
      id,
      user,
      s"$appName analysis $id completed",
      s"""
                |<p>Your analysis $id has completed successfully!</p>
                |<div>You can download the following outputs:
                |<ul>${urls
           .map { case (k, v) => s"""<li><a href="$v">$k</a></li>""" }
           .mkString("")}</ul>
                |</div>
                """.stripMargin
    )
  }

  def failed(id: AnalysisId, user: User): Future[Unit] = {
    val appName = config.get[String]("app.name")
    email(
      id,
      user,
      s"$appName analysis $id failed",
      s"""
           |<p>Your analysis $id has failed.<br>
           |Our team is looking at what has happened.
           |</p>
           """.stripMargin
    )
  }

  def issue(whatFailed: String,
            id: AnalysisId,
            e: Throwable,
            user: User,
            commitRef: Option[String] = None): Future[Unit] = {
    val appName = config.get[String]("app.name")
    email(
      id,
      user,
      s"$appName analysis $id $whatFailed failed",
      s"""
           |Analysis $id $whatFailed has failed with the following error:<br>
           |          ${e.getClass.getName}<br>
           |          Message: ${e.getMessage}<br>
           |          Cause: ${e.getCause}<br>
           |          Stacktrace: ${e.getStackTrace.mkString("<br>")}<br>
           |          Pachyderm commit ref: ${commitRef.getOrElse("N.A")}<br>
           """.stripMargin
    )
  }
}
