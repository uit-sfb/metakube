package modules

import java.io.InputStream
import java.net.URI
import java.nio.file.{Path, Paths}

import akka.Done
import akka.actor.CoordinatedShutdown
import com.typesafe.scalalogging.Logger
import javax.inject.{Inject, _}
import models.{AnalysisEntry, Analysis, FileInfo, StagePrintable}
import models.auth.User.UserId
import no.uit.sfb.metakube.MetakubeDriver
import no.uit.sfb.metakube.models.analysis.{AnalysisDef, AnalysisSubmission}
import no.uit.sfb.metakube.wm.WorkflowManagerLike.{StageId, WorkflowId}
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.PachydermDriver
import no.uit.sfb.scalautils.common.{FileUtils, Random}
import play.api.{Configuration, Logging}
import play.api.cache.AsyncCacheApi

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MetakubeClient @Inject()(
    config: Configuration,
    mongodb: MongodbClient,
    cache: AsyncCacheApi,
    cs: CoordinatedShutdown
)(implicit executionContext: ExecutionContext)
    extends Logging {
  private lazy val endpointSplit =
    config
      .getOptional[String]("app.pachyderm.endpoint")
      .getOrElse("localhost")
      .split(':')
  val (host, port) = endpointSplit.head -> endpointSplit.tail.headOption
    .map { _.toInt }
    .getOrElse(650)

  protected lazy val driver = new MetakubeDriver(
    new PachydermWorkflowManagerImpl(new PachydermDriver(host, port)))

  cs.addTask(CoordinatedShutdown.PhaseServiceStop,
             "Close connection to pachyderm") { () =>
    Future.successful(Done)
  }

  //create random branch and start commit
  //Returns wid and branch name
  def initAnalysis(): Future[(WorkflowId, String)] = {
    val randomId = Random.alphanumeric(10)
    driver.initAnalysis(randomId) map { wid =>
      (wid, randomId)
    }
  }

  //delete open commit and delete branch
  def abortAnalysis(wid: WorkflowId, branchName: String): Future[Unit] = {
    driver.abortAnalysis(wid, branchName)
  }

  def streamInput(is: InputStream,
                  wid: WorkflowId,
                  path: String): Future[Unit] = {
    driver.streamInput(is, wid, path)
  }

  def completeCommit(wid: WorkflowId,
                     analysis: Analysis,
                     userId: UserId): Future[WorkflowId] = {
    driver.submitAnalysis(
      AnalysisSubmission(AnalysisDef(analysis.id, Map(), analysis.config),
                         userId),
      wid = Some(wid))
  }

  def activate(wid: WorkflowId): Future[Unit] = {
    driver.runAnalysis(wid)
  }

  protected def getWid(aid: String): Future[WorkflowId] = {
    cache.getOrElseUpdate(s"aid.$aid") {
      mongodb
        .getAllMatch[AnalysisEntry](mongodb.analyses, "aid", aid) map {
        _.headOption.map { _.wid }.orNull //No caching is done when null is returned
      }
    }
  }

  def isFailed(aid: String): Future[Boolean] = {
    getWid(aid) flatMap { wid =>
      driver.isFailed(wid)
    }
  }

  def analysisStatus(aid: String): Future[Seq[StagePrintable]] = {
    getWid(aid) flatMap { wid =>
      (driver.listJobs(wid) map {
        _ map {
          case (sid, oJob) => StagePrintable(aid, wid, sid, oJob)
        }
      }) map {
        _.toSeq
      }
    }
  }

  def stage(aid: WorkflowId, sid: StageId): Future[Seq[StagePrintable]] = {
    getWid(aid) flatMap { wid =>
      driver.listRuns(wid, sid) map { s =>
        s.zipWithIndex map {
          case (oJob, idx) =>
            StagePrintable(aid, wid, sid, oJob, idx + 1)
        }
      }
    }
  }

  def logs(aid: WorkflowId, sid: StageId, run: Int): Future[String] = {
    getWid(aid) flatMap { wid =>
      driver.log(wid, sid, run)
    }
  }

  def outputs(aid: String): Future[Map[String, Seq[FileInfo]]] = {
    getWid(aid) flatMap { wid =>
      driver.outputs(wid) map { files =>
        (files map { of =>
          of.sid -> FileInfo.fromOutputFile(of)
        }).groupBy { case (sid, _) => sid } mapValues {
          _ map {
            _._2
          }
        }
      }
    }
  }

  def downloadAll(aid: String, to: Path): Future[Path] = {
    getWid(aid) flatMap { wid =>
      driver.downloadAll(wid, to)
    }
  }

  def cancel(aid: WorkflowId, sid: StageId): Future[Unit] = {
    getWid(aid) flatMap { wid =>
      driver.killJob(wid, sid)
    }
  }

  def delete(aid: WorkflowId): Future[Unit] = {
    getWid(aid) flatMap { wid =>
      driver.deleteJobs(wid)
    }
  }

  def retry(aid: WorkflowId): Future[Unit] = {
    getWid(aid) flatMap { wid =>
      driver.runAnalysis(wid)
    }
  }

  def check(retry: Int = 0): Future[Unit] = {
    driver.check(retry)
  }
}
