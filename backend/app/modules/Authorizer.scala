package modules

import javax.inject.{Inject, _}
import models.AnalysisEntry
import models.auth.User
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Pac4jScalaTemplateHelper
import play.api.mvc.{
  ActionBuilderImpl,
  BodyParsers,
  Request,
  RequestHeader,
  Result
}
import play.api.{Configuration, Logging}

import scala.concurrent.{ExecutionContext, Future}
import play.api.mvc.Results._

@Singleton
class Authorizer @Inject()(
    parser: BodyParsers.Default,
    mongodb: MongodbClient,
    config: Configuration,
    implicit val ec: ExecutionContext,
    implicit val pac4jTemplateHelper: Pac4jScalaTemplateHelper[CommonProfile],
    implicit val cfg: play.api.Configuration
) extends ActionBuilderImpl(parser)
    with Logging {
  def isLoggedIn(implicit request: RequestHeader): Boolean = {
    pac4jTemplateHelper.getCurrentProfile(request).isDefined
  }

  //Throws if not logged in
  def user()(implicit request: RequestHeader): User = {
    pac4jTemplateHelper.getCurrentProfile match {
      case Some(profile) => User(profile)
      case None          =>
        //This is a hack. It shouldn't be necessary to deal with this case,
        //but for some reason, "anonymous" profile doesn't show up here
        //It is not a security concern though, since endpoint protection works as intended
        User("anonymous",
             "anonymous",
             "Anonymous",
             cfg
               .getOptional[String]("app.auth.users.anonymousClient.email")
               .getOrElse(""))
    }
  }

  def isAdmin(implicit request: RequestHeader): Boolean = {
    config
      .get[Seq[String]]("app.authorization.admins")
      .contains(user().id)
  }

  def isOwner(implicit request: RequestHeader): Future[Boolean] = {
    val aid = request.getQueryString("aid").getOrElse("")
    mongodb.getAllMatch[AnalysisEntry](mongodb.analyses, "aid", aid) map {
      _.exists {
        _.owner == user().id
      }
    }
  }

  val loggedIn = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
        request: Request[A],
        block: (Request[A]) => Future[Result]): Future[Result] = {
      implicit val req = request
      Future(isLoggedIn) flatMap {
        case true  => block(request)
        case false => Future(Forbidden)
      }
    }
  }

  val admin = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
        request: Request[A],
        block: (Request[A]) => Future[Result]): Future[Result] = {
      implicit val req = request
      Future(isAdmin) flatMap {
        case true  => block(request)
        case false => Future(Forbidden)
      }
    }
  }

  val owner = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
        request: Request[A],
        block: (Request[A]) => Future[Result]): Future[Result] = {
      implicit val req = request
      isOwner flatMap {
        case true  => block(request)
        case false => Future(Forbidden)
      }
    }
  }

  val ownerOrAdmin = new ActionBuilderImpl(parser) {
    override def invokeBlock[A](
        request: Request[A],
        block: (Request[A]) => Future[Result]): Future[Result] = {
      implicit val req = request
      if (isAdmin)
        block(request)
      else {
        isOwner flatMap {
          case true  => block(request)
          case false => Future(Forbidden)
        }
      }
    }
  }
}
