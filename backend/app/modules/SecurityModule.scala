package modules

import java.util
import java.util.concurrent.CompletionStage

import com.google.inject.{AbstractModule, Provides}
import org.pac4j.core.client.{Client, Clients}
import org.pac4j.core.client.direct.AnonymousClient
import org.pac4j.play.{CallbackController, LogoutController, PlayWebContext}
import play.api.{Configuration, Environment}
import org.pac4j.play.store.{PlayCookieSessionStore, PlaySessionStore, ShiroAesDataEncrypter}
import org.pac4j.core.config.Config
import org.pac4j.core.context.session.SessionStore
import org.pac4j.core.context.{Pac4jConstants, WebContext}
import org.pac4j.core.credentials.{Credentials, UsernamePasswordCredentials}
import org.pac4j.core.credentials.authenticator.Authenticator
import org.pac4j.core.engine.DefaultSecurityLogic
import org.pac4j.core.exception.CredentialsException
import org.pac4j.core.http.url.DefaultUrlResolver
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.direct.DirectBasicAuthClient
import org.pac4j.oidc.client.OidcClient
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.profile.OidcProfile
import org.pac4j.play.http.PlayHttpActionAdapter
import org.pac4j.play.scala.{DefaultSecurityComponents, Pac4jScalaTemplateHelper, SecurityComponents}
import play.api.libs.ws.WSClient
import play.api.mvc.Result

import scala.concurrent.ExecutionContext
import scala.util.Try

/**
  * Guice DI module to be included in application.conf
  */
class SecurityModule(environment: Environment, configuration: Configuration)
    extends AbstractModule {

  override def configure(): Unit = {

    val sKey =
      configuration.get[String]("play.http.secret.key").substring(0, 16)
    val dataEncrypter = new ShiroAesDataEncrypter(sKey)
    val playSessionStore = new PlayCookieSessionStore(dataEncrypter)
    bind(classOf[PlaySessionStore]).toInstance(playSessionStore)
    //bind(classOf[PlaySessionStore]).to(classOf[PlayCacheSessionStore])

    bind(classOf[SecurityComponents]).to(classOf[DefaultSecurityComponents])

    bind(classOf[Pac4jScalaTemplateHelper[CommonProfile]]) //??

    // callback
    val callbackController = new CallbackController()
    callbackController.setRenewSession(false)
    bind(classOf[CallbackController]).toInstance(callbackController)

    // logout
    val logoutController = new LogoutController()
    logoutController.setCentralLogout(true)
    logoutController.setDefaultUrl(
      s"${configuration.getOptional[String]("app.externalUrl").getOrElse("")}/")
    bind(classOf[LogoutController]).toInstance(logoutController)
  }

  protected def anonymousClient: Option[AnonymousClient] = {
    val client = new AnonymousClient()
    client.setName("AnonymousClient")
    Some(client)
  }

  protected def elixirAaiClient
    : Option[OidcClient[OidcProfile, OidcConfiguration]] = {
    val oidcConfiguration = new OidcConfiguration()
    //oidcConfiguration.setScope("openid profile email")
    Try {
      oidcConfiguration.setClientId(
        configuration.get[String]("app.auth.users.elixirAai.client.id"))
      oidcConfiguration.setSecret(
        configuration.get[String]("app.auth.users.elixirAai.client.secret"))
      oidcConfiguration.setDiscoveryURI(
        configuration.get[String]("app.auth.users.elixirAai.discovery"))
      oidcConfiguration.addCustomParam("prompt", "consent")
    }.toOption map { _ =>
      val oidcClient =
        new OidcClient[OidcProfile, OidcConfiguration](oidcConfiguration)
      oidcClient.setName("ElixirAaiClient")
      oidcClient
    }
  }

  protected def basicDirectClient: Option[DirectBasicAuthClient] = {
    val basicAuth = new Authenticator[UsernamePasswordCredentials]() {
      override def validate(credentials: UsernamePasswordCredentials,
                            context: WebContext): Unit = {
        val clientKey = configuration.get[String]("app.auth.clients.key")
        val clientSecret = configuration.get[String]("app.auth.clients.secret")
        if (credentials.getUsername != clientKey || credentials.getPassword != clientSecret)
          throw new CredentialsException(
            "Username : '" + clientKey + "' does not match password")
        val profile = new CommonProfile()
        profile.setId(clientKey)
        profile.addAttribute(Pac4jConstants.USERNAME, clientKey)
        credentials.setUserProfile(profile)
      }
    }
    val client = new DirectBasicAuthClient(basicAuth)
    client.setName("BasicAuthClient")
    Some(client)
  }

  @Provides
  def provideConfig()(implicit ws: WSClient, ec: ExecutionContext): Config = {
    val cls =
      Seq(elixirAaiClient, anonymousClient, basicDirectClient).flatten
    val clients = new Clients(cls: _*)
    assert(cls.nonEmpty, "No security client found!")
    //When using a relative Url with completeRelativeUrl set to true, it seems that the web context indicates http when it should be https...
    configuration.getOptional[String]("app.externalUrl") match {
      case Some(url) =>
      clients.setCallbackUrl(url + "/callback")
        clients.setUrlResolver(new DefaultUrlResolver(false))
      case None =>
        clients.setCallbackUrl("/callback")
        clients.setUrlResolver(new DefaultUrlResolver(true))
    }
    val config = new Config(clients)
    config.setHttpActionAdapter(new PlayHttpActionAdapter())
    config.setSecurityLogic(new CustomSecurityLogic(configuration))
    config
  }
}

class CustomSecurityLogic(configuration: Configuration) extends DefaultSecurityLogic[CompletionStage[Result], PlayWebContext] {
  protected override def saveRequestedUrl(context: PlayWebContext, currentClients: util.List[Client[_ <: Credentials, _ <: CommonProfile]]): Unit = {
    val ajaxResolv = getAjaxRequestResolver
    if (ajaxResolv == null || !ajaxResolv.isAjax(context)) {
      val requestedUrl = configuration.getOptional[String]("app.externalUrl") match {
        case Some(url) =>
          s"${url}${context.getPath()}"
        case None =>
          context.getFullRequestURL()
      }
      logger.debug("requestedUrl: {}", requestedUrl);
      context.getSessionStore().asInstanceOf[SessionStore[PlayWebContext]].set(context, Pac4jConstants.REQUESTED_URL, requestedUrl);
    }
  }
}