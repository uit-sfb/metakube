package modules

import java.nio.file.{Path, Paths}

import javax.inject.{Inject, _}
import models.workflow.{Input, Module, Pathway, PathwayT, Pipeline, Profile, ProfileT, Stage, Template, TemplateT}
import no.uit.sfb.metakube.models.utils.DagLike
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.yaml.Yaml
import play.api.{Configuration, Logging}

import scala.concurrent.ExecutionContext

@Singleton
class Workflow @Inject()(mk: MetakubeClient,
                         config: Configuration,
                         implicit val ec: ExecutionContext)
  extends Logging {
  protected val workflowPath =
    Paths.get(config.get[String]("app.pipeline.workflowPath"))
  protected val templatesPath = workflowPath.resolve("templates")
  protected val profilesPath = workflowPath.resolve("profiles")
  protected val pathwayPath = workflowPath.resolve("pathways")

  import sys.process._

  protected def parseTemplates(path: Path): Seq[Template] = {
    val inTemplatesDir = FileUtils
      .filesUnder(path)
      // We are only interested in files, not symlinks created by k8s
      .filter {
        !_.toString.contains("/..")
      } // FileUtils.isFile(p, false) doesn't work

    val templateFilePaths = {
      val templates =
        inTemplatesDir.filter { p =>
          p.getFileName.toString
            .endsWith(".tpl.yaml")
        }
      templates.size match {
        case 0 =>
          throw new Exception(s"No templates found in $path")
        case _ => templates
      }
    }
    templateFilePaths map { p =>
      Yaml.fromFileAs[TemplateT](p).toTemplate
    }
  }

  protected def parseProfiles(path: Path): Seq[Profile] = {
    val inProfilesDir = FileUtils
      .filesUnder(path)
      // We are only interested in files, not symlinks created by k8s
      .filter {
        !_.toString.contains("/..")
      } // FileUtils.isFile(p, false) doesn't work

    inProfilesDir.size match {
      case 0 =>
        throw new Exception(s"No profile found in $path")
      case _ =>
        inProfilesDir map { p =>
          val profileT = Yaml.fromFileAs[ProfileT](p)
          Profile.profile(p.getFileName.toString.split('.').head, profileT)
        }
    }
  }

  protected def parsePathways(path: Path): Seq[Pathway] = {
    val inPathwaysDir = FileUtils
      .filesUnder(path)
      // We are only interested in files, not symlinks created by k8s
      .filter {
        !_.toString.contains("/..")
      } // FileUtils.isFile(p, false) doesn't work

    inPathwaysDir.size match {
      case 0 =>
        throw new Exception(s"No pathway found in $path")
      case _ =>
        inPathwaysDir map { p =>
          val pathway = Yaml.fromFileAs[PathwayT](p)
          val name = p.getFileName.toString.split('.').head
          val label = if (pathway.label.isEmpty)
            name
          else
            pathway.label
          Pathway(name, label, pathway.inputs.map { case (k, v) => Input(k, v.label, v.description, v.path) }.toSeq)
        }
    }
  }

  protected lazy val modules: Seq[Module] = {
    case class TemplateSet(elements: Set[Template]) extends DagLike[Template]

    lazy val templates = TemplateSet(parseTemplates(templatesPath).toSet).sorted
    templates.foldLeft[Seq[Module]](Seq()) {
      case (acc, template) =>
        val splt = template.stage.split('_')
        val moduleName = splt.head
        val stageName = splt.tail.mkString("_")
        val stage = Stage(stageName, template.parameters)
        acc.partition(_.name == moduleName) match {
          case (Nil, rest) => rest :+ Module(moduleName, Seq(stage))
          case (elem :: Nil, rest) => rest :+ elem.copy(stages = elem.stages :+ stage)
          case _ => //Should not happen
            throw new Exception("More than one Module have the same name.")
        }
    }
  }
  protected lazy val profiles: Seq[Profile] = {
    val rawProfiles = parseProfiles(profilesPath)
    val extraRunParam: Seq[String] = for (
      m <- modules;
      s <- m.stages if s.params.exists {
        _.name == "MK_RUN"
      }
    ) yield {
      s"${m.name}_${s.name}"
    }
    rawProfiles map { prof =>
      prof.copy(stages = prof.stages.map { sp =>
        if (sp.params.contains("MK_RUN") || !extraRunParam.contains(sp.name))
          sp
        else
          sp.copy(params = sp.params + ("MK_RUN" -> "true"))
      })
    }
  }
  protected lazy val pathways: Seq[Pathway] = parsePathways(pathwayPath)

  lazy val pipeline = Pipeline(modules, profiles, pathways)
}
