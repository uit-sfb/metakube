package modules

import java.util

import akka.Done
import akka.actor.CoordinatedShutdown
import com.mongodb.client.model.{Filters, Indexes}
import com.mongodb.{
  BasicDBObject,
  MongoClientSettings,
  MongoCredential,
  ServerAddress
}
import com.mongodb.client.{
  MongoClient,
  MongoClients,
  MongoCollection,
  MongoDatabase,
  MongoIterable
}
import javax.inject.{Inject, _}
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.json.JsonWriterSettings
import play.api.Configuration
import play.api.inject._
import play.api.libs.json.{Json, Reads, Writes}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future, blocking}

@Singleton
class MongodbClient @Inject()(config: Configuration, cs: CoordinatedShutdown)(
    implicit executionContext: ExecutionContext) {

  protected val credential = MongoCredential.createCredential(
    config.getOptional[String]("app.mongodb.user").getOrElse("user"),
    config.getOptional[String]("app.mongodb.database").getOrElse("metakube"),
    config.getOptional[String]("app.mongodb.password").getOrElse("changeme").toCharArray)
  protected val (host, port) = {
    val endpointSplit = config.getOptional[String]("app.mongodb.endpoint").getOrElse("localhost").split(':')
    endpointSplit.head -> endpointSplit.tail.headOption
      .map { _.toInt }
      .getOrElse(27017)
  }

  protected lazy val mongoClient = MongoClients.create(
    MongoClientSettings
      .builder()
      .applyToClusterSettings(builder =>
        builder.hosts(List(new ServerAddress(host, port)).asJava))
      .credential(credential)
      .build())

  cs.addTask(CoordinatedShutdown.PhaseServiceStop,
             "Close connection to MongoDb") { () =>
    Future { blocking { mongoClient.close(); Done } }
  }

  protected lazy val database: Future[MongoDatabase] = {
    Future {
      blocking {
        mongoClient.getDatabase(config.getOptional[String]("app.mongodb.database").getOrElse("metakube"))
      }
    }
  }

  def checkClient(): Future[Unit] = {
    Future {
      blocking {
        mongoClient.getClusterDescription
        Unit
      }
    }
  }

  lazy val analyses: Future[MongoCollection[Document]] = {
    database map { db =>
      val coll = db.getCollection("analyses")
      coll.createIndex(Indexes.hashed("aid"))
      coll
    }
  }

  def insert(coll: Future[MongoCollection[Document]],
             values: Map[String, Any]): Future[Unit] = {
    val doc = new Document(new BasicDBObject(values.asJava))
    coll map {
      _.insertOne(doc)
    }
  }

  def insert[T](coll: Future[MongoCollection[Document]], data: T)(
      implicit write: Writes[T]): Future[Unit] = {
    val str = stringify(data)
    val doc = Document.parse(str)
    coll map {
      _.insertOne(doc)
    }
  }

  def as[T](doc: Document)(implicit read: Reads[T]): T = {
    val settings = JsonWriterSettings
      .builder()
      .int64Converter((value, writer) => writer.writeNumber(value.toString))
      .build()
    Json.parse(doc.toJson(settings)).as[T]
  }

  protected def stringify[T](obj: T)(implicit write: Writes[T]): String =
    Json.stringify(Json.toJson(obj))

  protected def asSeq[T](it: MongoIterable[T]): Seq[T] = {
    val acc = new util.ArrayList[T]()
    it.into(acc)
    acc.asScala
  }

  def getAll[T](coll: Future[MongoCollection[Document]])(
      implicit read: Reads[T]): Future[Seq[T]] = {
    coll map { c =>
      asSeq[T](c.find().map(doc => as[T](doc)))
    }
  }

  def getAllMatch[T](
      coll: Future[MongoCollection[Document]],
      indexName: String,
      indexValue: Any)(implicit read: Reads[T]): Future[Seq[T]] = {
    coll map { c =>
      asSeq[T](c.find(Filters.eq(indexName, indexValue)).map(doc => as[T](doc)))
    }
  }

  def collectAll[A](coll: Future[MongoCollection[Document]],
                    filter: Bson,
                    collect: PartialFunction[Document, A]): Future[Seq[A]] = {
    coll map {
      _.find(filter).asScala.collect(collect).toSeq
    }
  }

  def count(coll: Future[MongoCollection[Document]],
            filter: Bson): Future[Long] = {
    coll map {
      _.countDocuments(filter)
    }
  }

  def replace[T](coll: Future[MongoCollection[Document]],
                 indexName: String,
                 indexValue: Any,
                 replacement: T)(implicit write: Writes[T]): Future[Unit] = {
    val docReplacement = Document.parse(stringify(replacement))
    coll map {
      _.replaceOne(Filters.eq(indexName, indexValue), docReplacement)
    }
  }

  //Could work but then you need to have a codec for basic types
  def update(coll: Future[MongoCollection[Document]],
             indexName: String,
             indexValue: Any,
             updates: Map[String, Any]): Future[Unit] = {
    val doc = new BasicDBObject("$set", new BasicDBObject(updates.asJava))
    coll map { c =>
      val res = c.updateOne(Filters.eq(indexName, indexValue), doc)
      if (res.getModifiedCount.toInt <= 0)
        throw new NoSuchElementException(
          s"Update failed due to no element matching '$indexName = $indexValue'")
    }
  }

  @deprecated("This is just a hack until a codec is added")
  def update[T](coll: Future[MongoCollection[Document]],
                indexName: String,
                indexValue: Any,
                updates: T => T)(implicit read: Reads[T],
                                 write: Writes[T]): Future[Unit] = {
    getAllMatch[T](coll, indexName, indexValue) flatMap { seq =>
      if (seq.isEmpty)
        throw new NoSuchElementException(
          s"Update failed due to no element matching '$indexName = $indexValue'")
      replace[T](coll, indexName, indexValue, updates(seq.head))
    }
  }

  def delete(coll: Future[MongoCollection[Document]],
             indexName: String,
             indexValue: Any): Future[Unit] = {
    coll map { _.deleteOne(Filters.eq(indexName, indexValue)) }
  }
}
