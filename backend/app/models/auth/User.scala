package models.auth

import models.auth.User.UserId
import org.pac4j.core.profile.CommonProfile

case class User(
    id: UserId,
    username: String,
    name: String,
    email: String
)

object User {
  type UserId = String

  def apply(p: CommonProfile): User = User(
    p.getId,
    p.getUsername,
    p.getDisplayName,
    p.getEmail
  )
}

object Anonymous
    extends User(
      "anonymous",
      username = "anonymous",
      name = "anonymous",
      email = "metapipe.uit.no@gmail.com"
    )
