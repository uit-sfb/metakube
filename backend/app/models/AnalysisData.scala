package models

import play.api.libs.json.Json

case class AnalysisData(data: Seq[StagePrintable],
                        isRetryable: Boolean,
                        isDeletable: Boolean)

object AnalysisData {
  implicit val format = Json.format[AnalysisData]

  def apply(data: Seq[StagePrintable]): AnalysisData = {
    AnalysisData(
      data,
      data.forall(
        s =>
          (s.state.isTerminal || s.state == StageState.Pending) && data.exists(
            _.state.isRetryable)),
      data.forall(_.state.isTerminal && data.exists(_.state.isDeletable))
    )
  }
}
