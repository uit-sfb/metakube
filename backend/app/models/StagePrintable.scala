package models

import java.time.{Instant, LocalDateTime, ZoneOffset}

import models.utils.Chronos
import no.uit.sfb.metakube.models.Job
import play.api.libs.json.Json

case class StagePrintable(aid: String,
                          wid: String,
                          sid: String,
                          status: String,
                          start: String,
                          end: String,
                          duration: String,
                          run: Int) {
  val state = StageState(status)
}

object StagePrintable {
  implicit val format = Json.format[StagePrintable]

  def apply(aid: String,
            wid: String,
            stage: String,
            oJob: Option[Job],
            run: Int = -1): StagePrintable =
    StagePrintable(
      aid = aid,
      wid = wid,
      sid = stage,
      status = oJob.map { _.state.name }.getOrElse("PENDING"),
      start = oJob
        .map { job =>
          Chronos.dateFormat.format(job.started)
        }
        .getOrElse("-"),
      end = (oJob
        .flatMap { _.finished }
        .map { Chronos.dateFormat.format })
        .getOrElse("-"),
      duration = {
        (oJob
          .map { job =>
            Chronos.prettyDuration(
              job.started,
              job.finished match {
                case Some(finish) => finish
                case None =>
                  LocalDateTime.ofInstant(
                    Instant.ofEpochMilli(System.currentTimeMillis()),
                    ZoneOffset.UTC)
              }
            ) + (if (job.finished.nonEmpty) "" else "...")
          })
          .getOrElse("-")
      },
      run = run
    )
}
