package models

import modules.MongodbClient
import play.api.Logging

import scala.concurrent.{ExecutionContext, Future}

case class StateController(mongodb: MongodbClient)(
    implicit ec: ExecutionContext)
    extends Logging {
  def switchTo(aid: String, newState: AnalysisState.State): Future[Unit] = {
    mongodb.getAllMatch[AnalysisEntry](mongodb.analyses, "aid", aid) flatMap {
      _.headOption match {
        case Some(ae) if !ae.state.isTerminal || ae.state.isRetryable  =>
          mongodb.update(
            mongodb.analyses,
            "aid",
            aid,
            Map("status" -> newState.name, "validity" -> {
              if (newState.timeout.toMillis > 0)
                System.currentTimeMillis() + newState.timeout.toMillis
              else
                0L
            })
          )
        case Some(ae) =>
          logger.info(
            s"Cannot switch state of analysis in final state ${ae.state} (aid='$aid')")
          Future.successful(())
        case _ =>
          logger.info(
            s"Cannot change state of analysis '$aid' since not found.")
          Future.successful(())
      }
    }
  }
}
