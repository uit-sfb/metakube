package models

import models.Dataset.DatasetId
import no.uit.sfb.apis.storage.ObjectId

case class Dataset(
    id: DatasetId,
    tags: Set[String]
)

object Dataset {
  type DatasetId = ObjectId

  def apply(id: DatasetId, tags: String): Dataset = {
    Dataset(id, tags.split(',').map { _.trim() }.toSet)
  }
}
