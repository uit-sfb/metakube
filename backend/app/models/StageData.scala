package models

import play.api.libs.json.Json

case class StageData(data: Seq[StagePrintable], isCancellable: Boolean)

object StageData {
  implicit val format = Json.format[StageData]

  def apply(data: Seq[StagePrintable]): StageData = {
    StageData(data,
              data.headOption map { _.state.isCancellable } getOrElse (false))
  }
}
