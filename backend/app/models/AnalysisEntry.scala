package models

import java.time.{Instant, LocalDateTime, ZoneOffset}

import models.Analysis.AnalysisId
import models.auth.User
import models.auth.User.UserId
import models.utils.Chronos
import no.uit.sfb.metakube.wm.WorkflowManagerLike.WorkflowId
import play.api.libs.json.Json
import no.uit.sfb.scalautils.json.{Json => ScalaUtilsJson}

case class AnalysisEntry(aid: AnalysisId,
                         owner: UserId,
                         tags: Set[String],
                         tsStart: Long,
                         status: String,
                         wid: String,
                         tsEnd: Option[Long],
                         metadata: Map[String, String],
                         config: Option[String],
                         logs: Map[String, String], //sid_jobId -> log
                         validity: Option[Long]) {
  private val dateStart =
    LocalDateTime.ofInstant(Instant.ofEpochMilli(tsStart), ZoneOffset.UTC)

  private val dateStartStr = Chronos.dateFormat.format(dateStart)

  private val dateEnd = tsEnd match {
    case Some(tEnd) =>
      LocalDateTime.ofInstant(Instant.ofEpochMilli(tEnd), ZoneOffset.UTC)
    case None =>
      LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()),
                              ZoneOffset.UTC)
  }

  private val duration = Chronos.prettyDuration(dateStart, dateEnd)

  val state = AnalysisState(status)

  val isDownloadable = state match {
    case AnalysisState.Finished => true
    case _                      => false
  }
  val isCancellable = state.isCancellable

  lazy val toPrintable = AnalysisEntryPrintable(
    aid,
    owner,
    tags.mkString(","),
    dateStartStr,
    state.name,
    wid,
    duration + (if (tsEnd.nonEmpty) "" else "..."),
    state.isRetryable,
    state.isDeletable
  )
}

object AnalysisEntry {
  implicit val format = Json.format[AnalysisEntry]

  def create(wid: WorkflowId, analysis: Analysis, user: User): AnalysisEntry = {
    val ts = System.currentTimeMillis()
    AnalysisEntry(
      analysis.id,
      user.id,
      analysis.tags,
      ts,
      AnalysisState.Initializing.name,
      wid,
      None,
      Map(),
      Some(ScalaUtilsJson.prettyPrint(analysis)),
      Map(),
      Some(ts + AnalysisState.Initializing.timeout.toMillis)
    )
  }
}

case class AnalysisEntryPrintable(aid: AnalysisId,
                                  owner: UserId,
                                  tags: String,
                                  timestamp: String,
                                  status: String,
                                  wid: String,
                                  duration: String,
                                  isRetryable: Boolean,
                                  isDeletable: Boolean)

object AnalysisEntryPrintable {
  implicit val format = Json.format[AnalysisEntryPrintable]
}
