package models.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

object Chronos {
  def prettyDuration(start: LocalDateTime, end: LocalDateTime): String = {
    val h = ChronoUnit.HOURS.between(start, end)
    val m = ChronoUnit.MINUTES.between(start.plusHours(h), end)
    val s =
      ChronoUnit.SECONDS.between(start.plusHours(h).plusMinutes(m), end)
    s"${h}h ${m}m ${s}s"
  }

  val dateFormat =
    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
}
