package models

import no.uit.sfb.metakube.models.OutputFile
import play.api.libs.json.Json

case class FileInfo(name: String, size: Long) {
  val prettySize: String = {
    size / 1024 match {
      case 0 => s"$size B"
      case k =>
        k / 1024 match {
          case 0 => s"$k KB"
          case m =>
            m / 1024 match {
              case 0 => s"$m MB"
              case g =>
                g / 1024 match {
                  case 0 => s"$g GB"
                  case t => s"$t TB"
                }
            }
        }
    }
  }
}

object FileInfo {
  implicit val format = Json.format[AnalysisEntry]

  def fromOutputFile(of: OutputFile): FileInfo = {
    FileInfo(of.path.getFileName.toString, of.size)
  }
}
