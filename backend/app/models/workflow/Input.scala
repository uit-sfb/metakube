package models.workflow

import com.fasterxml.jackson.annotation.JsonCreator

case class InputT(label: String, description: String, path: String) {
  @JsonCreator
  def this() = this("", "", ".")
}

case class Input(name: String, label: String, description: String, path: String) {
  val safeId = name.replace(' ', '_')
}
