package models.workflow

case class Module(name: String, stages: Seq[Stage]) {
  val nonEmpty: Boolean = stages.exists{ _.nonEmpty }
  val safeId = name.replace(' ', '_')
}
