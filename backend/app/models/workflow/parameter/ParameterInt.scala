package models.workflow.parameter

case class ParameterInt(name: String,
                        label: String,
                        description: String,
                        keyOverride: Option[String]) extends ParameterLike
