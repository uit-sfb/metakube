package models.workflow.parameter

case class ParameterBoolean(name: String,
                            label: String,
                            description: String,
                            keyOverride: Option[String]) extends ParameterLike
