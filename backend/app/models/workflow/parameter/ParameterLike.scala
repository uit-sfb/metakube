package models.workflow.parameter

trait ParameterLike {
  def name: String
  def label: String
  def description: String
  def keyOverride: Option[String]
  def stageIdOverride(moduleName: String, stageName: String) = {
    keyOverride match {
      case Some(kOverride) =>
        s"$kOverride:$name"
      case _ =>
        s"${moduleName}_$stageName:$name"
    }
  }
  def fLabel = if (label.isEmpty) name else label
  def safeId = name.replace(' ', '_')
}
