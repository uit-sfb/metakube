package models.workflow.parameter

case class ParameterSelect(name: String,
                           label: String,
                           description: String,
                           keyOverride: Option[String],
                           items: Map[String, String] = Map()) extends ParameterLike
