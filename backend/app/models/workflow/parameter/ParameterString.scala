package models.workflow.parameter

case class ParameterString(name: String,
                           label: String,
                           description: String,
                           keyOverride: Option[String]) extends ParameterLike
