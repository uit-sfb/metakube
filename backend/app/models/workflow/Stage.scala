package models.workflow

import models.workflow.parameter.ParameterLike

case class Stage(name: String, params: Seq[ParameterLike]) {
  val nonEmpty: Boolean = params.nonEmpty
  val safeId = name.replace(' ', '_')
}
