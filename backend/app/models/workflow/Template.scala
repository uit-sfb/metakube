package models.workflow

import com.fasterxml.jackson.annotation.JsonCreator
import models.workflow.parameter._
import no.uit.sfb.metakube.models.utils.DependsOnLike
import no.uit.sfb.pachyderm.models.spec.PipelineInput

case class TemplateT(stage: String,
                     optional: Boolean = false,
                     parameters: Map[String, ParameterDef] = Map(),
                     input: PipelineInput = PipelineInput()
                    ) {
  @JsonCreator
  def this() = this("", false, Map())

  lazy val toTemplate = {
    val extraParameters = {
      Seq(
        "MK_RUN" -> ParameterDef("Enabled", "Whether this optional stage should be enabled or not.", "Boolean", !optional)
      )
    }
    val params = (extraParameters ++ parameters.toSeq) collect {
      case (k, v) if !v.internal => v.parameterLike(k)
    }
    Template(stage, params, input)
  }
}

case class Template(stage: String,
                    parameters: Seq[ParameterLike] = Seq(),
                    input: PipelineInput = PipelineInput()
                   ) extends DependsOnLike {
  val id = stage
  val parents = input.parents()
}

case class ParameterDef(label: String,
                        description: String,
                        `type`: String,
                        internal: Boolean = false,
                        keyOverride: Option[String] = None,
                        select: Map[String, String] = Map()
                       ) {
  @JsonCreator
  def this() = this("", "", "", false, None, Map())

  def parameterLike(name: String): ParameterLike = {
    `type` match {
      case "String" =>
        require(select.isEmpty)
        ParameterString(name, label, description, keyOverride)
      case "Int" =>
        require(select.isEmpty)
        ParameterInt(name, label, description, keyOverride)
      case "Integer" =>
        require(select.isEmpty)
        ParameterInt(name, label, description, keyOverride)
      case "Boolean" =>
        require(select.isEmpty)
        ParameterBoolean(name, label, description, keyOverride)
      case "Select" => ParameterSelect(name, label, description, keyOverride, select)
    }
  }
}


