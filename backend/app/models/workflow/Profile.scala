package models.workflow

import com.fasterxml.jackson.annotation.JsonCreator

case class StageProfile(name: String, params: Map[String, String])

case class Profile(name: String, label: String, stages: Seq[StageProfile]) {
  val safeId = name.replace(' ', '_')
}

case class ProfileT(label: String = "", assign: Map[String, Map[String, Any]] = Map()) {
  @JsonCreator
  def this() = this("", Map())
}

object Profile {
  def profile(name: String, p: ProfileT): Profile = {
    val label = if (p.label.isEmpty)
      name
    else
      p.label
    Profile(name, label, (p.assign map { case (k, v) => StageProfile(k, v.mapValues{_.toString})}).toSeq)
  }
}