package models.workflow

case class Pipeline(modules: Seq[Module],
                    profiles: Seq[Profile],
                    pathways: Seq[Pathway]
                   )

