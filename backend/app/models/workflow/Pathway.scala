package models.workflow

import com.fasterxml.jackson.annotation.JsonCreator

case class PathwayT(label: String, inputs: Map[String, InputT]) {
  @JsonCreator
  def this() = this("", Map())
}

case class Pathway(name: String, label: String, inputs: Seq[Input]) {
  val safeId = name.replace(' ', '_')
}
