package models

import models.Analysis.AnalysisId

case class Analysis(
  id: AnalysisId,
  tags: Set[String],
  inputs: Map[String, Dataset],
  config: String
)

object Analysis {
  type AnalysisId = String
}
