package models

object StageState {
  sealed trait State {
    def name: String
    def isTerminal: Boolean = false
    final def isDeletable: Boolean = isTerminal
    def isCancellable: Boolean = false
    def isRetryable: Boolean = false
  }
  case object Pending extends State {
    val name = "PENDING"
  }
  case object Running extends State {
    val name = "RUNNING"
    override def isCancellable: Boolean = true
  }
  case object ExecutionFailed extends State {
    val name = "FAILED"
    override def isTerminal: Boolean = true
    override def isRetryable = true
  }
  case object Finished extends State {
    val name = "SUCCESS"
    override def isTerminal: Boolean = true
  }
  case object Killed extends State {
    val name = "KILLED"
    override def isTerminal: Boolean = true
    override def isRetryable: Boolean = true
  }
  case object Unknown extends State {
    val name = "UNKNOWN"
  }

  def apply(str: String): State = str match {
    case Pending.name         => Pending
    case Running.name         => Running
    case ExecutionFailed.name => ExecutionFailed
    case Finished.name        => Finished
    case Killed.name          => Killed
    case _                    => Unknown
  }
}
