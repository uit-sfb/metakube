package models

import scala.concurrent.duration._

object AnalysisState {
  sealed trait State {
    def name: String
    def isTerminal: Boolean = false
    def isDeletable: Boolean = false
    def isCancellable: Boolean = false
    def isRetryable: Boolean = true
    def isBackedByPachyderm: Boolean = false
    def timeout: Duration = 0.second //If 0, no timeout
    def failedState: Option[State] = None
  }
  case object Initializing extends State {
    val name = "Initializing"
    override val timeout: Duration = 15.minutes
    override val failedState = Some(InitializationFailed)
  }
  case object Queued extends State {
    val name = "Queued"
    override def isDeletable: Boolean = true
    override val failedState = Some(InitializationFailed)
  }
  //Note: Initialization failed can occur after both Initialization and Queued
  //We do not make the distinction for simplicity's sake
  case object InitializationFailed extends State {
    val name = "Initialization failed"
    override def isTerminal: Boolean = true
    override def isDeletable: Boolean = true
  }
  case object Running extends State {
    val name = "Running"
    override def isCancellable: Boolean = true
    override def isBackedByPachyderm: Boolean = true
    override def isDeletable: Boolean = true
  }
  case object ExecutionFailed extends State {
    val name = "Execution failed"
    override def isTerminal: Boolean = true
    override def isRetryable = true
    override def isBackedByPachyderm = true
    override def isDeletable: Boolean = true
  }
  case object Finished extends State {
    val name = "Finished"
    override def isTerminal: Boolean = true
    override def isDeletable: Boolean = true
    override def isRetryable: Boolean = false
  }
  case object DataLost extends State {
    val name = "Data lost"
    override def isTerminal: Boolean = true
    override def isDeletable: Boolean = true
    override def isRetryable = false
  }
  case object Unknown extends State {
    val name = "Unknown"
    override def isDeletable: Boolean = true
  }

  def apply(str: String): State = str match {
    case Initializing.name         => Initializing
    case InitializationFailed.name => InitializationFailed
    case Queued.name               => Queued
    case Running.name              => Running
    case ExecutionFailed.name      => ExecutionFailed
    case Finished.name             => Finished
    case DataLost.name             => DataLost
    case _                         => Unknown
  }

  private val allStates: Set[State] = Set(
    Initializing,
    InitializationFailed,
    Queued,
    Running,
    ExecutionFailed,
    Finished,
    DataLost,
    Unknown
  )

  def matching(cond: State => Boolean): Set[State] = {
    allStates filter cond
  }
}
