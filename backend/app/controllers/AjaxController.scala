package controllers

import javax.inject._
import models.{
  AnalysisData,
  AnalysisEntry,
  AnalysisState,
  StageData,
  StateController
}
import modules.{Authorizer, DatasetFactory, MetakubeClient, MongodbClient}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.{Configuration, Logging}
import play.api.libs.concurrent.Futures._
import play.api.libs.concurrent.Futures
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.control.NonFatal

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class AjaxController @Inject()(configuration: Configuration,
                               messagesAction: MessagesActionBuilder,
                               val controllerComponents: SecurityComponents,
                               mongodb: MongodbClient,
                               df: DatasetFactory,
                               mk: MetakubeClient,
                               implicit val futures: Futures,
                               implicit val ec: ExecutionContext,
                               implicit val authorizer: Authorizer,
                               implicit val cfg: play.api.Configuration)
    extends Security[CommonProfile]
    with Logging {
  val authClient = cfg.get[String]("app.auth.users.client")
  protected val timeout = 15.seconds

  private val stateController = StateController(mongodb)

  def analyses() = Secure(authClient).async {
    implicit request: Request[AnyContent] =>
      mongodb
        .getAllMatch[AnalysisEntry](mongodb.analyses,
                                    "owner",
                                    authorizer.user.id)
        .withTimeout(timeout)
        .map { res =>
          Ok(Json.toJson(res.reverse map {
            _.toPrintable
          }))
        }
        .recover {
          case e: scala.concurrent.TimeoutException =>
            GatewayTimeout("Request timeout while accessing mongodb")
          case NonFatal(e) => InternalServerError(e.getMessage)
        }
  }

  def allAnalyses() = Secure(authClient).andThen(authorizer.admin).async {
    implicit request: Request[AnyContent] =>
      mongodb
        .getAll[AnalysisEntry](mongodb.analyses)
        .withTimeout(timeout)
        .map { res =>
          Ok(Json.toJson(res.reverse map {
            _.toPrintable
          }))
        }
        .recover {
          case e: scala.concurrent.TimeoutException =>
            GatewayTimeout("Request timeout while accessing mongodb")
          case NonFatal(e) => InternalServerError(e.getMessage)
        }
  }

  def downloadUrl(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mongodb
          .getAllMatch[AnalysisEntry](mongodb.analyses, "aid", aid)
          .withTimeout(timeout)
          .map {
            _.headOption flatMap {
              _.metadata.get("outputs")
            }
          }
          .withTimeout(timeout)
          .flatMap {
            case Some(did) =>
              df.s3Client.presignedGetObjectUrl(did, 1.day) map { _.toString }
            case None => Future.successful("")
          }
          .withTimeout(timeout)
          .map { url =>
            Ok(url)
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  def analysis(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mk.analysisStatus(aid)
          .withTimeout(timeout)
          .map { res =>
            Ok(Json.toJson(AnalysisData(res.reverse)))
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing pachyderm")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
      //Todo update mongodb??
    }

  def analysisConf(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mongodb
          .getAllMatch[AnalysisEntry](mongodb.analyses, "aid", aid)
          .withTimeout(timeout)
          .map {
            _.headOption flatMap {
              _.config
            }
          }
          .withTimeout(timeout)
          .map { res =>
            Ok(res.getOrElse(""))
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing mongodb")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  def stage(aid: String, sid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mk.stage(aid, sid)
          .withTimeout(timeout)
          .map { res =>
            Ok(Json.toJson(StageData(res.reverse)))
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing pachyderm")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
      //Todo update mongodb??
    }

  def logs(aid: String, sid: String, run: Int) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mk.logs(aid, sid, run)
          .withTimeout(timeout)
          .map { res =>
            Ok(res)
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing pachyderm")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  def cancelStage(aid: String, sid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mk.cancel(aid, sid)
          .withTimeout(timeout)
          .map { _ =>
            Ok("")
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing pachyderm")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  def deleteAnalysis(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        //This line should execute whether or not mk.delete succeeded
        mongodb
          .delete(mongodb.analyses, "aid", aid)
          .andThen { case _ => mk.delete(aid) }
          .withTimeout(timeout)
          .map { _ =>
            Ok
          }
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout("Timeout while accessing pachyderm")
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  def retryAnalysis(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin).async {
      implicit request: Request[AnyContent] =>
        mk.retry(aid)
          .withTimeout(timeout)
          .flatMap { _ =>
            stateController.switchTo(aid, AnalysisState.Running) map { _ =>
              Ok
            }
          }
          .withTimeout(timeout)
          .recover {
            case e: scala.concurrent.TimeoutException =>
              GatewayTimeout
            case NonFatal(e) => InternalServerError(e.getMessage)
          }
    }

  /*def download(aid: String) = Secure(authClient).async {
    implicit request: Request[AnyContent] =>
      mongodb
        .getAll[AnalysisEntry](mongodb.analyses, "aid", aid)
        .withTimeout(timeout)
        .map {
          _.headOption flatMap {
            _.outputs
          }
        }
        .map {
          case Some(p) =>
            val zip = Paths.get(p)
            Ok.sendFile(
              content = zip.toFile,
              fileName = _ => zip.getFileName.toString
            )
          case None => InternalServerError("File not found")
        }
  }*/
}
