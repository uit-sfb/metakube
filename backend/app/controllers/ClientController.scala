package controllers

import javax.inject._
import models.{AnalysisEntry, AnalysisState}
import modules.{Authorizer, MetakubeClient, MongodbClient}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.Logging
import play.api.libs.concurrent.Futures
import play.api.libs.concurrent.Futures._
import play.api.libs.json.JsValue
import play.api.mvc._
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class ClientController @Inject()(messagesAction: MessagesActionBuilder,
                                 val controllerComponents: SecurityComponents,
                                 mongodb: MongodbClient,
                                 mk: MetakubeClient,
                                 implicit val authorizer: Authorizer,
                                 implicit val futures: Futures,
                                 implicit val ec: ExecutionContext,
                                 implicit val cfg: play.api.Configuration)
    extends Security[CommonProfile]
    with Logging {
  protected val timeout = 15.seconds

  def metadata(aid: String) =
    Secure("BasicAuthClient").andThen(messagesAction)(parse.json).async {
      implicit request: Request[JsValue] =>
        val meta = request.body.as[Map[String, String]]
        mongodb
          .update[AnalysisEntry](
            mongodb.analyses,
            "aid",
            aid,
            t => t.copy(metadata = t.metadata ++ meta)
          )
          .withTimeout(timeout)
          .map { _ =>
            logger.info(s"Successfully marked analysis '$aid' as finished.")
            Accepted
          } recover {
          case e: NoSuchElementException => NotFound(e.getMessage)
          case e: scala.concurrent.TimeoutException =>
            GatewayTimeout("Request timeout while accessing mongodb")
          case NonFatal(e) => InternalServerError(e.getMessage)
        }
    }

  def success(aid: String) =
    Secure("BasicAuthClient").andThen(messagesAction).async {
      implicit request: Request[AnyContent] =>
        mongodb
          .update(
            mongodb.analyses,
            "aid",
            aid,
            Map(
              "tsEnd" -> System.currentTimeMillis(),
              "status" -> AnalysisState.Finished.name
            )
          )
          .withTimeout(timeout)
          .map { _ =>
            logger.info(s"Successfully marked analysis '$aid' as finished.")
            Accepted
          } recover {
          case e: NoSuchElementException => NotFound(e.getMessage)
          case e: scala.concurrent.TimeoutException =>
            GatewayTimeout("Request timeout while accessing mongodb")
          case NonFatal(e) => InternalServerError(e.getMessage)
        }
    }
}
