package controllers.forms

import play.api.data.Form

case class AnalysisForm(
                 tags: String = "", //Comma separated
                 config: String = ""
               )

object AnalysisForm {
  import play.api.data.Forms._

  val form = Form(
    mapping(
      "Analysis tags" -> nonEmptyText,
      "Config" -> text
    )(AnalysisForm.apply)(AnalysisForm.unapply)
  )
}
