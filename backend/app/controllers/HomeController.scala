package controllers

import javax.inject._
import modules.{Authorizer, DatasetFactory, MetakubeClient, MongodbClient}
import no.uit.sfb.scalautils.common.FutureUtils
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.Logging
import play.api.libs.concurrent.Futures._
import play.api.libs.concurrent.Futures
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject()(messagesAction: MessagesActionBuilder,
                               val controllerComponents: SecurityComponents,
                               mongodb: MongodbClient,
                               mk: MetakubeClient,
                               df: DatasetFactory,
                               implicit val authorizer: Authorizer,
                               implicit val futures: Futures,
                               implicit val cfg: play.api.Configuration,
                               implicit val ec: ExecutionContext)
    extends Security[CommonProfile]
    with Logging {
  protected val timeout = 15.seconds
  val authClient = cfg.get[String]("app.auth.users.client")
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
  def about() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.about())
  }

  def termsOfUse() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.tou())
  }

  def privacyPolicy() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.privacy())
  }

  def personalInfo() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.personalInfo())
  }

  def contact() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.contact())
  }

  def analyses() = Secure(authClient) { implicit request: Request[AnyContent] =>
    Ok(views.html.analyses(false))
  }

  def analysis(aid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin) {
      implicit request: Request[AnyContent] =>
        Ok(views.html.analysis(aid))
    }

  def stage(aid: String, sid: String) =
    Secure(authClient).andThen(authorizer.ownerOrAdmin) {
      implicit request: Request[AnyContent] =>
        Ok(views.html.stage(aid, sid))
    }

  def admin() = Secure(authClient).andThen(authorizer.admin) {
    implicit request: Request[AnyContent] =>
      Ok(views.html.analyses(true))
  }

  //Used to force login with a specific security mode
  def protectedIndex = Secure(authClient) {
    implicit request: Request[AnyContent] =>
      logger.info(s"${authorizer.user.id} logged in")
      Redirect(routes.HomeController.index())
  }

  def readinessProbe = Action.async { implicit request: Request[AnyContent] =>
    val mapF = Map(
      "mongodb" -> mongodb.checkClient(),
      "pachyderm" -> mk.check(),
      "minio" -> df.check()
    )
    mapF foreach {
      case (k, f) =>
        f onComplete{
          case Success(_) => //Nothing to do
          case Failure(e) =>
            logger.warn(s"Service $k not ready", e)
        }
    }
    FutureUtils.sequenceMap(mapF).withTimeout(timeout) map { m =>
      Ok(s"All services are ready (${m.keySet.mkString(",")})")
    } recover {
      case _ =>
        //No need to printout anything here since we already printed with the onComplete
        ServiceUnavailable(s"Some services are unavailable")
    }
  }

  def api() = Action { implicit request: Request[AnyContent] =>
    logger.info("Redirecting to swagger generated API")

    Redirect(
      s"http://${request.host}/docs/swagger-ui/index.html?url=/assets/swagger.json")
  }
}
