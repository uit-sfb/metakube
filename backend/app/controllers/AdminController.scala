package controllers

import javax.inject._
import modules.{Authorizer, MetakubeClient, MongodbClient}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.Logging
import play.api.libs.concurrent.Futures
import play.api.mvc._

@Singleton
class AdminController @Inject()(messagesAction: MessagesActionBuilder,
                                val controllerComponents: SecurityComponents,
                                mongodb: MongodbClient,
                                mk: MetakubeClient,
                                implicit val authorizer: Authorizer,
                                implicit val futures: Futures,
                                implicit val cfg: play.api.Configuration)
    extends Security[CommonProfile]
    with Logging {
  val authClient = cfg.get[String]("app.auth.users.client")

  def analyses() = Secure(authClient).andThen(authorizer.admin) {
    implicit request: Request[AnyContent] =>
      Ok(views.html.analyses(true))
  }
}
