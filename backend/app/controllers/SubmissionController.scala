package controllers

import java.io.InputStream

import akka.stream.scaladsl.StreamConverters
import akka.util.ByteString
import controllers.forms.AnalysisForm
import javax.inject.Inject
import models.{Analysis, Dataset}
import modules.{
  AnalysisSupervisor,
  Authorizer,
  DatasetFactory,
  Emailer,
  MetakubeClient,
  Workflow
}
import no.uit.sfb.apis.storage.ObjectId
import no.uit.sfb.metakube.wm.WorkflowManagerLike.WorkflowId
import no.uit.sfb.scalautils.common.{FutureUtils, Random}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.{Configuration, Logging}
import play.api.data.Form
import play.api.libs.streams.Accumulator
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc.{Action, MessagesActionBuilder}
import play.core.parsers.Multipart.{FileInfo, FilePartHandler}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

class SubmissionController @Inject()(
                                      messagesAction: MessagesActionBuilder,
                                      val controllerComponents: SecurityComponents,
                                      as: AnalysisSupervisor,
                                      emailer: Emailer,
                                      df: DatasetFactory,
                                      mk: MetakubeClient,
                                      workflow: Workflow,
                                      implicit val cfg: Configuration,
                                      implicit val ec: ExecutionContext,
                                      implicit val authorizer: Authorizer,
                                    ) extends Security[CommonProfile]
  with Logging {

  val authClient = cfg.get[String]("app.auth.users.client")

  def submission =
    Secure(authClient).andThen(messagesAction) { implicit request =>
      // Pass an unpopulated form to the template
      Ok(views.html.submission(workflow.pipeline, AnalysisForm.form))
    }

  case class F(wid: WorkflowId,
               branchName: String,
               data: Map[String, Seq[String]],
               fInputs: Future[Map[String, ObjectId]])

  def submitAnalysis: Action[F] = {
    Secure(authClient)
      .andThen(messagesAction)({
        val (wid, branchName) = Await.result(mk.initAnalysis(), 1.minute)
        val bp = parse.multipartFormData(forwardFileToPachyderm(wid))
        bp.map(x => {
          if (x.badParts.nonEmpty)
            logger.warn("Some files were probably empty (BadPart).")
          F(wid, branchName, x.dataParts, FutureUtils.sequenceMap((x.files map {
            fp =>
              fp.key -> fp.ref
          }).toMap))
        })
      })
      .async { implicit request =>
        val (wid, branchName, data, fInputs) = {
          val body = request.body
          (body.wid, body.branchName, body.data, body.fInputs)
        }
        val errorFunction = { formWithErrors: Form[AnalysisForm] =>
          //async
          mk.abortAnalysis(wid, branchName)
          Future.successful(
            BadRequest(
              views.html.submission(workflow.pipeline, formWithErrors)
            ))
        }

        val successFunction = { data: AnalysisForm =>
          val user = authorizer.user()
          fInputs flatMap { inputs =>
            val analysis = Analysis(
              Random.alphanumeric(10),
              data.tags
                .replace(' ', '_')
                .split(',')
                .map {
                  _.trim()
                }
                .toSet,
              inputs map { case (k, v) => k -> Dataset(v, s"input,$k") },
              data.config)
            logger.info(s"Received Analysis '$analysis'")
            as.save(wid, analysis, user) map { _ =>
              emailer.confirmation(analysis.id, user) //Async
              Redirect(routes.SubmissionController.submitted(analysis.id))
            }
          }
        }
        AnalysisForm.form
          .bindFromRequest(data)
          .fold(errorFunction, successFunction)
      }
  }

  /**
   * Directly forwarding to Pachyderm
   */
  private def forwardFileToPachyderm(
                                      wid: String): FilePartHandler[Future[ObjectId]] = {
    case FileInfo(partName, filename, contentType, dispositionType) =>
      val sink =
        StreamConverters.asInputStream().mapMaterializedValue(is => Future(is))
      val accumulator: Accumulator[ByteString, InputStream] = Accumulator(sink)
      accumulator.map { is =>
        val path = {
          val (pathwayName, inputName) = {
            val splt = partName.split('-')
            splt.init.mkString("-") -> splt.last
          }
          val path = workflow.pipeline
            .pathways.find(_.safeId == pathwayName).get
            .inputs.find(_.safeId == inputName).get
            .path
            .trim
          if (path.endsWith(".*")) {
            if (filename.nonEmpty && filename.split('.').nonEmpty) {
              path.init ++ filename.split('.').tail.mkString(".")
            } else
              path.take(path.length - 2)
          } else
            path
        }
        val f = mk.streamInput(is, wid, s"data/$path")
        FilePart(partName, path, contentType, f map { _ =>
          ObjectId.fromString(path)
        }, -1, dispositionType)
      }(ec)
  }

  def submitted(aid: String) = Secure(authClient) { implicit request =>
    Ok(views.html.submitted(aid))
  }

  def submissionFailed = Secure(authClient) { implicit request =>
    Ok(views.html.submissionFailed())
  }
}
