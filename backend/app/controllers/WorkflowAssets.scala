package controllers

import java.nio.file.{Path, Paths}

import javax.inject._
import no.uit.sfb.scalautils.common.FileUtils
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{Security, SecurityComponents}
import play.api.Logging
import play.api.mvc.{AnyContent, Request}

import scala.concurrent.ExecutionContext

@Singleton
class WorkflowAssets @Inject()(
                                implicit val cfg: play.api.Configuration,
                                val controllerComponents: SecurityComponents,
                                implicit val ec: ExecutionContext
                              )
  extends Security[CommonProfile]
    with Logging {
  val externalAssetPath = Paths.get(cfg.get[String]("app.pipeline.workflowPath")).resolve("assets")
  def at(file: String) = Action { implicit request: Request[AnyContent] =>
    val oPathWithExt: Option[Path] = {
      val path = externalAssetPath.resolve(file)
      if (file.endsWith(".*")) {
        val (_, files) = FileUtils.ls(path.getParent)
        files.find{f => f.toString.startsWith(path.toString.init) }
      } else
        Some(path)
    }
    oPathWithExt match {
      case Some(p) => Ok.sendFile(p.toFile, true)
      case None => NotFound
    }
  }
}
