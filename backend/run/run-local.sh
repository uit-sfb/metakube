THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

POSITIONAL=()
PROFILE=metakubeTest
WORKFLOW_SCRIPT=""
REDEPLOY=""
while [[ $# -gt 0 ]]
do
  case "$1" in
      -h|--help)
      echo "Publish file to artifactory"
      echo ""
      echo "Usage"
      echo "--redeploy If set, delete namespace if exists (Default: false)"
      echo "--profile minishift profile (Default: metakubeTest)"
      echo "--workflow-script <pathToWorkflowScript>"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
      --redeploy)
      REDEPLOY="--redeploy"
      shift
      ;;
      --workflow-script)
      WORKFLOW_SCRIPT=$2
      shift
      shift
      ;;
    --profile)
      PROFILE=$2
      shift
      shift
      ;;
      *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

minishift start --profile $PROFILE --memory 12g --cpus 12 --disk-size 12g --profile metakube-local
minishift profile set $PROFILE #In case the vm already existed and the profile got changed, we need to re-set the right profile
oc login -u system:admin
oc delete ns metakube || true
oc new-project metakube
./${WORKFLOW_SCRIPT} deploy -c $THIS_PATH/config.yaml -y

CLUSTER_IP=$(minishift ip)
export MK_PACHYDERM_ENDPOINT=$CLUSTER_IP:30650
export MK_S3_URL=http://$CLUSTER_IP:31311
export MK_S3_BUCKET=$(oc get secret metakube-s3-backend -o yaml | yq r - data.bucket | base64 -d)
export MK_S3_ACCESS_KEY=$(oc get secret metakube-s3-backend -o yaml | yq r - data.accesskey | base64 -d)
export MK_S3_SECRET_KEY=$(oc get secret metakube-s3-backend -o yaml | yq r - data.secretkey | base64 -d)
export MK_MONGODB_ENDPOINT=$CLUSTER_IP:30017
export MK_MONGODB_USER=$(oc get secret metakube-mongodb -o yaml | yq r - data.user | base64 -d)
export MK_MONGODB_PASSWORD=$(oc get secret metakube-mongodb -o yaml | yq r - data.mongodb-password | base64 -d)
export MK_MONGODB_DATABASE=$(oc get secret metakube-mongodb -o yaml | yq r - data.database | base64 -d)
export MK_BACKEND_EXTERNAL_URL="http://localhost:9000"
cd  $THIS_PATH/..
sbt clean reload update run
