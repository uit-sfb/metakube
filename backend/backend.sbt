import java.nio.file.Paths

import com.typesafe.sbt.packager.docker._
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.Level._
import scala.io.Source

name := "mk-backend"
organization := "no.uit.sfb"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

resolvers ++= {
  Seq(Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if (version.value.endsWith("-SNAPSHOT"))
      Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

lazy val root = (project in file("."))

enablePlugins(PlayScala, SwaggerPlugin)

scalaVersion := "2.12.8"
val pac4jVersion = "3.8.3"
val playVersion = "2.8.0"
val scalaUtilsVer = "0.2.1"

libraryDependencies ++= Seq(
  guice,
  caffeine,
  ws,
  "org.webjars" % "swagger-ui" % "3.24.3",
  "com.typesafe.play" %% "play-mailer" % "7.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "7.0.1",
  "no.uit.sfb" %% "mk-drivers" % version.value,
  "no.uit.sfb" %% "scala-utils-s3store" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVer,
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test,
  "org.pac4j" % "pac4j-http" % pac4jVersion,
  //"no.uit.sfb" %% "forseti-client-scala" % "0.1.1",
  "com.typesafe.play" %% "play-json" % playVersion, //needed at runtime for SwaggerPlugin
  "org.apache.shiro" % "shiro-core" % "1.4.2", //needed at runtime by SecurityModule
  "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io" , "commons-io"),
  "org.mongodb" % "mongodb-driver-sync" % "3.12.0",
  "org.pac4j" %% "play-pac4j" % "8.0.2",
  "org.pac4j" % "pac4j-mongo" % pac4jVersion,
  "com.typesafe" % "config" % "1.4.0"
)

swaggerDomainNameSpaces := Seq("models")

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../targetVersion"

val pubDockerRepository = "registry.gitlab.com"
val pubDockerUsername = "uit-sfb/metakube"
val pachydermVersion = Source.fromFile("../pachydermVersion").mkString.trim()

dockerLabels := Map("gitCommit" -> s"${git.formattedShaVersion.value.getOrElse("unknown")}@${git.gitCurrentBranch.value}")
dockerRepository in Docker := Some(pubDockerRepository)
dockerUsername in Docker := Some(pubDockerUsername)
daemonUser in Docker := "sfb"
//daemonGroup in Docker := "0"
//dockerChmodType := DockerChmodType.UserGroupWriteExecute //Doesn't work for some reason
dockerCommands in Docker := dockerCommands.value
  .flatMap{
  case c @ Cmd("RUN", args @ _*) if args.head == "id" && args.tail.head == "-u" =>
    Seq(
      c,
      ExecCmd("RUN", "bash", "-c", s"curl -o /tmp/pachctl.deb -L https://github.com/pachyderm/pachyderm/releases/download/v${pachydermVersion}/pachctl_${pachydermVersion}_amd64.deb && dpkg -i /tmp/pachctl.deb"),
      Cmd("WORKDIR", "/usr/sbin/.pachyderm"),
      Cmd("RUN", s"chown ${(Docker / daemonUser).value}:${(Docker / daemonGroup).value} ."),
    )
  case c => Seq(c)
}.flatMap{ cmd =>
  val pf: PartialFunction[CmdLike, Boolean] = { case ExecCmd("ENTRYPOINT", _) => true }
  if (pf.lift(cmd).getOrElse(false))
    Seq(
      Cmd("ENV", "HOME", s"/home/${(Docker / daemonUser).value}"), //For some reason HOME is not set! So we need to do it manually.
      Cmd("RUN", "chmod -R g+w $HOME"),
    ) ++ Seq(cmd)
  else Seq(cmd)}

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion,
  git.formattedShaVersion
)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "no.uit.sfb.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "no.uit.sfb.binders._"

