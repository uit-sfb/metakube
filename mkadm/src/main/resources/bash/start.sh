#!/usr/bin/env bash

MK_UUID=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 6 | head -n 1)
export MK_RUN_UUID=${PPS_POD_NAME}:$MK_UUID

mkdir -p "$MK_OUT"
mkdir -p "$MK_META"
echo "*" > "$MK_META/touch"
#As long as no previous pipeline failed, /data and /meta are always created.
#Connecting to / should only be done if you want to execute always (since /meta contains aid)
#Connecting to /data will execute if /data has not been processed already, but it includes cases were the pipeline before had no datum (except the MK_CONTEXT datum which is always there)
#To make the distinction between a job that had datum and one that had not, look at /meta/uuids. If it is there at least one datum was processed.

#We make sure tmp is empty. The reason this is needed is that even though we use an emptydir
#pachyderm may run multiple datums back to back without restarting the pod hence leaving data in the temp dir
rm -rf "$MK_TMP/*"
cd "$MK_TMP"

if [[ -z "$MK_CONTEXT" ]]; then
  #This one was working fine except that due to the pipe, the exit code of logic.sh was ignored. Using set -o pipefail is not a good idea du to SIGPIPE error (141).
  #{ /opt/docker/bin/logic.sh 2>&1 1>&3 3>&- | while read line; do echo "$(date "+%Y-%m-%d_%T.%N")[$MK_RUN_UUID][error] $line"; done; } 3>&1 1>&2 | while read line; do echo "$(date "+%Y-%m-%d_%T.%N")[$MK_RUN_UUID] $line"; done
  /opt/docker/bin/logic.sh 2> >(while read line; do echo "$(date "+%Y-%m-%d_%T.%N")[$MK_RUN_UUID][error] $line"; done) > >(while read line; do echo "$(date "+%Y-%m-%d_%T.%N")[$MK_RUN_UUID] $line"; done)
fi
