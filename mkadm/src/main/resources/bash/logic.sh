#!/usr/bin/env bash

set -e

echo "$MK_RUN_UUID" > "$MK_META/uuids"

source "$ENV_SCRIPT"

echo ">>> Executing..."

#bash takes stdin as input by default
/usr/bin/time -f %P,%S,%U,%e,%M,%W,%c,%w -o /pfs/out/meta/time bash -e
_RES=$?
if [[ $_RES != 0 ]]; then
  echo "User code returned with status $_RES"
  exit $_RES
else
  echo ">>> Done"
fi
