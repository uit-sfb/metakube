package no.uit.sfb.metakube.mkadm

import java.nio.file.Paths

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.mkadm.analysis.{AnalysisActions, AnalysisConfig, AnalysisParser}
import no.uit.sfb.metakube.mkadm.pipeline.{PipelineActions, PipelineConfig, PipelineParser}
import scopt.OParser
import no.uit.sfb.info.mkadm.BuildInfo
import no.uit.sfb.metakube.MetakubeDriver
import no.uit.sfb.metakube.mkadm.build.{BuildConfig, BuildParser, Builder}
import no.uit.sfb.metakube.mkadm.build.BuildParser.parser
import no.uit.sfb.metakube.mkadm.workflow.{WorkflowAction, WorkflowConfig, WorkflowParser}
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.PachydermDriver
import no.uit.sfb.scalautils.common.FileUtils

import scala.concurrent.{Await, ExecutionContext}
import scala.util.control.NonFatal
import scala.concurrent.duration._

object Main extends App with LazyLogging {
  val name = BuildInfo.name
  val ver = BuildInfo.version
  val gitCommitId = BuildInfo.formattedShaVersion

  def parseEndpoint(str: String): (String, Int) = {
    str.split(':').toList match {
      case host :: portStr :: Nil =>
        if (host.isEmpty || host.contains('/'))
          throw new Exception(
            s"pachd endpoint cannot be parsed from '$str'. The host contains invalid characters or is empty.")
        val port = try { portStr.toInt } catch {
          case _: NumberFormatException =>
            throw new Exception(
              s"pachd endpoint cannot be parsed from '$str'. The port is not an integer.")
        }
        host -> port
      case _ =>
        throw new Exception(
          s"pachd endpoint cannot be parsed from '$str'. Expected exactly one ':'.")
    }
  }

  implicit val ec = ExecutionContext.global

  try {
    val mainBuilder = OParser.builder[String]

    val parserMain = {
      import mainBuilder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"git: ${gitCommitId.getOrElse("unknown")}"),
        help('h', "help")
          .text("prints this usage text"),
        version('v', "version")
          .text("prints the version"),
        cmd("build")
          .action((_, _) => "build")
          .text("Build the workflow"),
        cmd("workflow")
          .action((_, _) => "workflow")
          .text("Workflow management (deployment/tearing down)"),
        cmd("pipeline")
          .action((_, _) => "pipeline")
          .text("Pipeline management"),
        cmd("analysis")
          .action((_, _) => "analysis")
          .text("Analysis management"),
        cmd("init")
          .action((_, _) => "init")
          .text("Initialize pachyderm driver and wait for pachd to be ready")
      )
    }

    (OParser.parse(parserMain, args.headOption.toSeq, ""),
     if (args.nonEmpty) args.tail else Array("-h")) match {
      case (Some("pipeline"), arg) =>
        OParser.parse(PipelineParser.parser, arg, PipelineConfig()) match {
          case Some(config) => PipelineActions(config)
          case _            => throw new IllegalArgumentException()
        }
      case (Some("analysis"), arg) =>
        OParser.parse(AnalysisParser.parser, arg, AnalysisConfig()) match {
          case Some(config) => AnalysisActions(config)
          case _            => throw new IllegalArgumentException()
        }
      case (Some("init"), arg) =>
        OParser.parse(PipelineParser.parser, arg, PipelineConfig()) match {
          case Some(config) =>
            logger.info(
              s"Using pachd endpoint ${config.pachHost}:${config.pachPort}")
            val pd = new PachydermDriver(config.pachHost, config.pachPort)
            lazy val md = new MetakubeDriver(
              new PachydermWorkflowManagerImpl(pd)
            )
            Await.result(md.check(-1), 1.hour)
          case _ => throw new IllegalArgumentException()
        }
      case (Some("build"), arg) =>
        OParser.parse(BuildParser.parser, arg, BuildConfig()) match {
          case Some(c) =>
            val builder = new Builder(c)
            builder.buildWrappers()
            val archive = builder.buildWorkflow()
            val imgName = builder.buildImage(archive)
            val scriptPath = archive.getParent.resolve(archive.getFileName.toString.split('.').head + ".sh")
            builder.writeWrapperScript(imgName, scriptPath)
          case _ =>
            // arguments are bad, error message will have been displayed
            throw new IllegalArgumentException()
        }
      case (Some("workflow"), arg) =>
        OParser.parse(WorkflowParser.parser, arg, WorkflowConfig()) match {
          case Some(c) =>
            c.cmd match {
              case "default-config" =>
                new WorkflowAction(c).defaultConfig()
              case "deploy" =>
                new WorkflowAction(c).deploy()
              case "hibernate" =>
                new WorkflowAction(c).hibernate()
              case "wakeup" =>
                new WorkflowAction(c).wakeup()
              case "delete" =>
                new WorkflowAction(c).delete()
              case "destroy" =>
                new WorkflowAction(c).destroy()
              case _ =>
                // arguments are bad, error message will have been displayed
                throw new IllegalArgumentException()
            }
          case _ =>
            // arguments are bad, error message will have been displayed
            throw new IllegalArgumentException()
        }
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case NonFatal(e) =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
