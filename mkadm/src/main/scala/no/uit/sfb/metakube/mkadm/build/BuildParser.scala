package no.uit.sfb.metakube.mkadm.build

import java.io.File

import scopt.OParser

object BuildParser {
  val builder = OParser.builder[BuildConfig]
  val parser = {
    import builder._
    OParser.sequence(
      head("Build Metakube workflow"),
      help('h', "help")
        .text("prints this usage text"),
      opt[Map[String, String]]("subst")
        .valueName("k1=v1,k2=v2...")
        .action((v, c) => c.copy(subst = v))
        .text("Substitute '{{k1}}' with 'v1', etc in workflow.yaml and images"),
      opt[Unit]("force")
        .action((_, c) => c.copy(force = true))
        .text("Override images already pushed"),
      arg[File]("path")
        .action((p, c) => c.copy(path = p.toPath))
        .text("Path to workflow directory"),
    )
  }
}
