package no.uit.sfb.metakube.mkadm.analysis

import java.io.File

import scopt.OParser
import no.uit.sfb.metakube.mkadm.Main._

object AnalysisParser {

  lazy val builder = OParser.builder[AnalysisConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      help('h', "help")
        .text("prints this usage text"),
      cmd("submit")
        .action((_, c) => c.copy(cmd = "submit"))
        .text("Submit analysis")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[File]("config")
            .required()
            .action((x, c) => c.copy(configPath = x.toPath))
            .text("Path to runtime config file (yaml)"),
          arg[Map[String, File]]("inputs")
            .optional()
            .valueName("name1=path1,name2=path2,...")
            .action((x, c) => c.copy(inputs = x.mapValues { _.toPath }))
            .text("Path to inputs"),
        ),
      cmd("status")
        .action((_, c) => c.copy(cmd = "status"))
        .text("Submit analysis")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("commit")
            .required()
            .action((x, c) => c.copy(commitId = x))
            .text("Commit id (yyyyyy in 'commit ref: xxx@yyyyyy')")
        ),
      /*cmd("cancel")
        .action((_, c) => c.copy(cmd = "cancel"))
        .text("Cancel analysis")
        .children(
          opt[String]("pachd")
.required()
            .action((x, c) => c.copy(pachEndpoint = x))
            .text("Pachyderm endpoint"),
          opt[String]("commit")
            .required()
            .action((x, c) => c.copy(commitId = x))
            .text("Commit id (yyyyyy in 'commit ref: xxx@yyyyyy')")
        ),*/
      cmd("attach")
        .action((_, c) => c.copy(cmd = "attach"))
        .text("Follow analysis")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("commit")
            .required()
            .action((x, c) => c.copy(commitId = x))
            .text("Commit id (yyyyyy in 'commit ref: xxx@yyyyyy')")
        ),
      cmd("logs")
        .action((_, c) => c.copy(cmd = "logs"))
        .text("Print logs for analysis/stage")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("commit")
            .required()
            .action((x, c) => c.copy(commitId = x))
            .text("Commit id (yyyyyy in 'commit ref: xxx@yyyyyy')"),
          opt[String]("stage")
            .required()
            .action((x, c) => c.copy(stage = x))
            .text(
              "Stage id (run 'pipeline status' to display the list of stages)"),
          opt[Unit]('f', "follow")
            .action((x, c) => c.copy(follow = true))
            .text("Follow log")
        ),
      cmd("download")
        .action((_, c) => c.copy(cmd = "download"))
        .text("Download output files")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("commit")
            .required()
            .action((x, c) => c.copy(commitId = x))
            .text("Commit id (yyyyyy in 'commit ref: xxx@yyyyyy')"),
          opt[File]("download-path")
            .required()
            .action((x, c) => c.copy(downloadPath = x.toPath))
            .text("Path to download dir")
        )
    )
  }
}
