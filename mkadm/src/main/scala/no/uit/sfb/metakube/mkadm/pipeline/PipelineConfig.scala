package no.uit.sfb.metakube.mkadm.pipeline

case class PipelineConfig(
    cmd: String = "",
    pachHost: String = "",
    pachPort: Int = 0,
    matching: String = ""
)
