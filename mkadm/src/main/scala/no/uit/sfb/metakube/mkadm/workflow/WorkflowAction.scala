package no.uit.sfb.metakube.mkadm.workflow

import java.nio.file.Paths

import com.fasterxml.jackson.annotation.JsonCreator
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.{FileUtils, SystemProcess}
import no.uit.sfb.scalautils.yaml.Yaml

import scala.io.StdIn
import scala.util.control.NonFatal

class WorkflowAction(c: WorkflowConfig) extends LazyLogging {
  val currentContext = {
    val currCtx = if (c.dry)
      ""
    else
      SystemProcess("kubectl config current-context").execF(_.head)
    if (!c.yes) {
      val inp = StdIn.readLine(s"Is this the right context: '$currCtx'?  [y/N]").toLowerCase
      if (!Seq("y", "yes").contains(inp)) {
        println(s"Expected 'y' or 'yes', but answer was '$inp'.")
        println(s"Please select the right context before deploying.")
        sys.exit(1)
      }
    }
    println(s"Using context '$currCtx'")
    if (SystemProcess("kubectl get pods").execQ()._1 != 0) {
      println(
        s"""
           |Could not access cluster:
           |  - Is this the right context ('$currCtx')?
           |  - Are you logged in?
           |  - Does your context has an associated namespace? If not run 'kubectl config set-context <context> --namespace=<namespace>'
           |  - Does the namespace exist?
           |  - Do you have sufficient rights on this namespace?
           |""".stripMargin)
      sys.exit(1)
    }
    currCtx
  }


  def defaultConfig(): Unit = {
    val tmp = Paths.get(".")
    FileUtils.deleteFileIfExists(tmp)
    FileUtils.unzip(c.path, tmp)
    println(FileUtils.readFile(tmp.resolve("values.yaml")))
  }

  def deploy(): Unit = {
    if (c.udatePipeline && !c.yes) {
      val inp = StdIn.readLine("Are you sure you want to update the pipeline (this will result in running jobs being killed)? [y/N]").toLowerCase
      if (!Seq("y", "yes").contains(inp)) {
        println(s"Expected 'y' or 'yes', but answer was '$inp'")
        sys.exit(1)
      }
    }
    val targetPath = Paths.get("target")
    val k8sObjectsPath = targetPath.resolve("deployment.yaml")
    val pipelineVersion = if (c.dry)
      1
    else {
      val latestVersion: Int = {
        val reg = """deploy-pipeline-u([0-9]+)-.*""".r
        val s = try {
          SystemProcess("""kubectl get job -o jsonpath="{.items[].metadata.name}"""").execF(identity).flatMap {
            case reg(v) => Some(v.toInt)
            case _ => None
          }
        } catch {
          case NonFatal(e) => Seq()
        }
        if (s.isEmpty)
          1
        else
          s.max
      }
      if (c.udatePipeline)
        latestVersion + 1
      else
        latestVersion
    }

    if (!FileUtils.exists(c.config)) {
      FileUtils.createParentDirs(c.config)
      FileUtils.writeToFile(c.config, "")
    }


    val chartPath = {
      val decompressedPath = targetPath.resolve("decompressed")
      FileUtils.deleteDirIfExists(decompressedPath)
      FileUtils.createDirs(decompressedPath)
      FileUtils.unTgz(c.path, decompressedPath)
      val chartP = FileUtils.ls(decompressedPath)._1.head
      val conf = Yaml.fromFileAs[Conf](c.config)
      if (!conf.pachyderm.enabled) {
        FileUtils.deleteDir(chartP.resolve("charts/metakube/charts/pachyderm"))
      }
      if (!conf.minio.enabled) {
        FileUtils.deleteDir(chartP.resolve("charts/metakube/charts/minio"))
      }
      if (!conf.mongodb.enabled) {
        FileUtils.deleteDir(chartP.resolve("charts/metakube/charts/mongodb"))
      }
      chartP
    }

    val clusterIp = {
      def unquote(str: String): String = {
        val tmp = if (str.head == '"') str.tail else str
        if (tmp.last == '"') tmp.init else tmp
      }

      val clusterName = SystemProcess(s"""kubectl config view -o jsonpath="{.contexts[?(@.name==\"$currentContext\")].context.cluster}"""").execF(res => unquote(res.head))
      val serverUrl: String = SystemProcess(s"""kubectl config view -o jsonpath="{.clusters[?(@.name==\"$clusterName\")].cluster.server}"""").execF(res => unquote(res.head))
      serverUrl.split(':').tail.head.replace("/", "")
    }
    val version = {
      Yaml.fromFileAs[V](chartPath.resolve("Chart.yaml")).version
    }
    val objectsRaw: String = SystemProcess(s"""helm template metakube $chartPath --version $version -f ${c.config} --set "pipeline.update=$pipelineVersion,clusterIp=$clusterIp""").execF(_.mkString("\n"))
    val reg = """^( *MC_CONFIG_DIR)=".*"""".r
    val objects = (objectsRaw.split('\n') map {
      case reg(header) =>
        s"""$header="/tmp/mc""""
      case s => s
    }) mkString "\n"
    FileUtils.writeToFile(k8sObjectsPath, objects)
    if (c.dry)
      println(objects)
    else {
      val reg = "kind: *Route".r
      val cmd = if (objects.split('\n').exists {
        case reg() => true
        case _ => false
      }) //Contains routes
      "oc"
      else "kubectl"
      //Apply assets
      val assets = FileUtils.filesUnder(chartPath.resolve("pipeline/assets")).map { p => s"--from-file=$p" }
      SystemProcess(s"$cmd delete cm metakube-pipeline-assets").execFO(_ => "")
      SystemProcess(s"$cmd create cm metakube-pipeline-assets ${assets.mkString(" ")}").exec()

      //Apply everything else
      SystemProcess(s"$cmd apply -f $k8sObjectsPath").exec()
    }
  }

  val selector = "--selector app.kubernetes.io/instance=metakube,app.kubernetes.io/managed-by=Helm"

  def hibernate(): Unit = {
    SystemProcess("kubectl scale deploy --replicas=0 --all").exec()
  }

  def wakeup(): Unit = {
    SystemProcess("kubectl scale deploy --replicas=1 --all").exec()
  }

  def delete(): Unit = {
    val resources = SystemProcess(s"kubectl get all -o name $selector").execF(_.mkString("\n"))
    println(s"The following resources will be deleted permanently on context: '$currentContext':")
    println(resources)
    if (!c.yes) {
      val inp = StdIn.readLine(s"Are you sure you want to delete permanently all compute resources on the context '$currentContext' (data and secrets will be preserved)? [y/N]").toLowerCase
      if (!Seq("y", "yes").contains(inp)) {
        println(s"Expected 'y' or 'yes', but answer was '$inp'.")
        sys.exit(1)
      }
      SystemProcess(s"kubectl delete all $selector").exec()
    }
  }

  def destroy(): Unit = {
    val resources = SystemProcess(s"kubectl get all,configmap,pvc,secret -o name $selector").execF(_.mkString("\n"))
    println(s"The following resources will be deleted permanently on context: '$currentContext':")
    println(resources)
    if (!c.yes) {
      val inp = StdIn.readLine(s"Are you sure you want to delete permanently all resources and data on the context '$currentContext' (data and secrets will be preserved)'? [y/N]").toLowerCase
      if (!Seq("y", "yes").contains(inp)) {
        println(s"Expected 'y' or 'yes', but answer was '$inp'.")
        sys.exit(1)
      }
      SystemProcess(s"kubectl delete all,configmap,pvc,secret $selector").exec()
    }
  }
}

case class Enabled(enabled: Boolean = true) {
  @JsonCreator
  def this() = this(true)
}

case class Conf(mongodb: Enabled = Enabled(), minio: Enabled = Enabled(), pachyderm: Enabled = Enabled()) {
  @JsonCreator
  def this() = this(Enabled(), Enabled(), Enabled())
}

case class V(version: String)
