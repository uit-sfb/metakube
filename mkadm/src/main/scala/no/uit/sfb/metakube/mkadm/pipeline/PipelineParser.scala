package no.uit.sfb.metakube.mkadm.pipeline

import java.nio.file.Paths

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser
import no.uit.sfb.metakube.mkadm.Main._

object PipelineParser extends LazyLogging {

  lazy val builder = OParser.builder[PipelineConfig]

  lazy val kaijuDbs = Seq("mardb", "refseq", "")
  lazy val mapseqDbs = Seq("silva", "silvamar", "")

  val basePath = Paths.get(sys.env("HOME")).resolve("pipeline")
  val templatesPath = basePath.resolve("templates")
  val configOverridePath = basePath.resolve("configOverride")

  lazy val parser = {
    import builder._
    OParser.sequence(
      help('h', "help")
        .text("prints this usage text"),
      cmd("stage")
        .action((_, c) => c.copy(cmd = "stage"))
        .text("Print translation of templates definition")
        .children(
          opt[String]("matching")
            .action((x, c) => c.copy(matching = x))
            .text("Only pipelines containing this sub-string"),
        ),
      cmd("deploy")
        .action((_, c) => c.copy(cmd = "deploy"))
        .text("Deploy pipeline")
        .text(s"Mount the templates and config override inside $templatesPath")
        .text(s"Mount the runtime config override inside $configOverridePath")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("matching")
            .action((x, c) => c.copy(matching = x))
            .text("Only pipelines containing this sub-string"),
        ),
      cmd("update")
        .action((_, c) => c.copy(cmd = "update"))
        .text("Update pipeline")
        .text(s"Mount the templates and config override inside $templatesPath")
        .text(s"Mount the runtime config override inside $configOverridePath")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("matching")
            .action((x, c) => c.copy(matching = x))
            .text("Only stages containing this sub-string"),
        ),
      cmd("apply")
        .action((_, c) => c.copy(cmd = "apply"))
        .text("Create workflow stages or update if already exists (declarative version of 'deploy' = update)")
        .text(s"Mount the templates and config override inside $templatesPath")
        .text(s"Mount the runtime config override inside $configOverridePath")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("matching")
            .action((x, c) => c.copy(matching = x))
            .text("Only stages containing this sub-string"),
        ),
      cmd("undeploy")
        .action((_, c) => c.copy(cmd = "undeploy"))
        .text("Tear down pipeline")
        .text(s"Mount the templates and config override inside $templatesPath")
        .text(s"Mount the runtime config override inside $configOverridePath")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint"),
          opt[String]("matching")
            .action((x, c) => c.copy(matching = x))
            .text("Only pipelines containing this sub-string"),
        ),
      cmd("status")
        .action((_, c) => c.copy(cmd = "status"))
        .text("Print list of deployed repos")
        .children(
          opt[String]("pachd")
            .required()
            .action((x, c) => {
              val (host, port) = parseEndpoint(x)
              c.copy(pachHost = host, pachPort = port)
            })
            .text("Pachyderm endpoint")
        )
    )
  }
}
