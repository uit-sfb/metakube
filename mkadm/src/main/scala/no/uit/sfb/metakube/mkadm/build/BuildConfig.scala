package no.uit.sfb.metakube.mkadm.build

import java.nio.file.{Path, Paths}

case class BuildConfig(path: Path = Paths.get("."),
                       subst: Map[String, String] = Map(),
                       force: Boolean = false)
