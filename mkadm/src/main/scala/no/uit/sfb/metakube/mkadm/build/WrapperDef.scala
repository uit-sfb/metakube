package no.uit.sfb.metakube.mkadm.build

import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.yaml.Yaml

class WrapperDef(val key: String,
                      mic: WrapperConf) {
  lazy val script = s"""#!/bin/bash
                       |${mic.appScript}""".stripMargin

  lazy val dockerfileContent: String = {
    s"""
       |FROM ${mic.parentImage}
       |USER root
       |RUN id -u sfb 2> /dev/null || adduser -D -G root sfb || useradd -m -g root sfb
       |RUN apk --no-cache add bash coreutils ${mic.packages.mkString(" ")} || { apt update && apt install time ${mic.packages.mkString(" ")} -y && rm -rf /var/lib/apt/lists/* ; }
       |WORKDIR /opt/docker
       |COPY --chown=sfb:root *.sh bin/
       |RUN chmod +x bin/*.sh
       |USER sfb
       |WORKDIR /home/sfb
       |ENTRYPOINT ["/opt/docker/bin/start.sh"]
       |CMD []
       |""".stripMargin
  }
}

object WrapperDef {
  def fromFile(path: Path, subst: Map[String, String]): WrapperDef = {
    val wName = path.getFileName.toString.split('.').head
    val rawConf = FileUtils.readFile(path)
    val substConf = subst.foldLeft[String](rawConf){ case (acc, (k, v)) =>
      acc.replaceAll(s"""\\{\\{$k\\}\\}""", v)
    }
    val mic = Yaml.parse[WrapperConf](substConf)
    new WrapperDef(wName, mic)
  }
}
