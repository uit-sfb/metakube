package no.uit.sfb.metakube.mkadm.workflow

import java.io.File

import no.uit.sfb.scalautils.common.FileUtils
import scopt.OParser

object WorkflowParser {
  val builder = OParser.builder[WorkflowConfig]
  val parser = {
    val fromDockerImage = FileUtils.exists(WorkflowConfig().path)
    import builder._
    OParser.sequence(
      head("Workflow deployment manager"),
      help('h', "help")
        .text("prints this usage text"),
      cmd("default-config")
        .text("Prints default workflow config")
        .action((_, c) => c.copy(cmd = "default-config"))
        .children(
          arg[File]("path")
            .action((p, c) => c.copy(path = p.toPath))
            .text("Path to workflow chart")
        ),
      cmd("deploy")
        .text("Deploy Metakube workflow")
        .action((_, c) => c.copy(cmd = "deploy"))
        .children(
          opt[File]('c', "config")
            .action((p, c) => c.copy(config = p.toPath))
            .text(s"Path to config file (Default: ${WorkflowConfig().config})"),
          opt[Unit]("dry")
            .action((p, c) => c.copy(dry = true))
            .text("Dry run (only prints in stdout)"),
          opt[Unit]('y', "yes")
            .action((p, c) => c.copy(yes = true))
            .text("Do not asks for confirmation"),
          opt[Unit]("update-pipeline")
            .action((_, c) => c.copy(udatePipeline = true))
            .text("If set the pipeline is updated. Attention, this would result in running jobs being killed!"),
          arg[File]("path")
            .optional()
            .action((p, c) => c.copy(path = p.toPath))
            .text(s"Path to workflow chart (Default: ${WorkflowConfig().path})")
            .text(s"${if (fromDockerImage) "You are running a docke rimage already containing the workflow definition. Please ignore this field." else "Compulsory"}")
        ),
      cmd("hibernate")
        .text("Scale all replicas to 0")
        .action((_, c) => c.copy(cmd = "hibernate")),
      cmd("wakeup")
        .text("Scale all replicas to 1")
        .action((_, c) => c.copy(cmd = "wakeup")),
      cmd("delete")
        .text("Delete all computing resources (deployment, pods, services, ...). Data are preserved")
        .action((_, c) => c.copy(cmd = "delete")),
      cmd("destroy")
        .text("Delete all resources including secrets and data")
        .action((_, c) => c.copy(cmd = "destroy"))
    )
  }
}
