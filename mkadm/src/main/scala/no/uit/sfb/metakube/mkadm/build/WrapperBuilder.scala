package no.uit.sfb.metakube.mkadm.build

import java.nio.file.Paths

import com.github.blemale.scaffeine.Scaffeine
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.{FileUtils, SystemProcess}

import scala.io.Source
import scala.util.{Failure, Success, Try}

class WrapperBuilder(fullImageName: String => String,
                     version: String,
                     wrappers: Set[WrapperDef],
                     force: Boolean) extends LazyLogging {
  SystemProcess
    .from(
      Seq(
        "bash",
        "-c",
        s"""if ! grep -q experimental ${sys
          .env("HOME")}/.docker/config.json; then sed -i '$$s/}/,\n"experimental": "enabled"}/' ${sys
          .env("HOME")}/.docker/config.json; fi"""
      ))
    .execF(_ => "")

  protected val exsistCache =
    Scaffeine().build[String, Boolean]((imageName: String) =>
      Try {
        SystemProcess(s"docker manifest inspect $imageName").exec()
      } match {
        case Success(_) => true
        case Failure(_) => false
    })

  def buildAll(): Unit = {
    wrappers foreach { wrapper =>
      buildWrapper(wrapper)
    }
  }

  def buildWrapper(wrapperDef: WrapperDef): Unit = {
      val imgName = fullImageName(wrapperDef.key)
      val ctxDir = Paths.get(s"target/docker/${wrapperDef.key}")
      println(s"Writing to $ctxDir")
      FileUtils.deleteDirIfExists(ctxDir)
      FileUtils.createDirs(ctxDir)
      FileUtils.writeToFile(
        ctxDir.resolve("start.sh"),
        Source.fromResource("bash/start.sh").getLines.mkString("\n"))
      FileUtils.writeToFile(
        ctxDir.resolve("logic.sh"),
        Source.fromResource("bash/logic.sh").getLines.mkString("\n"))
      FileUtils.writeToFile(ctxDir.resolve("app.sh"),
        wrapperDef.script)
      FileUtils.writeToFile(ctxDir.resolve("dockerfile"), wrapperDef.dockerfileContent)
      if (force || !existsWrapper(wrapperDef))
        SystemProcess(
          s"docker build -t $imgName --build-arg version=$version .",
          path = Some(ctxDir)).exec()
  }

  def pushAll(): Unit = {
    wrappers foreach { wrapper =>
      pushWrapper(wrapper)
    }
  }

  def pushWrapper(wrapperDef: WrapperDef): Unit = {
      if (force || !existsWrapper(wrapperDef)) {
        val imageName = fullImageName(wrapperDef.key)
        if (exsistCache.get(imageName))
          println(s"Overriding image $imageName")
        SystemProcess(s"""docker push $imageName""").exec()
      }
  }

  protected def existsWrapper(wrapperDef: WrapperDef): Boolean = {
    val imageName = fullImageName(wrapperDef.key)
    val res = exsistCache.get(imageName)
    if (res)
      println(s"$imageName already exists")
    res
  }
}

object WrapperBuilder {
  def fromWorkflowConf(wConf: WorkflowConf, wrappers: Set[WrapperDef], force: Boolean): WrapperBuilder = {
    new WrapperBuilder(wConf.fullImageName, wConf.version, wrappers, force)
  }
}