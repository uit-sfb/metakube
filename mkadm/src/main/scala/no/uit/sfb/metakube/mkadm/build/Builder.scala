package no.uit.sfb.metakube.mkadm.build

import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.{FileUtils, SystemProcess}
import no.uit.sfb.scalautils.http.HttpClient
import no.uit.sfb.scalautils.yaml.Yaml

class Builder(c: BuildConfig) extends LazyLogging {

  lazy val wConf: WorkflowConf = {
    val rawConf = FileUtils.readFile(c.path.resolve("workflow.yaml"))
    val substConf = c.subst.foldLeft[String](rawConf){ case (acc, (k, v)) =>
      acc.replaceAll(s"""\\{\\{$k\\}\\}""", v)
    }
    Yaml.parse[WorkflowConf](substConf)
  }

  def buildWrappers(): Unit = {
    val wrappers = (FileUtils.filesUnder(c.path.resolve("images")) map { p => WrapperDef.fromFile(p, c.subst) }).toSet
    val wrapperBuilder = WrapperBuilder.fromWorkflowConf(wConf, wrappers, c.force)
    wrapperBuilder.buildAll()
    wrapperBuilder.pushAll()
  }

  def buildWorkflow(): Path = {
    //Need an existing context otherwise helm crashes
    SystemProcess("kubectl config set-context build").exec()
    SystemProcess("kubectl config use-context build").exec()

    val targetPath = Paths.get("target/buildWorkflow")
    FileUtils.deleteDirIfExists(targetPath)
    FileUtils.createDirs(targetPath)
    val metakubePath = targetPath.resolve("metakube")

    //Download metakube.zip
    {
      val http = HttpClient.stdClient
      val uri = s"https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/metakube/${wConf.metakubeVersion}/metakube.zip"
      val metakubeZipPath = targetPath.resolve("metakube.zip")
      http.download(uri, metakubeZipPath).throwError
      FileUtils.unzip(metakubeZipPath, targetPath, true)
    }

    //Substitution in Chart.yaml
    {
      val chartPath = metakubePath.resolve("Chart.yaml")
      val updatedChart = FileUtils.readFile(chartPath)
        .replaceAll(s"\\{\\{WORKFLOW_VERSION\\}\\}", wConf.version)
        .replaceAll(s"\\{\\{CHART_NAME\\}\\}", wConf.formatedName)
      FileUtils.writeToFile(chartPath, updatedChart)
    }

    //Addition in backend-conf.yaml
    {
      val backendConfPath = metakubePath.resolve("templates/backend-conf.yaml")
      val updatedBackendConf = FileUtils.readFile(backendConfPath) +
        s"""
          |    app.name = "${wConf.name}"
          |    app.pipeline.description = ""${'"' + wConf.description.replaceAll("\\n", "\\n    ") + '"'}""
          |    app.pipeline.defaultProfile = "${wConf.defaultProfile}"
          |    app.pipeline.defaultPathway = "${wConf.defaultPathway}"
          |""".stripMargin
      FileUtils.writeToFile(backendConfPath, updatedBackendConf)
    }

    //Addition to values.yaml
    {
      val valuesPath = metakubePath.resolve("values.yaml")
      val updatedValues = FileUtils.readFile(valuesPath)
      val addValuesPath = c.path.resolve("helm").resolve("values.yaml")
      val addStr = if (FileUtils.exists(addValuesPath, true))
        FileUtils.readFile(addValuesPath)
      else
        ""
      FileUtils.writeToFile(valuesPath, s"$updatedValues\n$addStr")
    }

    //Add extra helm templates
    {
      val helmPath = c.path.resolve("helm")
      val helmTemplates = if (FileUtils.exists(helmPath))
        FileUtils.filesUnder(c.path.resolve("helm")).filter(_.getFileName.toString != "values.yaml")
      else
        Seq()
      helmTemplates foreach { ht =>
        FileUtils.copyFileToDir(ht, metakubePath.resolve("templates"))
      }
    }

    //Add pipeline files
    {
      val pipelinePath = metakubePath.resolve("pipeline")
      FileUtils.createDirs(pipelinePath)
      FileUtils.copyDir(c.path.resolve("assets"), pipelinePath.resolve("assets"))
      FileUtils.copyDir(c.path.resolve("profiles"), pipelinePath.resolve("profiles"))
      FileUtils.copyDir(c.path.resolve("pathways"), pipelinePath.resolve("pathways"))
      val templatesPath = pipelinePath.resolve("templates")
      FileUtils.copyDir(c.path.resolve("templates"), templatesPath)
      val templates = FileUtils.filesUnder(templatesPath).filter{_.toString.endsWith(".tpl.yaml")}
      templates foreach { tPath =>
        val rawTemplate = FileUtils.readFile(tPath)
        val imageKey = Yaml.parse[ImageKey](rawTemplate).imageKey
        val withImage = FileUtils.readFile(tPath) + s"\nimage: ${wConf.fullImageName(imageKey)}"
        FileUtils.writeToFile(tPath, withImage)
      }
    }

    //Check the integrity of the Helm chart we just assembled
    SystemProcess(s"helm lint $metakubePath").exec()

    //Package the Helm chart
    SystemProcess(s"helm package --version ${wConf.version} -d $targetPath --app-version ${wConf.metakubeVersion} -u --debug $metakubePath").exec()
    FileUtils.rename(targetPath.resolve(s"${wConf.formatedName}-${wConf.version}.tgz"), s"${wConf.formatedName}.tgz")
    val out = targetPath.resolve(s"${wConf.formatedName}.tgz")
    println(s"Successfully built workflow '${wConf.name}' to $out")
    out
  }

  def buildImage(archive: Path): String = {
    val archiveName = archive.getFileName
    val dir = archive.getParent
    val dockerfile =
      s"""
         |FROM registry.gitlab.com/uit-sfb/metakube/mkadm:${wConf.metakubeVersion}
         |COPY --chown=sfb:0 $archiveName /home/sfb/
         |RUN mv /home/sfb/$archiveName /home/sfb/workflow.tgz
         |ENTRYPOINT ["/opt/docker/bin/mkadm", "workflow"]
         |""".stripMargin
    val dockerfilePath = dir.resolve("Dockerfile")
    FileUtils.writeToFile(dockerfilePath, dockerfile)
    val imgName = wConf.fullImageName(wConf.formatedName)
    SystemProcess(s"docker build -t $imgName .", path = Some(dir)).exec()
    SystemProcess(s"docker push $imgName").exec()
    imgName
  }

  def writeWrapperScript(imgName: String, scriptPath: Path): Unit = {
    val script =
      s"""#! /bin/bash
         |
         |IT_FLAG="-it"
         |CONFIG_ARG=""
         |POSITIONAL=()
         |while [[ $$# -gt 0 ]]
         |do
         |  case "$$1" in
         |    -c|--config)
         |      CONFIG_PATH=$$2
         |      CONFIG_ARG="-v $$(realpath $$CONFIG_PATH):/home/sfb/config.yaml"
         |      shift
         |      shift
         |      ;;
         |    -y|--yes)
         |      IT_FLAG=""
         |      POSITIONAL+=("-y")
         |      shift
         |      ;;
         |    *) #Unknown
         |      POSITIONAL+=("$$1")
         |      shift
         |      ;;
         |  esac
         |done
         |
         |docker run --rm \\
         |      $$IT_FLAG \\
         |      --network host \\
         |      --user $$(id -u):0 \\
         |      -v $$(realpath ~/.kube):/home/sfb/.kube \\
         |      $$CONFIG_ARG \\
         |      $imgName $${POSITIONAL[*]}
         |""".stripMargin
    FileUtils.writeToFile(scriptPath, script)
    println(s"Wrote script to $scriptPath")
  }
}
case class ImageKey(imageKey: String)