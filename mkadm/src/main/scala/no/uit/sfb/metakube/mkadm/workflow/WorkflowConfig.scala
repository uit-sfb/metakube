package no.uit.sfb.metakube.mkadm.workflow

import java.nio.file.{Path, Paths}

case class WorkflowConfig(cmd: String = "",
                          path: Path = Paths.get("/home/sfb/workflow.tgz"),
                          udatePipeline: Boolean = false,
                          dry: Boolean = false,
                          config: Path = Paths.get("/home/sfb/config.yaml"),
                          yes: Boolean = false
                       )
