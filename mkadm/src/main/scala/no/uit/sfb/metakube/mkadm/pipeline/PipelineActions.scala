package no.uit.sfb.metakube.mkadm.pipeline

import java.nio.file.Paths

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.MetakubeDriver
import no.uit.sfb.metakube.models.chart.ChartBuilder
import no.uit.sfb.metakube.template.{TemplateEngine, TemplateOverlay}
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.PachydermDriver
import no.uit.sfb.scalautils.common.FileUtils

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

object PipelineActions extends LazyLogging {
  implicit val ec = ExecutionContext.global
  def apply(c: PipelineConfig): Unit = {
    lazy val pd = {
      logger.info(s"Using pachd endpoint ${c.pachHost}:${c.pachPort}")
      new PachydermDriver(c.pachHost, c.pachPort)
    }
    lazy val md = new MetakubeDriver(
      new PachydermWorkflowManagerImpl(pd)
    )

    //Needs to be a bit longer than other timeouts as it is used for actions at the whole workflow level
    val timeout = 5.minutes

    val inTemplatesDir = FileUtils
      .filesUnder(PipelineParser.templatesPath)
      // We are only interested in files, not symlinks created by k8s
      .filter { !_.toString.contains("/..") } // FileUtils.isFile(p, false) doesn't work

    val templateFilePaths = {
      val templates =
        inTemplatesDir.filter { p =>
          p.getFileName.toString.endsWith(".tpl.yml") || p.getFileName.toString
            .endsWith(".tpl.yaml")
        }
      templates.size match {
        case 0 =>
          throw new Exception(
            s"No templates found in ${PipelineParser.templatesPath}")
        case _ => templates
      }
    }
    val oRuntimeConfigOverridePath = {
      val configOverrides = {
        if (FileUtils.exists(PipelineParser.configOverridePath))
        FileUtils
          .filesUnder(PipelineParser.configOverridePath)
          // We are only interested in files, not symlinks created by k8s
          .filter { !_.toString.contains("/..") } // FileUtils.isFile(p, false) doesn't work
          .filter { p =>
            p.getFileName.toString.endsWith(".yaml") || p.getFileName.toString
              .endsWith(".yml")
          }
        else
          Seq()
      }
      configOverrides.size match {
        case 0 => None
        case 1 => Some(configOverrides.head)
        case _ =>
          throw new Exception(
            s"More than one config override file found in ${PipelineParser.configOverridePath}: ${configOverrides
              .mkString(", ")}")
      }
    }
    val tmpPath = Paths.get("/tmp/pipeline")
    FileUtils.deleteDirIfExists(tmpPath)
    FileUtils.createDirs(tmpPath)
    val realizedTemplatesPath = tmpPath.resolve("pachydermTemplates")
    //No need to delete/create realizedTemplatesPath as TemplateEngine takes care of it
    val oMergedConfigPath = {
      oRuntimeConfigOverridePath map {
        secondaryConfOverridePath =>
          val mergedConfigPath = tmpPath.resolve("mergedConfig.yaml")
          //For config validation only
          TemplateOverlay.fromPath(secondaryConfOverridePath)
          FileUtils.copyFile(secondaryConfOverridePath, mergedConfigPath)
          mergedConfigPath
      }
    }
    lazy val tEngine = new TemplateEngine(
      templateFilePaths.toSet,
      oMergedConfigPath,
      realizedTemplatesPath
    )

    lazy val chart = {
      //Generate pachyderm pipeline defs from templates
      tEngine.generatePachydermDefs()
      //Build the chart from pachyderm pipeline defs
      new ChartBuilder(realizedTemplatesPath).chart
    }

    assert(chart.inputRepos.size <= 1,
           s"Multiple input repos are not yet supported (${chart.inputRepos
             .mkString(", ")}):\n${printPachDefs()}")

    def printPachDefs(): Unit = {
      chart.stages.foreach { stage =>
        println(s"\n### ${stage.id} ###")
        println(stage.printPipelineSpec)
      }
    }

    val nbRetry: Int = {
      val dur = timeout.toMinutes.toInt - 2 //We consider it takes 2 minutes to deploy the pipelines once pachd is running
      if (dur < 0)
        0
      else
        dur
    }

    c.cmd match {
      case "stage" =>
        printPachDefs()
      case "deploy" =>
        printPachDefs()
        Await.result(md.check(nbRetry) flatMap { _ =>
          md.deployWorkflow(chart)
        }, timeout)
      case "update" =>
        printPachDefs()
        Await.result(md.check(nbRetry) flatMap { _ =>
          md.updateWorkflow(chart, c.matching)
        }, timeout)
      case "apply" =>
        printPachDefs()
        Await.result(md.check(nbRetry) flatMap { _ =>
          md.updateWorkflow(chart, c.matching)
        }, timeout)
      case "undeploy" =>
        Await.result(md.check(nbRetry) flatMap { _ =>
          md.undeployWorkflow(chart)
        }, timeout)
      case "status" =>
        val f = pd.printRepos map { _ =>
          pd.printPipelines()
        } map { _ =>
          pd.printJobs(None)
        }
        Await.result(f, 15.seconds)
    }
  }
}
