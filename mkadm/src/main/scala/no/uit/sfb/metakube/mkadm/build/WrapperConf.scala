package no.uit.sfb.metakube.mkadm.build

import com.fasterxml.jackson.annotation.JsonCreator

case class WrapperConf(parentImage: String,
                       packages: Set[String] = Set(),
                       appScript: String = "") {
  @JsonCreator
  def this() = this("", Set(), "")
}
