package no.uit.sfb.metakube.mkadm.build

import com.fasterxml.jackson.annotation.JsonCreator

case class WorkflowConf(name: String,
                        version: String,
                        metakubeVersion: String,
                        description: String,
                        defaultProfile: String,
                        defaultPathway: String,
                        assets: Map[String, String],
                        registry: String,
                        images: Map[String, WrapperConf]) {
  @JsonCreator
  def this() = this("",
    "",
    "",
    "",
    "",
    "",
    Map(),
    "",
    Map()
  )

  lazy val formatedName = name.toLowerCase.replace(" ", "-")
  def fullImageName(image: String) = s"$registry/$image:$version"
}
