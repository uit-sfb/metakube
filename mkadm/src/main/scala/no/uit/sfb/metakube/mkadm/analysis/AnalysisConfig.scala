package no.uit.sfb.metakube.mkadm.analysis

import java.nio.file.Path

case class AnalysisConfig(
    cmd: String = "",
    pachHost: String = "",
    pachPort: Int = 0,
    commitId: String = "",
    stage: String = "",
    follow: Boolean = false,
    downloadPath: Path = null,
    configPath: Path = null,
    inputs: Map[String, Path] = Map()
)
