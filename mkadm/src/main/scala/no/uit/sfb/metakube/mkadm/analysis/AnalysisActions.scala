package no.uit.sfb.metakube.mkadm.analysis

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.MetakubeDriver
import no.uit.sfb.metakube.models.analysis.{AnalysisDef, AnalysisSubmission}
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.PachydermDriver
import no.uit.sfb.pachyderm.models.CommitRef
import no.uit.sfb.scalautils.common.FileUtils

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

object AnalysisActions extends LazyLogging {
  implicit val ec = ExecutionContext.global
  def apply(c: AnalysisConfig): Unit = {
    logger.info(s"Using pachd endpoint ${c.pachHost}:${c.pachPort}")

    val timeout = 4.hours

    val inputRepo = "reads"
    lazy val pd = new PachydermDriver(c.pachHost, c.pachPort)
    lazy val md = new MetakubeDriver(
      new PachydermWorkflowManagerImpl(pd)
    )

    def commitRef(id: String) = CommitRef(inputRepo, id)

    c.cmd match {
      case "status" =>
        Await.result(pd.printJobs(Some(commitRef(c.commitId))), timeout)
      case "submit" =>
        val anaDef =
          AnalysisDef(datasets = c.inputs map {
            case (inputType, path) =>
              inputType -> path.toUri
          }, config = FileUtils.readFile(c.configPath))

        val pachdReady = md.check(5)
        val wid = Await.result(pachdReady flatMap { _ =>
          md.submitAndWait(AnalysisSubmission(anaDef),
                           blockingMessage = true)
        }, timeout)
        println(wid) //Leave me here: some scripts are using me!
      case "cancel" => throw new Exception("Not implemented")
      case "attach" =>
        Await.result(md.followAnalysis(commitRef(c.commitId).toString, true),
                     timeout)
      case "logs" =>
        Await.result(pd.printJobLogs(commitRef(c.commitId), c.stage, c.follow),
                     timeout)
      case "download" =>
        Await.result(
          pd.downloadWorkflowOutputs(commitRef(c.commitId), c.downloadPath),
          timeout)
    }
  }
}
