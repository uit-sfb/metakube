addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.6.0")
addSbtPlugin("no.uit.sfb" % "artifactory-sbt-plugin" % "0.1.1-SNAPSHOT")
addSbtPlugin("no.uit.sfb" % "sbt-git-smartversion" % "1.1.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")
