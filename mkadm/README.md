# mkadm

METAkube admin client

## Help

Use `-h`.

## Publish

- Artifactory artifact:  
In sbt, run: `artifactory:publish`

- Docker:  
In sbt, run: `docker:publish`

Note when using Docker, remember to add `--` to separate the JVM arguments from mkadm's arguments.