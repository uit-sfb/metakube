import java.nio.file.Paths

import com.typesafe.sbt.packager.docker.{Cmd, CmdLike, ExecCmd}
import no.uit.sfb.scalautils.common.FileUtils
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Level._


name := "mkadm"
scalaVersion := "2.12.8"
organization     := "no.uit.sfb"
organizationName := "SfB"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

maintainer := "mmp@uit.no"

resolvers ++= {
  Seq(
    Some(
      "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if (version.value.endsWith("-SNAPSHOT"))
      Some(
        "Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

publishTo := {
  if (version.value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../targetVersion"

enablePlugins(DockerPlugin, JavaAppPackaging, ReleaseArtifactory)

Artifactory / artifBuildArtifacts := Seq((Universal / packageBin).value.toPath)
Artifactory / artifProjectVersion := version.value
Artifactory / artifLocation := s"${organization.value}/metakube"

lazy val pachVersion = sbt.settingKey[String]("Pachyderm version")
pachVersion := FileUtils.readFile(Paths.get("../pachydermVersion"))

dockerLabels := Map("gitCommit" -> s"${git.formattedShaVersion.value.getOrElse("unknown")}@${git.gitCurrentBranch.value}")
dockerRepository in Docker := Some("registry.gitlab.com")
dockerUsername in Docker := Some("uit-sfb/metakube")
//dockerChmodType := DockerChmodType.Custom("u=rX,g=rX,o=rX")
daemonUser in Docker := "sfb" //"in Docker" needed for this parameter
dockerCommands := dockerCommands.value
  .flatMap{ cmd =>
  val pf: PartialFunction[CmdLike, Boolean] = { case Cmd("RUN", args @ _*) if args.head == "id" => true }
  if (pf.lift(cmd).getOrElse(false))
    Seq(cmd) ++
    Seq(
      Cmd("RUN", "apt update && apt install -y nano && rm -rf /var/lib/apt/lists/*"),
      Cmd("ENV", "HELM_VERSION=\"v3.1.2\""),
      Cmd("RUN", "wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm && chmod +x /usr/local/bin/helm"),
      Cmd("RUN", s"curl -o /tmp/pachctl.deb -L https://github.com/pachyderm/pachyderm/releases/download/v${pachVersion.value}/pachctl_${pachVersion.value}_amd64.deb && dpkg -i /tmp/pachctl.deb && rm /tmp/pachctl.deb"),
      Cmd("RUN", s"curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl"),
      Cmd("RUN", s"wget -q https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz -O - | tar -xzO openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/oc > /usr/local/bin/oc && chmod +x /usr/local/bin/oc")
    )
  else Seq(cmd)
}
  .flatMap{ cmd =>
  val pf: PartialFunction[CmdLike, Boolean] = { case ExecCmd("ENTRYPOINT", _) => true }
  if (pf.lift(cmd).getOrElse(false))
    Seq(
      Cmd("ENV", "HOME", s"/home/${(Docker / daemonUser).value}"), //For some reason HOME is not set! So we need to do it manually.
      Cmd("RUN", "chmod -R g+w $HOME"),
      Cmd("WORKDIR", s"/home/${(Docker / daemonUser).value}")
    ) ++ Seq(cmd)
  else Seq(cmd)
}

lazy val scalaUtilsVer = "0.2.1"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "4.0.0-RC2",
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-json" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-http" % scalaUtilsVer,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.1.0" % Test,
  "no.uit.sfb" %% "mk-drivers" % version.value,
  "no.uit.sfb" %% "mk-template-engine" % version.value
)

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion,
  git.formattedShaVersion
)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"