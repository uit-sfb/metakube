import java.nio.file.Paths

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.SystemProcess
import org.scalatest.{FunSpec, Matchers}
import no.uit.sfb.info.mk_dummy_it.BuildInfo

import scala.concurrent.ExecutionContext

class IntegrationTest extends FunSpec with Matchers with LazyLogging {
  implicit val ec = ExecutionContext.global

  describe("The pipeline") {
    var wid = ""

    val minishiftProfile = sys.env("PROFILE")
    val ip = SystemProcess(s"minishift --profile $minishiftProfile ip").execFO(_.last)
      .getOrElse(throw new Exception(
        s"Minishift profile '$minishiftProfile' doesn't seem to exist. Please create it before running this test."))

    it("should execute without failure") {
      SystemProcess(
        s"docker pull registry.gitlab.com/uit-sfb/metakube/mkadm:${BuildInfo.version}")
        .exec(false)
      wid = SystemProcess(
        s"""docker run --rm
           |--network host
           |-v ${Paths.get(System.getProperty("user.dir"))}/src/test/resources/job.yaml:/data/conf.yaml:ro
           |registry.gitlab.com/uit-sfb/metakube/mkadm:${BuildInfo.version} --
           |analysis submit --pachd $ip:30650 --config /data/conf.yaml""".stripMargin
          .replace("\n", " ")
      ).execF(_.last, true)
    }
  }
}
