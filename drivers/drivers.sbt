name := "mk-drivers"
scalaVersion := "2.12.8"
organization     := "no.uit.sfb"
organizationName := "SfB"

resolvers ++= {
  Seq(
    Some(
      "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if (version.value.endsWith("-SNAPSHOT"))
      Some(
        "Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

publishTo := {
  if (version.value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../targetVersion"

val scalaUtilsVer = "0.2.1"

libraryDependencies ++= Seq(
  //"com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  //"ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.1.0" % Test,
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-json" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-yaml" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-apis" % scalaUtilsVer,
  "com.github.blemale" %% "scaffeine" % "3.1.0" % "compile"
)
