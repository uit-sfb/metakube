package no.uit.sfb.kubernetes

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.SystemProcess

class KubernetesDriver(namespace: String = "default") extends LazyLogging {
  def getAvailableDeployments(): Seq[String] = {
    SystemProcess
      .from(
        Seq(
          "kubectl",
          "get",
          "deployment",
          "--namespace",
          namespace,
          """-o=jsonpath='{range .items[*]}{.metadata.name}{"\\t"}{.status.availableReplicas}{"\\n"}{end}'"""
        )
      )
      .execF(_.filter { str =>
        str.split('\t').tail.head.toInt > 0
      })
  }

  def getPods(): String = {
    SystemProcess(s"kubectl get pods --namespace $namespace").execF(r =>
      r.mkString("\n"))
  }
}
