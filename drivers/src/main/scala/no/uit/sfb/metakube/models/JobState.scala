package no.uit.sfb.metakube.models

object JobState {
  sealed trait State {
    def name: String
    def failed: Boolean = false
  }

  case object Success extends State {
    val name = "SUCCESS"
  }
  case object Running extends State {
    val name = "RUNNING"
  }
  case object Failed extends State {
    val name = "FAILED"
    override def failed: Boolean = true
  }
  case object Killed extends State {
    val name = "KILLED"
    override def failed: Boolean = true
  }
  case object Unknown extends State {
    val name = "UNKNOWN"
  }

  def apply(str: String): State = {
    str match {
      case "JOB_Starting" => Running
      case "JOB_RUNNING"  => Running
      case "JOB_MERGING"  => Running
      case "JOB_SUCCESS"  => Success
      case "JOB_FAILURE"  => Failed
      case "JOB_KILLED"   => Killed
      case _              => Unknown
    }
  }
}
