package no.uit.sfb.metakube.models.analysis

case class AnalysisSubmission(
    analysisDef: AnalysisDef,
    userId: String = "anonymous"
)
