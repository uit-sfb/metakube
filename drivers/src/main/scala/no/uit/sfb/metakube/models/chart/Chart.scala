package no.uit.sfb.metakube.models.chart

/**
  * A Chart is an ordered sequence of stage spec (build time)
  * A stage can be either:
  *   - an input repository
  *   - an active stage (performing a transformation)
  *
  * The ordering is a linearization of the workflow graph: the ordered sequence is such that a stage appear always after any stage it depends on.
  * Use a ChartBuilder to produce a Chart.
  */
case class Chart(stages: Seq[StageDef]) {
  lazy val (inputRepos, activeStages) = stages.partition { _.isInput }
}
