package no.uit.sfb.metakube.models

import java.time.LocalDateTime

import no.uit.sfb.metakube.utils.packaging.GenericVersion
import no.uit.sfb.metakube.wm.WorkflowManagerLike._

import scala.util.Try

case class ActiveStage(
    id: StageId,
    createdDate: LocalDateTime,
    state: StageState.State,
    metadata: Map[String, String] //Any extra data that we may want to keep
)

object ActiveStage extends Ordering[ActiveStage] {
  //If they do not have the same number of items, we just assume the first one is less than the second member
  override def compare(x: ActiveStage, y: ActiveStage): Int =
    Try { GenericVersion.compare(x.id, y.id) } getOrElse -1
}
