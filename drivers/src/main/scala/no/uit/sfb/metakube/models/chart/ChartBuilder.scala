package no.uit.sfb.metakube.models.chart

import java.nio.file.Path

import no.uit.sfb.metakube.models.utils.DagLike
import no.uit.sfb.scalautils.common.FileUtils

import scala.collection.mutable

/**
  * Chart producer
  *
  * @param path Path to translated pachyderm templates
  */
class ChartBuilder(path: Path) extends DagLike[StageDef] {
  private val stageListBuilder = mutable.ArrayBuffer[StageDef]()

  private def addStage(stageDef: StageDef): Unit = {
    synchronized {
      stageListBuilder.append(stageDef)
    }
  }

  private val files = FileUtils.filesUnder(path)

  assert(files.nonEmpty, "No pipeline def found.")

  files foreach { p =>
    addStage(StageDef.fromFile(p))
  }

  lazy val unorderedStageList: Set[StageDef] = {
    stageListBuilder.toSet
  }

  lazy val elements = unorderedStageList
  override protected def unknownLeaf(v: String): Option[StageDef] = Some(StageDef(v))

  lazy val chart: Chart = {
    Chart(sorted)
  }
}
