package no.uit.sfb.metakube.models.utils

import scala.collection.mutable

trait DagLike[T <: DependsOnLike] {
  def elements: Set[T]

  protected def unknownLeaf(v: String): Option[T] = None

  lazy val sorted: Seq[T] = {
    val notVisited =
      mutable.ArrayBuffer[T](elements.toSeq: _*)
    val stageSequenceBuilder = mutable.ArrayBuffer[T]()

    def visit(node: Option[T]): Unit = {
      node match {
        case Some(stage) =>
          val idx = notVisited
            .indexWhere(_.id == stage.id)
          if (idx >= 0)
            notVisited.remove(idx) //Mark as visited
          stage.parents foreach { p =>
            notVisited.find(_.id == p) match {
              case Some(parent) => visit(Some(parent))
              case None =>
                if (!elements.exists(_.id == p) && !stageSequenceBuilder
                  .exists(_.id == p)) //If it is an input repo and it has not yet been added
                visit(unknownLeaf(p))
            }
          }
          stageSequenceBuilder.append(stage) //Because of the way the algorithm is implemented we know that a new ctx should always be added at the end.
        case None => //Do nothing
      }
    }

    while (notVisited.nonEmpty) { //Because a graph may have several independent tails, we ensure we visited everyone.
      visit(notVisited.headOption)
    }
    stageSequenceBuilder
  }
}
