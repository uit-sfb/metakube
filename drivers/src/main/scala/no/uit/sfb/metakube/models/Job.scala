package no.uit.sfb.metakube.models

import java.time.LocalDateTime

import no.uit.sfb.metakube.wm.WorkflowManagerLike._

case class Job(
    wid: WorkflowId,
    sid: StageId,
    started: LocalDateTime,
    finished: Option[LocalDateTime],
    state: JobState.State,
    metadata: Map[String, String], //Any extra data that we may want to keep
    datum_success: Int,
    datum_skipped: Int,
    datum_remaining: Int, //Or failed
    datum_total: Int
) {
  val isSuccess = state == JobState.Success
  val failed = state.failed
}
