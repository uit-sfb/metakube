package no.uit.sfb.metakube.models.utils

trait DependsOnLike {
  def id: String
  def parents: Set[String]
}
