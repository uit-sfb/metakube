package no.uit.sfb.metakube.models.analysis

import java.net.URI

import scala.util.Random

case class AnalysisDef(
    analysisId: String = Random.alphanumeric.take(10).mkString,
    datasets: Map[String, URI],
    config: String
)
