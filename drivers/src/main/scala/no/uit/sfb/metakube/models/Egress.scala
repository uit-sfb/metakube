package no.uit.sfb.metakube.models

import java.time.LocalDateTime

import no.uit.sfb.metakube.wm.WorkflowManagerLike._

case class Ingress(
    id: StageId,
    createdDate: LocalDateTime,
    size: Long,
    metadata: Map[String, String]
)
