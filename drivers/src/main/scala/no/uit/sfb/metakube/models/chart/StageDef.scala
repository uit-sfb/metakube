package no.uit.sfb.metakube.models.chart

import java.nio.file.Path

import no.uit.sfb.metakube.models.utils.DependsOnLike
import no.uit.sfb.pachyderm.models.spec.PipelineSpec
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

case class StageDef(
    id: String, //ex: assembly_seqprep_1-2
    defPath: Option[Path] = None //None if input repo
) extends DependsOnLike {
  val isInput: Boolean = defPath.isEmpty
  val isActive: Boolean = !isInput

  def printPipelineSpec: Option[String] = defPath map { path =>
    FileUtils.readFile(path)
  }

  lazy val pipelineSpec: Option[PipelineSpec] = defPath map { path =>
    Json.fromFileAs[PipelineSpec](path)
  }

  lazy val parents: Set[String] = pipelineSpec map { _.parents } getOrElse Set()
}

object StageDef {
  def fromFile(path: Path): StageDef = {
    val pipelineDef = Json.fromFileAs[PipelineSpec](path)
    StageDef(pipelineDef.pipeline.name, Some(path))
  }
}
