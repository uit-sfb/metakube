package no.uit.sfb.metakube

import java.io.InputStream
import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.models.analysis.{AnalysisDef, AnalysisSubmission}
import no.uit.sfb.metakube.models.chart.Chart
import no.uit.sfb.metakube.models.{ActiveStage, Job, OutputFile, StageState}
import no.uit.sfb.metakube.wm.WorkflowManagerLike
import no.uit.sfb.metakube.wm.WorkflowManagerLike._
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.models.CommitRef
import no.uit.sfb.scalautils.common.{FileUtils, FutureUtils}
import no.uit.sfb.scalautils.yaml.Yaml

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Success

class MetakubeDriver(pm: WorkflowManagerLike)(implicit val ec: ExecutionContext)
    extends LazyLogging {
  import MetakubeDriver._

  protected def buildEnvs(submission: AnalysisSubmission,
                          stages: Seq[ActiveStage]): Map[String, ENVIRS] = {
    //Default config
    val default: Map[String, Map[String, String]] =
      (stages.sorted(ActiveStage) map { s =>
        s.id -> Map[String, String]()
      }).toMap
    //Defined configs
    val defined: Seq[(String, Map[String, String])] = {
      val parsed = Yaml
        .parse[Seq[Map[String, Map[String, Any]]]](
          submission.analysisDef.config) //We use a Seq[Map] because if using only a Map[] we would loose the ordering
      parsed
        .map { m =>
          m.head
        }
        .map {
          case (k, v) =>
            val m: ENVIRS =
              if (v == null)
                Map()
              else {
                //Yaml is not able to recognize that we want strings (not boolean or integers), so we do it manually
                (v collect {
                  case (_, null) =>
                    None
                  case (kk, vv) =>
                    Some(kk -> vv.toString)
                }).flatten.toMap
              }
            k -> m
        }
    }
    //We override the default config with the defined values
    //We apply the value if the pipeline id contains the key
    //For instance configs defined under "assembly" will apply to any pipeline containing "assembly" in its id.
    //"_" matches all
    default map {
      case (k, m) =>
        k -> defined
          .filter { case (dk, _) => k.contains(dk) || dk == "_" }
          .foldLeft(m) { case (acc, mm) => acc ++ mm._2 } //The latest definition in the list overrides the previous ones
    }
  }

  def check(retry: Int = 0): Future[Unit] = {
    pm.check(retry)
  }

  /**
    * Inspect workflow
    * @return (input repo, active repos)
    */
  def workflowRuntimeInfo(): Future[(StageId, Seq[ActiveStage])] = {
    pm.listIngresses().map { ingresses =>
      if (ingresses.size > 1)
        throw new Exception(
          s"Found more than one input repo (${ingresses.map { _.id }.mkString(", ")})")
      else
        ingresses.headOption
          .getOrElse(throw new Exception(
            "Could not find input repo. Are you sure the pachyderm pipeline has been deployed?"))
          .id
    } flatMap { rn =>
      pm.isInitialized(rn) map { _ =>
        rn
      }
    } flatMap { rn =>
      pm.listActiveStages() map { as =>
        if (as.isEmpty)
          throw new Exception(
            "No active stage found. Are you sure the pipeline is deployed?")
        (rn, as)
      }
    }
  }

  /** Analysis submission **/
  /**
    * Attention: cannot be used concurrently
    *
    * Submits an analysis
    * @param submission The submission
    * @param wid If set, the commit is assumed to exist already
    * @return CommitRef
    */
  def submitAnalysis(submission: AnalysisSubmission,
                     validate: AnalysisDef => Boolean = _ => true,
                     wid: Option[WorkflowId] = None): Future[WorkflowId] = {
    if (!validate(submission.analysisDef))
      throw new Exception("AnalysisDef validation failed.")
    FutureUtils.retry(workflowRuntimeInfo(), period = 30.seconds) flatMap {
      case (inputRepoName, as) =>
        val envMap = buildEnvs(submission, as)
        val data = submission.analysisDef.datasets map {
          case (k, uri) =>
            val pathWithExt = {
              val ext = Paths
                .get(uri.getPath)
                .getFileName
                .toString
                .split('.')
                .tail
                .mkString(".")
              if (ext.nonEmpty)
                s"$k.$ext"
              else
                k
            }
            Paths.get(s"data/$pathWithExt") -> uri
        }
        val cfgFiles =
          (envMap map {
            case (pipeline, envs) =>
              //If MK_RUN is false then we do not create the env script.
              //Being crossed will all it's inputs it will prevent the stage from running.
              if (envs.getOrElse("MK_RUN", "true") == "true") {
                val fName = s"env_$pipeline.sh"
                val str =
                  s"""#!/usr/bin/env bash
                     |${envs
                       .collect { case (k, v) if k != "MK_RUN" => s"export $k=$v" } //We do not use MK_RUN any longer within the wrapper code, so no need to set it.
                       .mkString("\n")}

                     |""".stripMargin
                Some(Paths.get(fName) -> str)
              } else
                None
          }).flatten.toMap ++ Seq({
            val str = s"""#!/usr/bin/env bash
                 |export USER_ID="${submission.userId}"
                 |export AID="${submission.analysisDef.analysisId}"
                 |""".stripMargin
            Paths.get("context.sh") -> str
          })
        val fWid: Future[WorkflowId] =
          pm.submitData(inputRepoName,
                        data,
                        cfgFiles,
                        _ => wid.isEmpty, //We delete any existing files
                        wid)
        fWid andThen {
          case Success(wid) => logger.info(s"Submitted wid '$wid'")
        }
    }
  }

  /**
    * Submits an analysis and wait for result to be available.
    * Throws AssertionError if at least one pipeline failed.
    * Blocking
    */
  def submitAndWait(submission: AnalysisSubmission,
                    validate: AnalysisDef => Boolean = _ => true,
                    blockingMessage: Boolean = false,
                    retry: Int = 1,
                    wid: Option[WorkflowId] = None): Future[WorkflowId] = {
    submitAnalysis(submission, validate, wid) flatMap { wid =>
      followAnalysis(wid, blockingMessage) map { _ =>
        wid
      }
    }
  }

  private def waitForPipelineStarted(): Unit = {
    while ({
      val stages = Await.result(pm.listActiveStages(), 5.minutes)
      if (stages.exists(_.state == StageState.Failure))
        throw new Exception("One pipeline is not properly deployed.")
      else stages.exists(_.state == StageState.Starting)
    }) {
      try Thread.sleep(1000)
      catch {
        case ex: InterruptedException =>
          Thread.currentThread.interrupt()
      }
    }
  }

  //The default input is used to notify that the pipeline has successfully been deployed
  private def initialize(chart: Chart): Future[Unit] = {
    chart.stages.foldLeft[Future[Unit]](Future(Unit)) {
      case (acc, stage) =>
        acc flatMap { _ =>
          if (stage.isInput) {
            pm.submitOne("", Paths.get("/context.sh"), stage.id) map { _ =>
              Unit
            }
          } else
            Future.successful()
        }
    }
  }

  /** Workflow deployment **/
  /**
    * Create all the pipelines contained in a chart.
    * Throw exception if on stage already exist.
    *
    * @param chart
    * @return
    */
  def deployWorkflow(chart: Chart): Future[Unit] = {
    chart.stages.foldLeft[Future[Unit]](Future(Unit)) {
      case (acc, stage) =>
        acc flatMap { _ =>
          if (stage.isInput)
            pm.createIngress(stage.id)
          else
            pm.createActiveStage(stage.defPath.get)
        }
    } map { _ =>
      waitForPipelineStarted()
    } flatMap { _ =>
      initialize(chart)
    }
  }

  /**
    * Update workflow or create stages that do not exist yet.
    *
    * @param chart
    * @param matching
    * @return
    */
  def updateWorkflow(chart: Chart, matching: String): Future[Unit] = {
    chart.stages
      .filter { _.id.contains(matching) }
      .foldLeft[Future[Unit]](Future(Unit)) {
        case (acc, stage) =>
          acc flatMap { _ =>
            stage match {
              case stage if stage.isInput => pm.updateIngress(stage.id)
              case stage                  => pm.updateActiveStage(stage.defPath.get)
            }
          }
      } map { _ =>
      waitForPipelineStarted()
    } flatMap { _ =>
      initialize(chart)
    }
  }

  def undeployWorkflow(chart: Chart): Future[Unit] = {
    chart.stages.reverse.foldLeft[Future[Unit]](Future(Unit)) {
      case (acc, stage) =>
        acc flatMap { _ =>
          stage match {
            case stage if stage.isInput => pm.deleteIngress(stage.id)
            case stage                  => pm.deleteActiveStage(stage.id)
          }
        }
    }
  }

  //Job == None means that the job is pending
  def listJobs(wid: WorkflowId): Future[Seq[(StageId, Option[Job])]] = {
    (pm.listActiveStages() zip pm.listLatestJobs(wid)) map {
      case (activeStages, jobs) =>
        activeStages map { as =>
          as.id -> jobs.find(_.sid == as.id)
        }
    }
  }

  def isFailed(wid: WorkflowId): Future[Boolean] = {
    pm.listLatestJobs(wid) map { jobs =>
      jobs.exists(_.state.failed)
    }
  }

  def listRuns(wid: WorkflowId, sid: StageId): Future[Seq[Option[Job]]] = {
    pm.listStageJobs(wid, sid)
  }

  def outputs(wid: WorkflowId): Future[Seq[OutputFile]] = {
    pm.listAllOutputs(wid)
  }

  def downloadAll(wid: WorkflowId,
                  to: Path,
                  zip: Boolean = true): Future[Path] = {
    pm.downloadWorkflowOutputs(wid, to, zip)
  }

  def log(wid: WorkflowId, sid: StageId, run: Int = -1): Future[String] = {
    pm.jobLogs(wid, sid, run)
  }

  def deleteJobs(wid: WorkflowId): Future[Unit] = {
    pm.deleteJobs(wid)
  }

  /** Analysis **/
  //Throws AssertionError if at least one pipeline failed
  def followAnalysis(wid: WorkflowId,
                     blockingMessage: Boolean = false): Future[Unit] = {
    //Todo: Implement properly
    pm.asInstanceOf[PachydermWorkflowManagerImpl]
      .pd
      .printFinalStatus(CommitRef.fromString(wid), blockingMessage) flatMap {
      _ =>
        pm.listFinalStatus(wid) map { res =>
          require(res forall {
            _.isSuccess
          }, s"One or more stage failed for commit $wid")
        }
    }
  }

  //Create branch and start commit
  def initAnalysis(branchName: String): Future[WorkflowId] = {
    FutureUtils.retry(workflowRuntimeInfo(), period = 30.seconds) flatMap {
      case (inputRepoName, _) =>
        pm.initAnalysis(inputRepoName, branchName)
    }
  }

  def streamInput(is: InputStream,
                  wid: WorkflowId,
                  path: String): Future[Unit] = {
    pm.streamInput(is, wid, path)
  }

  //delete open commit and delete branch
  def abortAnalysis(wid: WorkflowId, branchName: String): Future[Unit] = {
    pm.abortAnalysis(wid, branchName)
  }

  def runAnalysis(wid: WorkflowId): Future[Unit] = {
    pm.runAnalysis(wid)
  }

  def killJob(wid: WorkflowId, sid: StageId): Future[Unit] = {
    pm.stopJob(wid, sid)
  }

  /*def rerunJob(wid: WorkflowId, sid: StageId): Future[Unit] = {
    pm.rerunJob(wid, sid)
  }*/
}

object MetakubeDriver {
  type ENVIRS = Map[String, String]
}
