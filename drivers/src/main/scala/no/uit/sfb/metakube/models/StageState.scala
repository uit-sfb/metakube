package no.uit.sfb.metakube.models

object StageState {
  sealed trait State {
    def name: String
  }

  case object Starting extends State {
    val name = "STARTING"
  }

  case object Running extends State {
    val name = "RUNNING"
  }

  case object Failure extends State {
    val name = "FAILURE"
  }

  case object Idle extends State {
    val name = "IDLE"
  }

  case object Paused extends State {
    val name = "PAUSED"
  }

  case object Unknown extends State {
    val name = "UNKNOWN"
  }

  def apply(str: String): State = str match {
    case "PIPELINE_STARTING"   => Starting
    case "PIPELINE_RUNNING"    => Running
    case "PIPELINE_RESTARTING" => Starting
    case "PIPELINE_FAILURE"    => Failure
    case "PIPELINE_PAUSED"     => Paused
    case "PIPELINE_STANDBY"    => Idle
    case _                     => Unknown
  }
}
