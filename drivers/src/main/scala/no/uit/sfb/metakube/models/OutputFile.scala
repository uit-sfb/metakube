package no.uit.sfb.metakube.models

import java.nio.file.Path
import java.time.LocalDateTime

import no.uit.sfb.metakube.wm.WorkflowManagerLike.{StageId, WorkflowId}

case class OutputFile(
    wid: WorkflowId,
    sid: StageId,
    path: Path,
    creationDate: LocalDateTime,
    size: Long,
    metadata: Map[String, String] //Any extra data that we may want to keep
)
