package no.uit.sfb.metakube.wm.impl

import java.io.InputStream
import java.net.URI
import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.models._
import no.uit.sfb.metakube.wm.WorkflowManagerLike
import no.uit.sfb.metakube.wm.WorkflowManagerLike._
import no.uit.sfb.pachyderm.PachydermDriver
import no.uit.sfb.pachyderm.PachydermDriver.stringToDate
import no.uit.sfb.pachyderm.models.CommitRef
import no.uit.sfb.pachyderm.models.status.{
  FileStatus,
  JobStatus,
  PipelineStatus,
  RepoStatus
}
import no.uit.sfb.scalautils.common.{FileUtils, FutureUtils, Random}

import scala.concurrent.duration._
import scala.concurrent.Future

/**
  * Implementation of WorkflowManagerLike backed by [[PachydermDriver]]
  *
  * Mappings:
  *   - WorkflowId = scr.toString
  *
  * @param pd
  */
class PachydermWorkflowManagerImpl(val pd: PachydermDriver)
    extends WorkflowManagerLike
    with LazyLogging {

  protected implicit val ec = pd.ec

  def jobLogs(wid: WorkflowId, sid: StageId, run: Int = -1): Future[String] = {
    pd.jobLogs(CommitRef.fromString(wid), sid, run)
  }

  def listActiveStages(): Future[Seq[ActiveStage]] = {
    pd.listPipelines() map {
      _ map { activeStageFromPipelineStatus }
    }
  }

  def listIngresses(): Future[Seq[Ingress]] = {
    (pd.listPipelines() zip pd.listRepos()) map {
      case (pipelines, repos) =>
        logger.info(
          s"Found following repos: ${repos.map { _.repo.name }.mkString(", ")}")
        logger.info(
          s"Found following pipelines: ${pipelines.map { _.pipeline.name }.mkString(", ")}")
        repos
          .filter { repo =>
            !pipelines.exists { p =>
              p.pipeline.name == repo.repo.name
            }
          }
          .map { ingressFromRepoStatus }
    }
  }

  def activeStage(sid: StageId): Future[Option[ActiveStage]] = {
    pd.inspectPipeline(sid) map {
      _ map { activeStageFromPipelineStatus }
    }
  }

  def ingress(sid: StageId): Future[Option[Ingress]] = {
    pd.inspectRepo(sid) map {
      _ map { ingressFromRepoStatus }
    }
  }

  def listJobOutputs(wid: WorkflowId,
                     sid: StageId,
                     path: String = "/"): Future[Seq[OutputFile]] = {
    pd.listOutputs(CommitRef.fromString(wid), sid, path) map {
      _ flatMap { os =>
        outputFileFromFileStatus(wid, sid, os)
      }
    }
  }

  def listAllOutputs(wid: WorkflowId): Future[Seq[OutputFile]] = {
    (pd.listAllOutputs(CommitRef.fromString(wid)) map {
      _ flatMap {
        case (sid, files) =>
          files flatMap { os =>
            outputFileFromFileStatus(wid, sid, os)
          }
      }
    }) map { _.toSeq }
  }

  def isInitialized(stageName: String): Future[Unit] = {
    pd.getOutput(CommitRef(stageName, "master"), "/context.sh") map { _ =>
      ()
    }
  }

  def createActiveStage(config: Path): Future[Unit] = {
    pd.createPipeline(config)
  }

  def deleteActiveStage(sid: StageId): Future[Unit] = {
    pd.deletePipeline(sid)
  }

  def startStage(sid: StageId): Future[Unit] = {
    pd.startPipeline(sid)
  }

  def stopStage(sid: StageId): Future[Unit] = {
    pd.stopPipeline(sid)
  }

  def updateActiveStage(config: Path): Future[Unit] = {
    pd.updatePipeline(config)
  }

  protected def convertToJob(oj: Option[JobStatus],
                             wid: WorkflowId): Option[Job] = {
    oj map { js =>
      jobFromJobStatus(wid, js)
    }
  }

  def jobStatus(wid: WorkflowId,
                sid: StageId,
                run: Int = -1): Future[Option[Job]] = {
    pd.jobStatus(CommitRef.fromString(wid), sid, run) map { oj =>
      convertToJob(oj, wid)
    }
  }

  def listStageJobs(wid: String, sid: String): Future[Seq[Option[Job]]] = {
    pd.stageJobsStatus(CommitRef.fromString(wid), sid) map { seq =>
      seq map { oj =>
        convertToJob(oj, wid)
      }
    }
  }

  def listLatestJobs(
      wid: WorkflowId
  ): Future[Seq[Job]] = {
    pd.listLatestJobs(CommitRef.fromString(wid)) map { seq =>
      seq map { js =>
        jobFromJobStatus(wid, js)
      }
    }
  }

  def listFinalStatus(wid: WorkflowId,
                      blockingMessage: Boolean = false): Future[Seq[Job]] = {
    pd.listFinalStatus(CommitRef.fromString(wid), blockingMessage) map { seq =>
      seq map { js =>
        jobFromJobStatus(wid, js)
      }
    }
  }

  def stopJob(wid: WorkflowId, sid: StageId): Future[Unit] = {
    pd.stopJob(CommitRef.fromString(wid), sid)
  }

  /*def rerunJob(wid: WorkflowId, sid: StageId): Future[Unit] = {
    pd.rerunJob(CommitRef.fromString(wid), sid)
  }*/

  def downloadFiles(wid: WorkflowId,
                    sid: StageId,
                    from: String,
                    to: Path): Future[Unit] = {
    pd.downloadFile(CommitRef.fromString(wid), sid, from, to)
  }

  def downloadAllFiles(wid: WorkflowId,
                       sid: StageId,
                       to: Path): Future[Unit] = {
    pd.downloadPipelineOutputs(CommitRef.fromString(wid), sid, to)
  }

  def downloadWorkflowOutputs(wid: WorkflowId,
                              to: Path,
                              zip: Boolean): Future[Path] = {
    pd.downloadWorkflowOutputs(CommitRef.fromString(wid), to) map { _ =>
      if (zip) {
        val zipPath = Paths.get(s"$to.zip")
        FileUtils.zip(to, zipPath, true)
        zipPath
      } else
        to
    }
  }

  def submitData(ingressId: StageId,
                 dataFiles: Map[Path, URI] = Map(),
                 dataStr: Map[Path, String] = Map(),
                 deleteExistingData: String => Boolean = _ => false,
                 wid: Option[WorkflowId] = None): Future[WorkflowId] = {
    val fStart = wid match {
      case Some(w) =>
        Future.successful(CommitRef.fromString(w))
      case _ =>
        //Start commit
        pd.startCommit(ingressId) map { commitId =>
          CommitRef(ingressId, commitId)
        }
    }
    fStart flatMap { cref =>
      val wid = cref.toString
      //Delete files matching deleteExistingData
      val fDeleted = pd.listInputFiles(cref) flatMap { files =>
        Future.sequence(files collect {
          case f if deleteExistingData(f.file.path) =>
            pd.deleteFile(cref, Paths.get(f.file.path))
        }) map { _ =>
          Unit
        }
      }
      val fs = fDeleted flatMap { _ =>
        //pachd gets OOM killed when submitting multiple files in parallel. Use submitInputs in this case.
        /*Future.sequence(data map {
          case (from, to) =>
            //Push file
            pd.submitInput(from, to.toString, ingressId, appendExt = appendExt)
        })*/
        dataFiles.foldLeft(Future.successful(())) {
          case (acc, (to, from)) => //Push file
            acc.flatMap(_ => pd.submitInput(from, to.toString, ingressId))
        }
        dataStr.foldLeft(Future.successful(())) {
          case (acc, (to, str)) => //Push file
            acc.flatMap(_ => pd.putInput(str, wid, to.toString))
        }
      }
      //Finish commit
      fs flatMap { _ =>
        pd.finishCommit(CommitRef.fromString(wid))
      } map { _ =>
        logger.info(s"Submitted workflow wid '$wid'")
        wid
      } recoverWith {
        case e: Throwable =>
          pd.deleteCommit(CommitRef.fromString(wid))
          throw e
      }
    }
  }

  def initAnalysis(ingressId: StageId,
                   branchName: String): Future[WorkflowId] = {
    pd.createBranch(ingressId, branchName) flatMap { _ =>
      pd.startCommit(ingressId, branchName) map { commitId =>
        CommitRef(ingressId, commitId).toString
      }
    }
  }

  def abortAnalysis(wid: WorkflowId, branchName: String): Future[Unit] = {
    pd.finishCommit(CommitRef.fromString(wid)) flatMap { _ =>
      pd.deleteBranch(CommitRef.fromString(wid).repoName, branchName)
    }
  }

  def streamInput(is: InputStream,
                  wid: WorkflowId,
                  path: String): Future[Unit] = {
    pd.streamInput(is, wid, path)
  }

  def putInput(str: String, wid: WorkflowId, path: String): Future[Unit] = {
    pd.putInput(str, wid, path)
  }

  def submitOne(data: String, path: Path, ingressId: StageId): Future[Unit] = {
    pd.submitInput(data, path.toString, ingressId, "master")
  }

  def createIngress(ingressId: StageId): Future[Unit] = {
    pd.createRepo(ingressId)
  }

  def updateIngress(ingressId: StageId): Future[Unit] = {
    pd.updateRepo(ingressId)
  }

  def deleteIngress(ingressId: StageId): Future[Unit] = {
    pd.deleteRepo(ingressId)
  }

  def destroy(): Future[Unit] = {
    pd.deleteAll()
  }

  def deleteJobs(wid: WorkflowId): Future[Unit] = {
    pd.deleteCommit(CommitRef.fromString(wid))
  }

  def runAnalysis(wid: WorkflowId): Future[Unit] = {
    pd.runAnalysis(CommitRef.fromString(wid))
  }

  def check(retry: Int = 0): Future[Unit] = {
    FutureUtils.retry(pd.version() map { _ =>
      Unit
    }, retry, 1.minute)
  }

  /****** Conversion *****/
  protected def jobFromJobStatus(wid: WorkflowId, js: JobStatus): Job = {
    val processed = js.data_processed map {
      _.toInt
    } getOrElse 0
    val skipped = js.data_skipped map {
      _.toInt
    } getOrElse 0
    val total = js.data_total map {
      _.toInt
    } getOrElse 0
    val state = JobState(js.state)
    val remaining = total - (processed + skipped)
    Job(
      wid = wid,
      sid = js.pipeline.name,
      started = js.started,
      finished = js.finished map {
        stringToDate
      },
      state = state,
      metadata = Map(
        "jobId" -> js.job.id,
        "out_branch" -> js.output_branch,
        "out_commit" -> (js.output_commit map {
          _.toCommitRef.toString
        }).getOrElse("")
      ),
      datum_success = processed,
      datum_skipped = skipped,
      datum_remaining = remaining,
      datum_total = total
    )
  }

  protected def outputFileFromFileStatus(wid: WorkflowId,
                                         sid: StageId,
                                         os: FileStatus): Option[OutputFile] = {
    if (os.isFile) {
      val date = os.committed.getOrElse("") //needed here otherwise it doesn't compile for some reason
      Some(
        OutputFile(
          wid = wid,
          sid = sid,
          path = Paths.get(os.file.path),
          creationDate = date,
          size = os.sizeBytes.map { _.toLong }.getOrElse(0),
          metadata = Map(
            "commit" -> os.file.commit.toCommitRef.toString
          )
        )
      )
    } else
      None
  }

  protected def activeStageFromPipelineStatus(
      ps: PipelineStatus): ActiveStage = {
    ActiveStage(
      id = ps.pipeline.name,
      createdDate = ps.created_at,
      state = StageState.apply(ps.state),
      metadata = Map(
        "out_branch" -> ps.output_branch
      )
    )
  }

  protected def ingressFromRepoStatus(rs: RepoStatus): Ingress = {
    Ingress(
      id = rs.repo.name,
      createdDate = rs.created,
      size = rs.sizeBytes.map { _.toLong }.getOrElse(0),
      metadata = Map(
        "branches" -> rs.branches.map { _.name }.mkString(",")
      )
    )
  }
}
