package no.uit.sfb.pachyderm.models

case class CommitRef(repoName: String, commitId: String) {
  override def toString: String = s"$repoName@$commitId"
}

object CommitRef {
  def fromString(str: String) = {
    val regexp = "^(\\w+)@(\\w+)$".r
    str match {
      case regexp(repo, cid) => CommitRef(repo, cid)
      case _                 => throw new Exception(s"Could not parse CommitId from '$str'")
    }
  }
}
