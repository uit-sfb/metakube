package no.uit.sfb.pachyderm

import java.io.InputStream
import java.net.URI
import java.nio.file.{Path, Paths}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.github.blemale.scaffeine.Scaffeine
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metakube.wm.WorkflowManagerLike.WorkflowId
import no.uit.sfb.pachyderm.PachydermDriver.UnreachableException
import no.uit.sfb.pachyderm.models.CommitRef
import no.uit.sfb.pachyderm.models.status.{
  BranchStatus,
  CommitRefStatus,
  CommitStatus,
  FileStatus,
  JobStatus,
  PipelineStatus,
  RepoStatus
}
import no.uit.sfb.scalautils.common.{
  FileUtils,
  FutureUtils,
  Random,
  SystemProcess
}
import no.uit.sfb.scalautils.json.Json

import scala.collection.mutable
import scala.language.implicitConversions
import scala.concurrent.{ExecutionContext, Future, blocking}
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class PachydermDriver(
    host: String,
    port: Int = 30650,
    cacheExpiration: Duration = 5.minutes, //We need a short expiration time since some outside event might make the cached data obsolete such as pipeline update for instance
    cacheMaxSize: Long = 1000,
    contextName: String = Random.alphanumeric(5))(
    implicit val ec: ExecutionContext)
    extends LazyLogging {
  //We set pachyderm context up
  SystemProcess
    .stdin(
      s"""{"pachd_address":"$host:$port"}""",
      SystemProcess(s"pachctl config set context $contextName --overwrite"))
    .exec()
  SystemProcess
    .apply(s"pachctl config set active-context $contextName")
    .exec()

  //The caches cannot be invalidated. You would need to throw out this instance of PachydermDriver and start up a new one.

  //Usage: commitsCache.get(scr)
  //Note: in case a nes pipeline is added after the cache was filled for this entry, the new pipeline would never be detected by the plugin. So be it.
  //Note: do not use me directly! Use commitRef() or commitRefT().
  protected val commitsCache =
    Scaffeine()
      .expireAfterWrite(cacheExpiration)
      .maximumSize(cacheMaxSize)
      .buildAsyncFuture[CommitRef, Seq[(String, Seq[CommitRef])]](scr =>
        triggeredCommits(scr))

  /**
    * Get a commit ref from a seed commit ref and a pipeline id
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param run If -1 returns the latest run. Otherwise returns run number `run`
    * @return Option of the commit ref
    */
  protected def commitRef(scr: CommitRef,
                          pid: String,
                          run: Int = -1): Future[Option[CommitRef]] = {
    commitsCache.get(scr).map(_.find(_._1 == pid)) map { s =>
      s flatMap {
        case (_, ss) =>
          if (run < 0)
            ss.lastOption
          else
            ss.lift(run)
      }
    }
  }

  /**
    * Same as [[commitRef]] except it throws an exception in case no commit is found for the provided pipeline id
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @return
    */
  protected def commitRefT(scr: CommitRef,
                           pid: String,
                           run: Int = -1): Future[CommitRef] = {
    commitRef(scr, pid, run) map {
      case Some(c) => c
      case None =>
        throw new Exception(
          s"Could not get commit for seed commit '$scr' and pipeline '$pid'")
    }
  }

  //Usage: jobCache.get((scr, pid, run))
  //Note: do not use me directly! Use jobId() or jobIdT().
  protected val jobCache =
    Scaffeine()
      .expireAfterWrite(cacheExpiration)
      .maximumSize(cacheMaxSize)
      .buildAsyncFuture[(CommitRef, String, Int), String] {
        case (scr, pid, run) =>
          jobStatus(scr, pid, run) map {
            case Some(v) => v.job.id
            case None =>
              logger.info(
                s"No job found for seed commit ref '$scr' and pid '$pid' (most likely pending). Not cached.")
              null //We return null so that the cache is not updated
          }
      }

  /**
    * Get a job id from a seed commit ref and a pipeline id
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param run Run number (if -1, the latest)
    * @return Option of job id
    */
  protected def jobId(scr: CommitRef,
                      pid: String,
                      run: Int = -1): Future[Option[String]] = {
    jobCache
      .get((scr, pid, run))
      .map(Option(_)) //Option(_) since .get can return null
  }

  /**
    * Same as [[jobId]] except it throws an exception in case no job is found for the provided pipeline id
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @return
    */
  protected def jobIdT(scr: CommitRef, pid: String): Future[String] = {
    jobId(scr, pid) map {
      case Some(jobid) =>
        jobid
      case None =>
        throw new Exception(
          s"Could not get jobId for seed commit '$scr' and pipeline '$pid'")
    }
  }

  //Usage: outputs.get(commitRef)
  //Do not use me directly! Use files() instead
  protected val filesCache =
    Scaffeine()
      .expireAfterWrite(cacheExpiration)
      .maximumSize(cacheMaxSize)
      .buildAsyncFuture[CommitRef, Seq[FileStatus]] { commitRef =>
        Future {
          blocking {
            listFilesLoop(commitRef)
          }
        } recover {
          case e: Throwable =>
            logger.info(
              s"Files exploration failed for commit '$commitRef' due to ${e.getMessage}. Not cached.")
            null //We return null so that the cache is not updated
        }
      }

  protected def files(cr: CommitRef): Future[Seq[FileStatus]] = {
    filesCache
      .get(cr)
      .map(outputs =>
        Option(outputs) match {
          case Some(v) => v
          case None    => Seq()
      }) //Option(_) since .get can return null
  }

  protected def driver(cmd: String): Unit = {
    driverF(cmd, _ => ())
  }

  protected def driverF[T](cmd: String, parse: Seq[String] => T): T = {
    val pb = s"pachctl $cmd"
    val (ret, out, err) = SystemProcess(pb).execQ()
    if (ret == 0)
      parse(out)
    else if (err.exists { _.contains("context deadline exceeded") })
      throw UnreachableException()
    else
      throw new RuntimeException(
        s"System process '$pb' returned non-zero status code: '$ret'")
  }

  protected def driverFO[T](cmd: String, parse: Seq[String] => T): Option[T] = {
    Try {
      driverF(cmd, parse)
    } match {
      case Success(v)                       => Some(v)
      case Failure(e: UnreachableException) => throw e
      case _                                => None
    }
  }

  def version(): Future[String] = {
    Future {
      blocking {
        driverF("version", _ mkString "\n")
      }
    }
  }

  /**
    * Get job's log
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @return Log
    */
  def jobLogs(scr: CommitRef, pid: String, run: Int = -1): Future[String] = {
    jobId(scr, pid, run) map {
      case Some(jobid) =>
        driverF(s"logs --job $jobid", _.mkString("\n"))
      case None => s"Job pending."
    }
  }

  /**
    * Print job's log
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param follow If true, blocks and follow log
    * @return
    */
  def printJobLogs(scr: CommitRef,
                   pid: String,
                   follow: Boolean = false): Future[Unit] = {
    val followFlag = if (follow) "-f" else ""
    jobId(scr, pid) map {
      case Some(jobid) =>
        driver(s"logs --job $jobid $followFlag")
      case None => println("Job pending.")
    }
  }

  /**
    * Inspect repo
    *
    * @param repo Repo name (same as pipeline id)
    * @return
    */
  def inspectRepo(repo: String): Future[Option[RepoStatus]] = {
    Future {
      blocking {
        driverFO(s"inspect repo $repo --raw",
                 r =>
                   Json
                     .parse[RepoStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * List all repos
    *
    * @return
    */
  def listRepos(): Future[Seq[RepoStatus]] = {
    Future {
      blocking {
        driverF(s"list repo --raw",
                r => Json.parseMulti[RepoStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * Inspect pipeline
    *
    * @param pid Pipeline id
    * @return
    */
  def inspectPipeline(pid: String): Future[Option[PipelineStatus]] = {
    Future {
      blocking {
        driverFO(s"inspect pipeline $pid --raw",
                 r =>
                   Json
                     .parse[PipelineStatus](r.mkString("\n")))
      }
    }
  }

  //Cannot be used as long as the commit happens on <none> branch
  //Do not use me directly!!
  /*protected def rerunJob(commitRef: CommitRef): Future[Unit] = {
    Future {
      blocking {
        driver(
          s"run pipeline ${commitRef.repoName} ${commitRef.commitId}"
        )
      }
    }
  }*/

  //Cannot be used as long as the commit happens on <none> branch
  /*/**
   * Re-run a job
   * @param scr Seed commit ref
   * @param pid Pipeline id
   * @return
   */
  def rerunJob(scr: CommitRef, pid: String): Future[Unit] = {
    commitRefT(scr, pid) flatMap { commit =>
      rerunJob(commit) map { _ =>
        //We remove from cache since a new commit replace an old one
        commitsCache.put(scr, Future.failed(new Exception()))
        jobCache.put((scr, pid), Future.failed(new Exception()))
        filesCache.put(commit, Future.failed(new Exception()))
      }
    }
  }*/

  /**
    * List all pipelines
    *
    * @return
    */
  def listPipelines(): Future[Seq[PipelineStatus]] = {
    Future {
      blocking {
        driverF(s"list pipeline --raw",
                r => Json.parseMulti[PipelineStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * List commit's output files
    *
    * @param commitRef
    * @param path Only list files located under this path
    * @return
    */
  protected def listCommitFiles(commitRef: CommitRef,
                                path: String): Future[Seq[FileStatus]] = {
    files(commitRef) map {
      _.filter { os =>
        os.file.path.startsWith(path)
      }
    }
  }

  /**
    * List input repo's files
    *
    * @param commitRef
    * @param path Only list files located under this path
    * @return
    */
  def listInputFiles(commitRef: CommitRef,
                     path: String = "/"): Future[Seq[FileStatus]] = {
    listCommitFiles(commitRef, path)
  }

  /**
    * Helper method to visit directory structure of an output repo recursively
    * ATTENTION: Do NOT use me! Use outputsCache instead!
    * ATTENTION: synchronous!!!
    * Note: Throws if the commit is not finished.
    *
    * @param commitRef Commit ref
    * @param path Only visit files located under this path
    * @param acc Accumulator
    * @return
    */
  protected def listFilesLoop(commitRef: CommitRef,
                              path: String = "",
                              acc: Seq[FileStatus] = Seq()): Seq[FileStatus] = {
    val oOutputs =
      driverF(s"list file $commitRef:$path --raw",
              r => Json.parseMulti[FileStatus](r.mkString("\n")))
    oOutputs match {
      case Seq() => acc
      case outputs =>
        outputs flatMap { output =>
          if (output.file.path == path) //To be sure we do not get into any infinite loop
            acc
          else {
            if (output.isDir) {
              listFilesLoop(commitRef, output.file.path, acc)
            } else {
              acc :+ output
            }
          }
        }
    }
  }

  /**
    * List job's outputs or input repo files
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param path Only list files located under this path
    * @return
    */
  def listOutputs(scr: CommitRef,
                  pid: String,
                  path: String = "/"): Future[Seq[FileStatus]] = {
    commitRefT(scr, pid) flatMap { commitRef =>
      listCommitFiles(commitRef, path)
    }
  }

  /**
    * List all files
    *
    * @param scr Seed commit ref
    * @return
    */
  def listAllOutputs(scr: CommitRef): Future[Map[String, Seq[FileStatus]]] = {
    commitsCache.get(scr) flatMap { commits =>
      val cs = commits flatMap {
        case (_, ss) =>
          ss.lastOption

      }
      FutureUtils
        .sequenceSuccessful(cs map { commit =>
          listCommitFiles(commit, "/") map { os =>
            commit.repoName -> os
          }
        }) map {
        _.toMap
      }
    }
  }

  /**
    * Inspect job
    *
    * @param jobId
    * @return
    */
  protected def inspectJob(jobId: String): Future[Option[JobStatus]] = {
    Future {
      blocking {
        driverFO(s"inspect job $jobId --raw",
                 r =>
                   Json
                     .parse[JobStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * Create pipeline (throws if already exsists)
    *
    * @param pjson Path to pipeline definition file (json)
    * @return
    */
  def createPipeline(pjson: Path): Future[Unit] = {
    Future {
      blocking {
        driver(s"create pipeline -f $pjson")
      }
    }
  }

  /**
    * Delete pipeline
    *
    * @param pid Pipeline id
    * @return
    */
  def deletePipeline(pid: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"delete pipeline $pid -f")
      }
    }
  }

  /**
    * Start pipeline
    *
    * @param pid Pipeline id
    * @return
    */
  def startPipeline(pid: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"start pipeline $pid")
      }
    }
  }

  /**
    * Stop pipeline
    *
    * @param pid Pipeline id
    * @return
    */
  def stopPipeline(pid: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"stop pipeline $pid")
      }
    }
  }

  /**
    * Update pipeline (or create if not exist)
    *
    * @param pjson Path to pipeline definition file (json)
    * @return
    *  Note: We don't allow to use --reprocess flag as it is messy. Instead use rerunJob
    */
  def updatePipeline(pjson: Path): Future[Unit] = {
    Future {
      blocking {
        driver(s"update pipeline -f $pjson")
      }
    }
  }

  /**
    * Print list of pipeline's commits
    * @param pid Pipeline id
    * @param branch Branch
    * @return
    */
  def printCommits(pid: String, branch: String = "master"): Future[Unit] = {
    val ref = CommitRef(pid, branch)
    Future {
      blocking {
        driver(s"list commit $ref")
      }
    }
  }

  /**
    * List pipeline's commits
    * @param pid Pipeline id
    * @param branch If provided, return the head commit of the branch
    * @return
    */
  def listCommits(pid: String,
                  branch: Option[String] = None): Future[Seq[CommitStatus]] = {
    val ref = branch match {
      case Some(br) =>
        CommitRef(pid, br).toString
      case _ =>
        pid
    }
    Future {
      blocking {
        driverF(s"list commit $ref --raw",
                r =>
                  Json
                    .parseMulti[CommitStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * List all commits generated by a seed commit
    * ATTENTION: Do NOT use me directly! Use the commitsCache instead!
    *
    * @param scr Seed commit ref
    * @param repo Repo name (same as pid)
    * @return
    */
  protected def triggeredCommits(
      scr: CommitRef,
      repo: Option[String] = None): Future[Seq[(String, Seq[CommitRef])]] = {
    val fCommits: Future[Seq[CommitRefStatus]] =
      inspectCommit(scr) map {
        _.subvenance.map {
          _.upper //upper != lower if a pipeline has been re-run. We are only interested in upper since it represents our latest re-try
        }
      }
    val fCommitRefs: Future[Seq[CommitRef]] = fCommits map { commits =>
      (repo match {
        case Some(n) =>
          commits.filter {
            _.repo.name == n
          }
        case None => commits
      }).map {
        _.toCommitRef
      }
    }
    //We remove any commit belonging to `stats` branch
    val fFilteredCommitRefs: Future[Seq[CommitRef]] = (fCommitRefs flatMap {
      commits =>
        Future.sequence(commits map { inspectCommit })
    }) map {
      _ collect {
        case commit if commit.branch.name != "stats" =>
          commit.commit.toCommitRef
      }
    }
    //Because of retries, one repo can have multiple commit refs
    fFilteredCommitRefs map { seq =>
      seq.foldLeft(Seq[(String, mutable.ListBuffer[CommitRef])]()) {
        case (acc, v) =>
          val stage = v.repoName
          acc.find(_._1 == stage) match {
            case Some((_, ss)) =>
              ss += v
              acc
            case None => acc :+ (stage, mutable.ListBuffer(v))
          }
      }
    }
  }

  /**
    * Start a commit
    *
    * @param repo Repo name (same as pid)
    * @param branch Branch
    * @return
    */
  def startCommit(repo: String, branch: String = "master"): Future[String] = {
    val ref = CommitRef(repo, branch)
    Future {
      blocking {
        driverF(s"start commit $ref", _.head)
      }
    }
  }

  def streamInput(is: InputStream,
                  wid: WorkflowId,
                  path: String): Future[Unit] = {
    Future {
      blocking {
        SystemProcess
          .streamIn(is, SystemProcess(s"pachctl put file $wid:$path"))
          .exec()
      }
    }
  }

  def putInput(str: String, wid: WorkflowId, path: String): Future[Unit] = {
    Future {
      blocking {
        SystemProcess
          .stdin(str, SystemProcess(s"pachctl put file $wid:$path"))
          .exec()
      }
    }
  }

  def deleteBranch(repoName: String, branchName: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"delete branch $repoName@$branchName")
      }
    }
  }

  /**
    * Finish a commit
    *
    * @param commitRef
    * @return
    */
  def finishCommit(commitRef: CommitRef): Future[Unit] = {
    Future {
      blocking {
        driver(s"finish commit $commitRef")
      }
    }
  }

  /**
    * Delete a commit
    *
    * @param commitRef
    * @return
    */
  def deleteCommit(commitRef: CommitRef): Future[Unit] = {
    Future {
      blocking {
        driver(s"delete commit $commitRef")
      }
    }
  }

  /**
    * Inspect commit
    *
    * @param commitRef
    * @return
    */
  protected def inspectCommit(commitRef: CommitRef): Future[CommitStatus] = {
    Future {
      blocking {
        driverF(s"inspect commit $commitRef --raw",
                r => Json.parse[CommitStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * Print all pipeline's banches
    * @param pid Pipeline id
    * @return
    */
  def printBranches(pid: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"list branch $pid")
      }
    }
  }

  def listBranches(pid: String): Future[Seq[BranchStatus]] = {
    Future {
      blocking {
        driverF(s"list branch $pid --raw",
                r => Json.parseMulti[BranchStatus](r.mkString("\n")))
      }
    }
  }

  def headCommit(pid: String, branch: String): Future[Option[CommitStatus]] = {
    listCommits(pid, Some(branch)) map { commits =>
      commits.headOption
    }
  }

  /**
    * Get job's status
    *
    * @param commitRef
    * @return
    */
  protected def jobStatus(commitRef: CommitRef): Future[Option[JobStatus]] = {
    Future {
      blocking {
        driverFO[Seq[JobStatus]](
          s"list job -o $commitRef --no-pager --raw",
          r => Json.parseMulti[JobStatus](r.mkString("\n")))
          .flatMap { _.headOption }
      }
    }
  }

  /**
    * Get job's status
    *
    * @param scr Seed commit
    * @param pid Pipeline id
    * @return
    */
  def jobStatus(scr: CommitRef,
                pid: String,
                run: Int): Future[Option[JobStatus]] = {
    commitRefT(scr, pid, run) flatMap { commitRef =>
      jobStatus(commitRef)
    }
  }

  /**
    * Create/update a branch (= pointer to a commit)
    *
    * @param repo Repo to create the branch on
    * @param branch Branch name
    * @param commit Commit id to point to. Must be a commit belonging to the repo `repo`. Can be another branch incl ^ notation or a commit id. If None, the branch points to <nil>
    * @return
    */
  def createBranch(repo: String,
                   branch: String,
                   commit: Option[String] = None): Future[Unit] = {
    val headParam = commit
      .map { h =>
        s"--head $h"
      }
      .getOrElse("")
    Future {
      blocking {
        driver(s"create branch $repo@$branch $headParam")
      }
    }
  }

  /**
    * Re-run an analysis
    *
    * @param scr Seed commit
    * @return
    */
  def runAnalysis(scr: CommitRef): Future[Unit] = {
    val repo = scr.repoName
    //If commit not finished throw exception
    inspectCommit(scr) flatMap { commit =>
      commit.finished match {
        case Some(_) =>
          val fBefore = headCommit(repo, "master") flatMap {
            case Some(headCommit) if headCommit.commit.toCommitRef == scr =>
              //If the latest commit is the same as the one we want to switch to,
              //We need to switch first to a dummy commit the back otherwise nothing happens
              //A good dummy commit is the initial commit on master which is used for testing the readiness of the pipeline
              listCommits(repo) flatMap { commits =>
                val masterCommits = commits.filter(_.branch.name == "master")
                val oDummyCommit = masterCommits.lastOption.map {
                  _.commit.id
                }
                createBranch(scr.repoName, "master", oDummyCommit)
              }
            case _ =>
              //Otherwise we don't need to do anything
              Future.successful()
          }
          fBefore.flatMap { _ =>
            createBranch(scr.repoName, "master", Some(scr.commitId)) map { _ =>
              //Clear the cache entry for that commit
              commitsCache.underlying.synchronous().invalidate(scr)
              jobCache.underlying.synchronous().invalidateAll()
              filesCache.underlying.synchronous().invalidateAll()
            }
          }
        case None =>
          throw new IllegalStateException(
            s"Commit '$scr' cannot be activated because it is not finished.")
      }
    }
  }

  def stageJobsStatus(scr: CommitRef,
                      pid: String): Future[Seq[Option[JobStatus]]] = {
    commitsCache.get(scr) flatMap { commits =>
      Future.sequence(commits.find(_._1 == pid).get._2 map jobStatus)
    }
  }

  /**
    * List all jobs that were triggered by seed commit
    * Note: pending jobs are excluded
    *
    * @param scr Seed commit ref
    * @return
    */
  def listLatestJobs(
      scr: CommitRef
  ): Future[Seq[JobStatus]] = {
    commitsCache.get(scr) flatMap { commits =>
      val cs = commits flatMap {
        case (_, ss) =>
          ss.lastOption
      }
      FutureUtils.sequenceSuccessful(cs.map { commitRef =>
        jobStatus(commitRef)
      }) map { s =>
        s.flatten
      }
    }
  }

  @deprecated("This command is slow.")
  /**
    * Print all jobs that were triggered by a seed commit and optionally match the provided pid
    * Note: pending jobs are not displayed
    *
    * @param scr Seed commit ref (option)
    * @param pid Pipeline id (option)
    * @return
    */
  def printJobs(scr: Option[CommitRef] = None,
                pid: Option[String] = None): Future[Unit] = {
    val inputCommit: String = scr match {
      case Some(commitRef) => s"-i $commitRef"
      case None            => ""
    }
    val pipeline = pid match {
      case Some(pname) => s"-p $pname"
      case None        => ""
    }
    Future {
      blocking {
        driver(s"list job $pipeline $inputCommit --no-pager")
      }
    }
  }

  /**
    * Same as [[listLatestJobs]] but blocks until the last job is complete
    *
    * @param scr Seed commit ref
    * @param blockingMessage If set to true, prints a waiting message
    * @return
    */
  def listFinalStatus(
      scr: CommitRef,
      blockingMessage: Boolean = false): Future[Seq[JobStatus]] = {
    if (blockingMessage)
      println("Analysis ongoing. Please wait for all the jobs to finish...")
    Future {
      blocking {
        driverF(s"flush job $scr --raw --full-timestamps",
                r => Json.parseMulti[JobStatus](r.mkString("\n")))
      }
    }
  }

  /**
    * Same as [[printJobs]] but blocks until the last job is complete
    *
    * @param scr Seed commit ref
    * @param blockingMessage If set to true, prints a waiting message
    * @return
    */
  def printFinalStatus(scr: CommitRef,
                       blockingMessage: Boolean = false): Future[Unit] = {
    if (blockingMessage)
      println("Analysis ongoing. Please wait for all the jobs to finish...")
    Future {
      blocking {
        driver(s"flush job $scr --full-timestamps")
      }
    }
  }

  /**
    * Stop a job
    * @param jobId
    * @return
    */
  protected def stopJob(jobId: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"stop job $jobId")
      }
    }
  }

  /**
    * Stop a job
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @return
    */
  def stopJob(scr: CommitRef, pid: String): Future[Unit] = {
    jobIdT(scr, pid) flatMap {
      stopJob
    }
  }

  /**
    * Print list of repos
    *
    * @return
    */
  def printRepos(): Future[Unit] = {
    Future {
      blocking {
        driver(s"list repo")
      }
    }
  }

  /**
    * Print list of piplines
    *
    * @return
    */
  def printPipelines(): Future[Unit] = {
    Future {
      blocking {
        driver(s"list pipeline")
      }
    }
  }

  /**
    * Download output
    *
    * @param commitRef
    * @param from Path designating the file to download
    * @param to Includes the destination file name
    */
  protected def downloadOutput(commitRef: CommitRef,
                               from: String,
                               to: Path): Future[Unit] = {
    FileUtils.createParentDirs(to)
    Future {
      blocking {
        driver(s"get file $commitRef:$from -o $to")
      }
    }
  }

  def getOutput(commitRef: CommitRef, from: String): Future[String] = {
    Future {
      blocking {
        driverF(s"get file $commitRef:$from", _.mkString("\n"))
      }
    }
  }

  /**
    * Download output
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param from Path designating the file to download
    * @param to Includes the destination file name
    */
  def downloadFile(scr: CommitRef,
                   pid: String,
                   from: String,
                   to: Path): Future[Unit] = {
    commitRefT(scr, pid) flatMap { commitRef =>
      downloadOutput(commitRef, from, to)
    }
  }

  def getFile(scr: CommitRef, pid: String, from: String): Future[String] = {
    commitRefT(scr, pid) flatMap { commitRef =>
      getOutput(commitRef, from)
    }
  }

  /**
    * Download commit outputs to path
    *
    * @param scr Seed commit ref
    * @param pid Pipeline id
    * @param to Folder where to download the files (created if not exist)
    * @return
    */
  def downloadPipelineOutputs(scr: CommitRef,
                              pid: String,
                              to: Path): Future[Unit] = {
    val fCommitRef = commitRefT(scr, pid)
    val fOutputs = fCommitRef flatMap files
    (fCommitRef zip fOutputs) flatMap {
      case (commitRef, outputs) =>
        FutureUtils.sequenceSuccessful(outputs.map { os =>
          val p = os.file.path
          downloadOutput(commitRef, p, to.resolve(p.stripPrefix("/")))
        }) map { _ =>
          Unit
        }
    }
  }

  /**
    * Download all  outputs generated by one seed commit
    *
    * @param scr Seed commit ref
    * @param to Folder where to download the files (each pipeline will have it's own sub directory under 'to')
    */
  def downloadWorkflowOutputs(scr: CommitRef, to: Path): Future[Unit] = {
    commitsCache.get(scr) flatMap { commits =>
      val cs = commits flatMap {
        case (_, ss) =>
          ss.lastOption

      }
      Future.sequence(cs.map { cr =>
        downloadPipelineOutputs(scr, cr.repoName, to.resolve(cr.repoName))
      }) map { _ =>
        Unit
      }
    }
  }

  /**
    * Delete a file in input repo
    *
    * @param scr  : Seed commit ref
    * @param path Path of the file to delete
    * @return
    */
  def deleteFile(scr: CommitRef, path: Path): Future[Unit] = {
    Future {
      blocking {
        driver(s"delete file $scr:/$path")
      }
    }
  }

  /**
    * Submit input file (overrides if exists)
    *
    * @param from File to add to commit
    * @param to Path where the file should be committed to
    * @param repo Repo name (same as pipeline id)
    * @param branch Branch to commit to
    * @return
    */
  def submitInput(from: URI,
                  to: String,
                  repo: String,
                  branch: String = "master"): Future[Unit] = {
    val ref = CommitRef(repo, branch)
    val urlOrPath = from.getScheme match {
      case "file" => from.getPath
      case _      => from
    }
    Future {
      blocking {
        driver(s"put file -o $ref:/$to -f $urlOrPath")
      }
    }
  }

  def submitInput(str: String,
                  to: String,
                  repo: String,
                  branch: String): Future[Unit] = {
    val ref = CommitRef(repo, branch)
    Future {
      blocking {
        SystemProcess
          .stdin(str, SystemProcess(s"pachctl put file -o $ref:/$to"))
          .exec
      }
    }
  }

  /**
    * Create repo (throw if already exists)
    *
    * @param repo Repo name (same as pipeline id)
    * @return
    */
  def createRepo(repo: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"create repo $repo")
      }
    }
  }

  /**
    * Update repo (or create if not exist)
    *
    * @param repo Repo name (same as pipeline id)
    * @return
    */
  def updateRepo(repo: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"update repo $repo")
      }
    }
  }

  /**
    * Delete repo
    *
    * @param repo Repo name (same as pipeline id)
    * @return
    */
  def deleteRepo(repo: String): Future[Unit] = {
    Future {
      blocking {
        driver(s"delete repo $repo -f")
      }
    }
  }

  /**
    * Delete all pachyderm resources (repo, pipelines, data, commits, ...)
    *
    * @return
    */
  def deleteAll(): Future[Unit] = {
    Future {
      blocking {
        SystemProcess
          .stdin("y", SystemProcess("pachctl delete all"))
          .exec()
      }
    }
  }
}

object PachydermDriver {
  case class UnreachableException()
      extends RuntimeException("Pachyderm is currently unreachable")

  private val format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
  implicit def stringToDate(str: String): LocalDateTime = {
    LocalDateTime.parse(str.take(23), format)
  }
}
