package no.uit.sfb.pachyderm.models.status

case class JobStatus(job: JobIdStatus,
                     pipeline: PipelineNameStatus,
                     started: String,
                     finished: Option[String],
                     output_commit: Option[CommitRefStatus],
                     state: String,
                     output_branch: String,
                     data_processed: Option[String],
                     data_total: Option[String],
                     data_skipped: Option[String],
                     pipeline_version: String)

case class JobIdStatus(id: String)

case class PipelineNameStatus(name: String)
