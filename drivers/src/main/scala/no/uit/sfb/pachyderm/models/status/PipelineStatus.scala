package no.uit.sfb.pachyderm.models.status

import no.uit.sfb.pachyderm.models.spec.{
  ParallelismSpec,
  PipelineInput,
  PipelineName,
  Transform
}

case class PipelineStatus(
    pipeline: PipelineName,
    version: String,
    description: String = "",
    output_branch: String,
    transform: Transform,
    input: PipelineInput,
    parallelism_spec: ParallelismSpec,
    state: String = "",
    last_job_state: String = "",
    created_at: String = "",
    pod_patch: String = ""
)
