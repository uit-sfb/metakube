package no.uit.sfb.pachyderm.models.status

import no.uit.sfb.pachyderm.models.CommitRef

case class CommitStatus(commit: CommitRefStatus,
                        childCommits: Option[Seq[CommitRefStatus]],
                        started: String,
                        finished: Option[String],
                        sizeBytes: Option[String],
                        subvenance: Seq[CommitTandemStatus],
                        branch: BranchRefStatus)

case class CommitTandemStatus(lower: CommitRefStatus, upper: CommitRefStatus)

case class CommitRefStatus(repo: RepoNameStatus, id: String) {
  lazy val toCommitRef = CommitRef(repo.name, id)
}
