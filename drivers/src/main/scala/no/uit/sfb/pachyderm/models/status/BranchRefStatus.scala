package no.uit.sfb.pachyderm.models.status

case class BranchRefStatus(name: String, repo: RepoNameStatus)
