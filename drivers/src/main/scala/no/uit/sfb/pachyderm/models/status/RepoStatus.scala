package no.uit.sfb.pachyderm.models.status

case class RepoStatus(repo: RepoNameStatus,
                      created: String,
                      sizeBytes: Option[String],
                      branches: Seq[BranchRefStatus])

case class RepoNameStatus(name: String)
