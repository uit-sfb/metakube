package no.uit.sfb.pachyderm.models.status

case class FileStatus(file: FileDescrStatus,
                      fileType: String,
                      sizeBytes: Option[String] = None,
                      committed: Option[String] = None,
                      hash: String = "") {
  val isFile = fileType == "FILE"
  val isDir = fileType == "DIR"
}

case class FileDescrStatus(commit: CommitRefStatus, path: String)
