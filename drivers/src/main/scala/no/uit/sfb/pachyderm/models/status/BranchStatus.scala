package no.uit.sfb.pachyderm.models.status

case class BranchStatus(name: String,
                        branch: BranchRefStatus,
                        head: Option[HeadStatus])

case class HeadStatus(id: String, repo: RepoNameStatus)
