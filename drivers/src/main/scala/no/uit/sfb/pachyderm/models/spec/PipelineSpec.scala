package no.uit.sfb.pachyderm.models.spec

import com.fasterxml.jackson.annotation.JsonCreator

case class PipelineSpec(pipeline: PipelineName,
                        description: String = "",
                        transform: Transform,
                        input: PipelineInput,
                        enable_stats: Boolean = false,
                        standby: Boolean = false,
                        parallelism_spec: ParallelismSpec,
                        pod_patch: String = "",
                        job_timeout: Option[String] = None,
                        //Do not use as do not support milli CPU
                        //resource_requests: Option[ResourcesValues] = None,
                        //resource_limits: Option[ResourcesValues] = None,
                        datum_tries: Option[Int] = None,
                        scheduling_spec: SchedulingSpec = SchedulingSpec()) {
  lazy val parents: Set[String] = input.parents()
}

case class SchedulingSpec(node_selector: Map[String, String] = Map(),
                          priority_class_name: Option[String] = None)

// If empty string: undefined
case class ResourcesValues(cpu: String = "",
                               memory: String = "",
                               disk: String = "") {
  @JsonCreator
  def this() = this("", "", "")

  def merge(layer: ResourcesValues): ResourcesValues = {
    this.copy(
      cpu = {
        if (layer.cpu.isEmpty)
          cpu
        else
          layer.cpu
      },
      memory = {
        if (layer.memory.isEmpty)
          memory
        else
          layer.memory
      },
      disk = {
        if (layer.disk.isEmpty)
          disk
        else
          layer.disk
      }
    )
  }
}

case class PipelineInput(pfs: Option[Pfs] = None,
                         cross: Option[Seq[PipelineInput]] = None,
                         union: Option[Seq[PipelineInput]] = None,
                         join: Option[Seq[PipelineInput]] = None,
                        ) {
  //pfs XOR cross XOR union
  assert(
    pfs
      .map { _ =>
        1
      }
      .getOrElse(0) + cross
      .map { _ =>
        1
      }
      .getOrElse(0) + union
      .map { _ =>
        1
      }
      .getOrElse(0) + join
      .map { _ =>
        1
      }
      .getOrElse(0) <= 1) //only one at a time or all empty (for pipeline with empty inputs)

  //Join should contain only pfs
  assert(join match {
    case Some(j) => j.forall(_.pfs.nonEmpty)
    case _ => true
  })

  //Join should contain only pfs with non-empty join_on
  assert(join match {
    case Some(j) => j.forall(_.pfs.get.join_on.nonEmpty)
    case _ => true
  })

  //Cross should contain only pfs with empty join_on
  assert(cross match {
    case Some(c) =>
      c.forall(
        _.pfs
          .forall(x => !Option(x.join_on).exists(_.nonEmpty)))
    case _ => true
  })

  //Union should contain only pfs with empty join_on
  assert(union match {
    case Some(u) =>
      u.forall(
        _.pfs
          .forall(x => !Option(x.join_on).exists(_.nonEmpty)))
    case _ => true
  })

  /**
   * Finds all the parent of a pipeline recursively
   *
   * @return
   */
  def parents(): Set[String] = {
    val h: Set[String] =
      (cross.getOrElse(Seq()) ++ union.getOrElse(Seq()) ++ join.getOrElse(
        Seq()))
        .foldLeft(Seq[String]()) {
          case (acc, pi) =>
            acc ++ pi.parents()
        }
        .toSet
    h ++ (pfs match {
      case Some(pi) => Set(pi.repo)
      case None => Set()
    })
  }
}

object PipelineInput {
  def compact(pi: PipelineInput): PipelineInput = {
    compactImpl(pi).getOrElse(PipelineInput())
  }

  //Merge consecutive unions (resp. crosses) together since they are associative
  protected def compactImpl(pi: PipelineInput): Option[PipelineInput] = {
    if (pi.pfs.nonEmpty || pi.join
      .getOrElse(Seq())
      .nonEmpty) //Join: Do not compact join since it can only contain pfs (which are not compactable)
    Some(pi)
    else if (pi.cross.nonEmpty) {
      val compacted = PipelineInput(cross = Some(pi.cross.get.flatMap {
        compactImpl
      }))
      val (same, other) = compacted.cross.get partition {
        _.cross.nonEmpty
      }
      val out = compacted.copy(cross = Some(other ++ same.flatMap {
        _.cross.get
      }))
      if (out.cross.get.nonEmpty)
        Some(out)
      else
        None
    } else if (pi.union.nonEmpty) {
      val compacted = PipelineInput(union = Some(pi.union.get.flatMap {
        compactImpl
      }))
      val (same, other) = compacted.union.get partition {
        _.union.nonEmpty
      }
      val out = compacted.copy(union = Some(other ++ same.flatMap {
        _.union.get
      }))
      if (out.union.get.nonEmpty)
        Some(out)
      else
        None
    } else
      None
  }
}

case class Pfs(
                glob: String,
                repo: String = "", //If not defined, takes overrideDefaultInputRepo setting
                name: String = "", //If not defined, takes repo name
                `lazy`: Boolean = false,
                empty_files: Boolean = false,
                join_on: Option[String] = None,
                optional: Option[Boolean] = None)

case class PipelineName(name: String)

case class Transform(image: String,
                     cmd: Seq[String] = Seq(),
                     stdin: Seq[String] = Seq(),
                     env: Map[String, String] = Map(),
                     secrets: Seq[Secret] = Seq(),
                     debug: Boolean = false,
                     accept_return_code: Seq[String] = Seq())

case class Secret(name: String, key: String, env_var: String)

case class ParallelismSpec(constant: Option[Int] = Some(1),
                           coefficient: Option[Float] = None)
