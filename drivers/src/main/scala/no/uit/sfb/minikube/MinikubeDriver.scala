package no.uit.sfb.minikube

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.SystemProcess

class MinikubeDriver(profile: String = "minikube") extends LazyLogging {
  def process(command: String, noThrow: Boolean = false): Unit = {
    val process = SystemProcess
      .from(Seq("minikube", "-p", profile, "ssh", command))
    if (noThrow)
      process.execQ()
    else
      process.exec()
  }

  def function(
      command: String,
      parse: Seq[String] => String = _.mkString("\n")): Option[String] = {
    val process = SystemProcess
      .from(Seq("minikube", "-p", profile, "ssh", command))
    process.execFQ(parse)
  }

  def ip(): Option[String] = {
    SystemProcess(s"minikube -p $profile ip")
      .execFQ(_.last)
  }

  def delete(): Unit = {
    SystemProcess(s"minikube -p $profile delete").exec()
  }

  /** Starts minikube instance with specified resources.
    * Throws an exception, if the profile already exists
    *
    * @param ram Upper limit RAM in Gb
    * @param cpus Upper limit of number of CPUs
    * @param disk Upper limit disk in Gb
    */
  def start(ram: Int, cpus: Int, disk: Int): Unit = {
    ip() match {
      case Some(_) =>
        throw new Exception(
          s"Minikube profile '$profile' already exists. Please delete it before attempting to start a new instance, or use another profile name.")
      case None =>
        SystemProcess(
          s"minikube -p $profile start --extra-config=apiserver.authorization-mode=RBAC --memory=${ram}000 --cpus $cpus --disk-size ${disk}g"
        ).exec()
    }
  }
}

object MinikubeDriver {
  def hostPath(path: String): String = path.replaceFirst("/home/", "/hosthome/")
}
