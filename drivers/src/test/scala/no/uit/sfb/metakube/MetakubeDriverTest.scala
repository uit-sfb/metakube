package no.uit.sfb.metakube

import java.time.{LocalDate, LocalDateTime, LocalTime}

import no.uit.sfb.metakube.MetakubeDriver.ENVIRS
import no.uit.sfb.metakube.models.analysis.{AnalysisDef, AnalysisSubmission}
import no.uit.sfb.metakube.models.{ActiveStage, StageState}
import no.uit.sfb.metakube.wm.impl.PachydermWorkflowManagerImpl
import no.uit.sfb.pachyderm.PachydermDriver
import org.scalatest.{FunSpec, Matchers}

import scala.concurrent.ExecutionContext

class MetakubeDriverTest extends FunSpec with Matchers {
  describe("MetakubeDriverTest should") {
    val config = s"""
                    |- _:
                    |    MK_RUN: true
                    |
                    |- myTool:
                    |    S3_URL: "http://localhost:9022"
                    |
                    |- assembly_myTool:
                    |    MK_RUN: false
      """.stripMargin
    val stages = Seq(
      ActiveStage("assembly_myTool",
                  LocalDateTime.of(LocalDate.now(), LocalTime.now()),
                  StageState.Running,
                  Map()),
      ActiveStage("taxonomy_kaiju",
                  LocalDateTime.of(LocalDate.now(), LocalTime.now()),
                  StageState.Running,
                  Map()),
      ActiveStage("taxonomy_myTool",
                  LocalDateTime.of(LocalDate.now(), LocalTime.now()),
                  StageState.Running,
                  Map())
    )
    val res: Map[String, ENVIRS] =
      new MetakubeDriverInstrumented().buildEnvsInstrumented(config, stages)
    it("append S3_URL in all myTool stages") {
      res("assembly_myTool").get("S3_URL") should be(
        Some("http://localhost:9022"))
      res("taxonomy_myTool").get("S3_URL") should be(
        Some("http://localhost:9022"))
    }
    it("set MK_RUN to true in taxonomy_myTool") {
      println(res("taxonomy_myTool").get("MK_RUN").map { _.getClass })
      res("taxonomy_myTool").get("MK_RUN") should be(Some("true"))
    }
    it("set MK_RUN to false in assembly_myTool") {
      res("assembly_myTool").get("MK_RUN") should be(Some("false"))
    }
    it("set MK_RUN to false for Kaiju") {
      res("taxonomy_kaiju").get("MK_RUN") should be(Some("true"))
    }
  }
}

class MetakubeDriverInstrumented
    extends MetakubeDriver(
      new PachydermWorkflowManagerImpl(
        new PachydermDriver("localhost")(ExecutionContext.global)))(
      ExecutionContext.global) {
  def buildEnvsInstrumented(anaConfig: String, stages: Seq[ActiveStage]) = {
    buildEnvs(
      AnalysisSubmission(AnalysisDef(config = anaConfig, datasets = Map())),
      stages)
  }
}
