package no.uit.sfb.pachyderm

import no.uit.sfb.pachyderm.models.status.PipelineStatus
import no.uit.sfb.scalautils.json.Json
import org.scalatest.{FunSpec, Matchers}

class MetakubeDriverTest extends FunSpec with Matchers {
  describe("PipelineStatus should") {
    val pipelineDescr =
      """
        |{
        |  "pipeline": {
        |    "name": "mgmt_postHook"
        |  },
        |  "version": "1",
        |  "transform": {
        |    "image": "ubuntu:bionic-20190912.1",
        |    "cmd": [
        |      "bash",
        |      "-c",
        |      "RETURN=\"{ \"outputs\": [ \"$DATASET_ID\" ] }\" ; curl -d $RETURN -H \"Content-Type: application/json\" -X POST $URL"
        |    ],
        |    "env": {
        |      "META": "/pfs/out/meta",
        |      "OUT": "/pfs/out/data",
        |      "MK_RUN": "false",
        |      "TMP": "/pfs/out/tmp"
        |    }
        |  },
        |  "created_at": "2019-10-17T06:57:50.499051806Z",
        |  "state": "PIPELINE_RUNNING",
        |  "output_branch": "master",
        |  "resource_requests": {
        |    "memory": "64M"
        |  },
        |  "input": {
        |    "pfs": {
        |      "name": "ENV_SCRIPT",
        |      "repo": "reads",
        |      "branch": "master",
        |      "glob": "/env_mgmt_postHook.sh"
        |    }
        |  },
        |  "description": "metadata:\n  \nparameters:\n",
        |  "cache_size": "64M",
        |  "salt": "ad29338a63ba47209a4b72d97ac8ed54",
        |  "max_queue_size": "1",
        |  "spec_commit": {
        |    "repo": {
        |      "name": "__spec__"
        |    },
        |    "id": "c8e81d9be3044e2d811a8e6baea2c687"
        |  },
        |  "datum_tries": "3",
        |  "pod_patch": "[]"
        |}
        |""".stripMargin
    var parsed: PipelineStatus = null
    it("be parsable") {
      parsed = Json.parse[PipelineStatus](pipelineDescr)
    }
    it("contain right default values") {
      parsed.parallelism_spec.constant should be(Some(1))
    }
  }
}
