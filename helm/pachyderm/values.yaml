rbac:
 enabled: true
 ## Set to false if deploying on openshift
 clusterRoles: true
 ## Set to false if you do not have node access (openshift)
 nodeAccess: true

## Set default credentials for object store
persistence:
  ## We do not support local mode in this chart. Use `pachctl deploy local` if needed
  selector: "" #One of minio, amazon, microsoft, google
  #existingSecret: pachyderm-storage-secret #If in use, keep pachyderm-storage-secret as name as there is not way to change the name on the workers!

  ## Minio credentials
  minio:
    accessKey: ""
    secretKey: ""
    bucketName: ""
    ## The endpoint should be something like: <mydomain>.<subdomain>:<port>
    endpoint: ""
    ## Set to 1 is endpoint is https
    secure: "0"
    ## For 'S3v2' signature, set the value to 1. For 'S3v4', set this value to 0
    signature: "0"
  ## Google Cloud credentials
  google:
    bucketName: ""
    credentials: ""
  ## Amazon Web Services credentials
  amazon:
    bucketName: ""
    ## The distribution parameter is often an empty string
    distribution: ""
    id: ""
    region: ""
    roleArn: ""
    secret: ""
    token: ""
  ## Microsoft Azure credentials
  microsoft:
    container: ""
    id: ""
    secret: ""

## Set default image settings, resource requests and number of replicas of pachd
pachd:
  replicaCount: 1
  timeout: 1m #Timeout to reach the object store
  ## Size of pachd's in-memory cache for PFS files. Size is specified in bytes, with allowed SI suffixes (M, K, G, Mi, Ki, Gi, etc).
  pfsCache: 256Mi
  ## Log level (debug, info, error)
  logLevel: info
  minimumServiceSet: true #critical services only
  #uploadAcl:
  #reverse:
  #partSize:
  #maxUploadParts:
  #disableSsl:
  #noVerifySsl:
  ## For available images please check: https://hub.docker.com/r/pachyderm/pachd/tags/
  images:
    pachd:
      repository: pachyderm/pachd
      tag: "1.9.9"
      pullPolicy: IfNotPresent
    worker:
      repository: pachyderm/worker
      tag: "1.9.9"
      pullPolicy: IfNotPresent
  resources:
    requests:
      cpu: 1
      memory: 2Gi
   # limits:
   #  cpu: 1
   #  memory: 2Gi
  service:
    type: ClusterIP
    grpc:
      #nodePort: 30650
      port: 650
    trace:
      #nodePort: 30651
      port: 651
    http:
      #nodePort: 30652
      port: 652
    peer:
      #nodePort: 30653
      port: 653
    saml:
      #nodePort: 30654
      port: 654
    gitapi:
      #nodePort: 30655
      port: 655
    prometheus:
      #nodePort: 30656
      port: 656
    s3Gateway:
      #nodePort: 30600
      port: 600
    worker:
      port: 80
      privileged: true #Set when port is < 1024


## Set default image settings and persistence settings of etcd
etcd:
  ## For available images please check: https://hub.docker.com/r/pachyderm/etcd/tags or: https://quay.io/repository/coreos/etcd?tag=latest&tab=tags
  image:
    repository: quay.io/coreos/etcd
    tag: v3.3.5
    pullPolicy: IfNotPresent
    ## Enable persistence using Persistent Volume Claims
    ## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
  service:
    type: ClusterIP
    port: 2379
    ## Only when typ is NodePort
    #nodePort: 32379
  persistence:
    ## If set to false, an emptydir is used. The data will be lost if the pod is scheduled to another node
    enabled: false
    ## If enabled is set, sets the storage class
    ## Attention, the conventions are different than k8s's ones:
    ## - if set to "" no storage class is applied:
    ##      - will bind to default dynamic provisioning storage class if one is defined and admission plugin turned on
    ##      - will bind exclusively to PV having storageClassName set to ""
    ## - if set to "-": disables dynamic provisioning. Will bind exclusively to PV having storageClassName set to "".
    ## - if set to any value xyz: Will bind exclusively to PV having storageClassName set to "xyz" (whether it is dynamically provisioned or not).
    storageClass: ""
    ## If enabled is set, set PVC size
    size: 1Gi
    ## Set default PVC access mode: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes
    accessMode: ReadWriteOnce
  resources:
    requests:
      cpu: 1
      memory: 2Gi
     # limits:
     #   cpu: 1
     #   memory: 2Gi
