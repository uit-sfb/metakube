{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "metakube.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Hack: we do not want the CHart.Name to appear in the full name
because Helm is broken and it doesn't work with sub-charts...
*/}}
{{- define "metakube.fullname" -}}
{{- default .Release.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "metakube.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "metakube.backend.externalHost" -}}
  {{- if .Values.backend.route.hostname }}
    {{- .Values.backend.route.hostname | toString }}
  {{- else }}
    {{- printf "backend.%s.%s" .Release.Namespace ( include "metakube.baseDomain" . ) }}
  {{- end }}
{{- end -}}

{{- define "metakube.backend.externalUrl" -}}
  {{- if .Values.backendCredentials.url }}
    {{- .Values.backendCredentials.url | toString }}
  {{- else if .Values.backend.enabled }}
    {{- if .Values.backend.route.enabled }}
      {{- if and .Values.backend.route.tls.enabled .Values.backend.route.openshiftRoute }}
        {{- printf "https://%s%s" ( include "metakube.backend.externalHost" . ) ( .Values.backend.route.path | default "/" | trimSuffix "/" ) }}
      {{- else }}
        {{- printf "http://%s%s" ( include "metakube.backend.externalHost" . ) ( .Values.backend.route.path | default "/" | trimSuffix "/" ) }}
      {{- end }}
    {{- else }}
      {{- printf "http://%s-backend:9000" (include "metakube.fullname" .) }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "metakube.backend.internalUrl" -}}
  {{- if .Values.backend.enabled }}
    {{- include "metakube.fullname" . | printf "http://%s-backend:9000" }}
  {{- else }}
    {{- .Values.backendCredentials.url | toString }}
  {{- end -}}
{{- end -}}

{{- define "metakube.minio.externalHost" -}}
  {{- if .Values.minio.route.hostname }}
    {{- .Values.minio.route.hostname | toString }}
  {{- else }}
    {{- printf "minio.%s.%s" .Release.Namespace ( include "metakube.baseDomain" . ) }}
  {{- end }}
{{- end -}}

{{- define "metakube.minio.externalUrl" -}}
  {{- if .Values.s3TmpStorageCredentials.url }}
    {{- .Values.s3TmpStorageCredentials.url | toString }}
  {{- else if .Values.s3PachydermCredentials.url }}
    {{- .Values.s3PachydermCredentials.url | toString }}
  {{- else if .Values.minio.enabled }}
    {{- if .Values.minio.route.enabled }}
      {{- if and .Values.minio.route.tls.enabled .Values.minio.route.openshiftRoute }}
        {{- printf "https://%s%s" ( include "metakube.minio.externalHost" . ) ( .Values.minio.route.path | default "/" | trimSuffix "/" ) }}
      {{- else }}
        {{- printf "http://%s%s" ( include "metakube.minio.externalHost" . ) ( .Values.minio.route.path | default "/" | trimSuffix "/" ) }}
      {{- end }}
    {{- else }}
      {{- printf "http://%s-minio:9000" (include "metakube.fullname" .) }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "metakube.baseDomain" -}}
  {{- if .Values.baseDomain }}
    {{- .Values.baseDomain }}
  {{- else }}
    {{- if .Values.clusterIp }}
      {{- printf "%s.nip.io" .Values.clusterIp }}
    {{- else }}
      {{- "nip.io" }}
    {{- end }}
  {{- end }}
{{- end -}}