#!/usr/bin/env bash

ORIGIN=$(pwd)

set -e

#note: Requires:
# - helm v3.1 or above
# - helm push plugin v0.8.1 or above

POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
    -h|--help)
      echo "Build chart"
      echo ""
      echo "Usage"
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
    *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

VERSION=$(cat "$ORIGIN/.version")
echo "$VERSION"
CHART_VERSION=$VERSION
#Need an exisiting context otherwise helm crashes
kubectl config set-context build
kubectl config use-context build
#Install Helm push plugin
helm plugin install https://github.com/chartmuseum/helm-push 2> /dev/null || true
#Add stable to repo list
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
#Add chartmuseum to your repo list
helm repo add chartmuseum https://chartmuseum.metapipe.uit.no
DIR="$THIS_PATH/target"
helm repo update
rm -rf "$DIR"
mkdir -p "$DIR"
cp -r "$THIS_PATH/pachyderm" "$DIR/pachyderm"
awk -i inplace "{gsub(\"{{VERSION}}\", \""$CHART_VERSION"\", \$0); print}" "$DIR/pachyderm/Chart.yaml"
helm lint "$DIR/pachyderm"
helm package --version $CHART_VERSION -d $DIR --app-version $VERSION --debug $DIR/pachyderm
helm push -u $CHARTMUSEUM_USER -p $CHARTMUSEUM_PASSWORD -f "$DIR/pachyderm-${CHART_VERSION}.tgz" chartmuseum
helm repo update
rm -rf "$DIR"
mkdir -p "$DIR"
cp -r "$THIS_PATH/metakube" "$DIR/metakube"
find "$DIR/metakube" -maxdepth 2 -type f -exec awk -i inplace "{gsub(\"{{VERSION}}\", \""$CHART_VERSION"\", \$0); print}" {} \;
cd $DIR
zip metakube.zip -r metakube
$ORIGIN/utils/publish.sh --file metakube.zip --artifact metakube/${VERSION}/metakube.zip
