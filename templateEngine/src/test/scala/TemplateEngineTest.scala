import java.nio.file.Paths

import com.fasterxml.jackson.annotation.JsonCreator
import no.uit.sfb.metakube.template.{TemplateOverlay, Template, TemplateEngine}
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.yaml.Yaml
import org.scalatest.{FunSpec, Matchers}

class TemplateEngineTest extends FunSpec with Matchers {
  private val templatePath =
    Paths.get(getClass.getResource("/no/uit/sfb/templates").getPath)
  private val fasitPath =
    Paths.get(getClass.getResource("/no/uit/sfb/pipelinedefs").getPath)
  private val target = Paths.get("./target/test")
  private def newTranslator() =
    new TemplateEngine(
      FileUtils.filesUnder(templatePath).toSet filter {
        _.getFileName.toString != "config.yaml"
      },
      Some(templatePath.resolve("config.yaml")),
      Paths.get("./target/test")
    )
  describe(classOf[TemplateEngine].getName) {
    it("should create all the needed pipeline definitions") {
      newTranslator().generatePachydermDefs()
      val (_, defs) = FileUtils.ls(target)
      val (_, fasits) = FileUtils.ls(fasitPath)
      fasits forall { fasit =>
        defs.exists(p => p.getFileName == fasit.getFileName)
      }
      defs foreach { p =>
        val oFasit = fasits.find(fasit => fasit.getFileName == p.getFileName)
        oFasit.isDefined should be(true)
        val fasit = oFasit.get
        FileUtils.readFile(p).replace("\\r", "") should be(
          FileUtils.readFile(fasit).replace("\\r", ""))
      }
    }
    it("should parse template properly") {
      val str =
        """
          |stage: functionalAnalysis_diamond
          |
          |image: registry.gitlab.com/uit-sfb/genomic-tools/mk-diamond:0.9.26
          |
          |parameters:
          |  DB_NAME:
          |    descr: "Diamond database. One of 'diamond-uniref50', 'diamond-marref-proteins'"
          |    type: String
          |  DB_VERSION:
          |    descr: "Diamond database version."
          |    type: String
          |  SENSITIVITY:
          |    descr: "Diamond sensitive selector. One of 'sensitive', 'more-sensitive' or anything else meaning no flag"
          |    type: String
          |
          |runtime: |
          |  if [[ -z "$DB_VERSION" ]]; then
          |    DB_VERSION=$(head -n 1 $db_versions/$DB_NAME)
          |  fi
          |  if [[ "$DB_NAME" == "diamond-uniref50" ]]; then
          |    DB_SUFFIX=uniprot/uniref
          |    OUT_FMT="6"
          |  else
          |    DB_SUFFIX=marref/proteins
          |    OUT_FMT="6 qseqid sseqid stitle pident length mismatch gapopen qstart qend sstart send evalue bitscore"
          |  fi
          |  DB_PATH="$DB_NAME/$DB_VERSION/db/diamond/$DB_SUFFIX/nr.dmnd"
          |  DATUM=$(basename "$input")
          |  mkdir -p "$MK_OUT/slices/$DATUM"
          |  case "$SENSITIVITY" in sensitive)
          |    SENSITIVE_FLAG="--sensitive";;
          |    more-sensitive) SENSITIVE_FLAG="--more-sensitive";;
          |    *) SENSITIVE_FLAG="";;
          |  esac
          |  $MK_APP blastp -d "/home/sfb/packages/$DB_PATH" -q "$input/cds.prot.fasta" -o "$MK_OUT/slices/$DATUM/diamond.out" -k 5 -p $MK_CPU_INT -c 4 -b 1 $SENSITIVE_FLAG --outfmt $OUT_FMT
          |
          |input:
          |  cross:
          |    - pfs:
          |        name: input
          |        repo: functionalAnalysis_postProcessingMga
          |        glob: "/data/slices/*"
          |    - pfs:
          |        name: db_versions
          |        repo: mgmt_downloadDatabases
          |        glob: "/data"
          |
          |volumes:
          |  packages:
          |    mountPath: /home/sfb/packages
          |    type: persistentVolumeClaim
          |    params:
          |      claimName: metakube-packages
          |
          |""".stripMargin
      Yaml.parse[Template](str)
    }
    it("should parse template override properly") {
      val str = """- _:
                  |    overrideDefaultInputRepo: dummy_input
                  |    resources:
                  |      requests:
                  |        cpu: 500m
                  |        memory: 500Mi
                  |      limits:
                  |        memory: 700Mi
                  |    sidecarResources:
                  |      requests:
                  |        cpu: 500m
                  |    forcePullImage: true
                  |- stage3:
                  |    params:
                  |      FAIL: true
                  |    after: stage2
                  |    parallelism: 2
                  |""".stripMargin
      val path = Paths.get("target/f.yaml")
      FileUtils.writeToFile(path, str)
      TemplateOverlay.fromPath(path)
    }
  }
}
