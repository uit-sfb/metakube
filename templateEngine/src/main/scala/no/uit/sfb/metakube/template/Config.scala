package no.uit.sfb.metakube.template

import java.nio.file.{Path, Paths}

import no.uit.sfb.minikube.MinikubeDriver

case class Config(
    paths: Seq[Path] = Seq(),
    target: Path = Paths.get("."),
    default: Option[Path] = None
)
