package no.uit.sfb.metakube.template

import java.io.File
import java.nio.file.Path

import no.uit.sfb.info.mk_template_engine.BuildInfo
import no.uit.sfb.scalautils.common.FileUtils
import scopt.OParser

object Main extends App {
  try {
    val builder = OParser.builder[Config]

    val name = BuildInfo.name
    val ver = BuildInfo.version
    val gitCommitId = BuildInfo.formattedShaVersion

    val parser = {
      import builder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"git: ${gitCommitId.getOrElse("unknown")}"),
        help('h', "help")
          .text("prints this usage text"),
        version('v', "version")
          .text("prints the version"),
        opt[Seq[File]]('p', "path")
          .required()
          .action((x, c) => c.copy(paths = x.map { _.toPath }))
          .text(
            "Comma separated list of path of stage template or directory containing templates. ~ cannot be used"),
        opt[File]('t', "target")
          .action((x, c) => c.copy(target = x.toPath))
          .text(
            "Path to where the generated pipeline definition should be stored. ~ cannot be used"),
        opt[File]("default")
          .action((x, c) => c.copy(default = Some(x.toPath)))
          .text("Path to default config.")
      )
    }

    OParser.parse(parser, args, Config()) match {
      case Some(config) =>
        val files: Set[Path] = config.paths
          .foldLeft(Seq[Path]()) {
            case (acc, path) =>
              acc ++ FileUtils
                .filesUnder(path)
                .filter(
                  p =>
                    p.getFileName.toString
                      .endsWith(".tpl.yml") || p.getFileName.toString.endsWith(
                      ".tpl.yaml"))
          }
          .toSet
        val templateGen = new TemplateEngine(
          files,
          config.default,
          config.target
        )

        templateGen.generatePachydermDefs()
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
