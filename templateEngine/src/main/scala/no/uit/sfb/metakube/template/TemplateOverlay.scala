package no.uit.sfb.metakube.template

import java.nio.file.Path

import no.uit.sfb.metakube.template.TemplateOverlay.RawConfigOverride
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.yaml.Yaml

import scala.util.control.NonFatal

case class TemplateOverlay(layers: Seq[TemplateOverlayLayer] = Seq()) {
  def merge(runtimeLayers: TemplateOverlay): TemplateOverlay = {
    TemplateOverlay(layers ++ runtimeLayers.layers)
  }
  def toFile(path: Path): Unit = {
    val tmp = layers.map { tl =>
      Map(tl.selectors.mkString(",") -> tl.layer)
    }
    Yaml.toFileAs[Seq[Map[String, TemplateSettingsOverride]]](path, tmp)
  }
}

object TemplateOverlay {
  protected def fromRawRuntimeOverride(
      rrco: RawRuntimeConfigOverride): TemplateOverlay = {
    val rco = rrco.map { m =>
      m.map {
        case (k, v) =>
          k -> TemplateSettingsOverride(assign = Some(v))
      }
    }
    fromRawOverride(rco)
  }

  protected def fromRawOverride(rco: RawConfigOverride): TemplateOverlay = {
    val tl: Seq[TemplateOverlayLayer] = rco.map { m =>
      val (selectorStr, tpl) = m.toSeq.head
      TemplateOverlayLayer(selectorStr.split(',').toSet, tpl)
    }
    TemplateOverlay(tl)
  }

  def fromPathRuntime(path: Path): TemplateOverlay = {
    try {
      //Yaml cannot parse Seq[Map[String, TemplateOverlayLayer]] probably due to type erasure
      //Instead it returns TemplateOverride as a Map.
      //In order to get the TemplateOverride, we serialize the Map or parse it again
      val tmp = Yaml.fromFileAs[Seq[Map[String, Map[String, Any]]]](path)
      val rrco: RawRuntimeConfigOverride = tmp.map {
        _.map {
          case (k, v) =>
            k -> Yaml.parse[Map[String, String]](Yaml.serialize(v))
        }
      }
      TemplateOverlay.fromRawRuntimeOverride(rrco)
    } catch {
      case NonFatal(e) =>
        println(s"Could not parse $path")
        println("***")
        println(FileUtils.readFile(path))
        println("***")
        throw e
    }
  }

  def fromPath(path: Path): TemplateOverlay = {
    try {
      //Yaml cannot parse Seq[Map[String, TemplateOverlayLayer]] probably due to type erasure
      //Instead it returns TemplateOverride as a Map.
      //In order to get the TemplateOverride, we serialize the Map or parse it again
      val tmp = Yaml.fromFileAs[Seq[Map[String, Map[String, Any]]]](path)
      val rco = tmp.map {
        _.map {
          case (k, v) =>
            k -> Yaml.parse[TemplateSettingsOverride](Yaml.serialize(v))
        }
      }
      TemplateOverlay.fromRawOverride(rco)
    } catch {
      case NonFatal(e) =>
        println(s"Could not parse $path")
        println("***")
        println(FileUtils.readFile(path))
        println("***")
        throw e
    }
  }

  type RawConfigOverride = Seq[Map[String, TemplateSettingsOverride]]
  type RawRuntimeConfigOverride = Seq[Map[String, Map[String, String]]]
}
