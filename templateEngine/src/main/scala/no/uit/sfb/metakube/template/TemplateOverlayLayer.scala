package no.uit.sfb.metakube.template

case class TemplateOverlayLayer(selectors: Set[String] = Set(),
                                layer: TemplateSettingsOverride) {
  def overlay(tpl: Template): Template = {
    if (layer == null)
      tpl
    else {
      //If at least one selector is contain in the stage name, we superpose the layer
      if (selectors.contains("_") || selectors.exists(
            sel => tpl.stage.contains(sel)))
        tpl.copy(
          settings = {
            tpl.settings.copy(
              assign =
                tpl.settings.assign ++ layer.assign.getOrElse(Map()),
              inputRepo = layer.inputRepo.getOrElse(tpl.settings.inputRepo),
              after =
                Some(layer.after.getOrElse(tpl.settings.after.getOrElse(""))),
              resources = tpl.settings.resources.merge(layer.resources.getOrElse(Resources())),
              sidecarResources = tpl.settings.sidecarResources.merge(layer.sidecarResources.getOrElse(Resources())),
              nodeSelector = tpl.settings.nodeSelector ++ layer.nodeSelector
                .getOrElse(Map()),
              parallelism =
                layer.parallelism.getOrElse(tpl.settings.parallelism),
              jobTimeout = layer.jobTimeout.getOrElse(tpl.settings.jobTimeout),
              standby = layer.standby.getOrElse(tpl.settings.standby),
              forcePullImage =
                layer.forcePullImage.getOrElse(tpl.settings.forcePullImage),
              debug = layer.debug.getOrElse(tpl.settings.debug),
              alwaysReprocess =
                layer.alwaysReprocess.getOrElse(tpl.settings.alwaysReprocess),
              datumTries = layer.datumTries.getOrElse(tpl.settings.datumTries),
              allowRetCode = tpl.settings.allowRetCode ++ layer.allowRetCode
                .getOrElse(Seq()),
              deploy = layer.deploy.getOrElse(tpl.settings.deploy)
            )
          }
        )
      else //If not, we just return the template
        tpl
    }
  }
}
