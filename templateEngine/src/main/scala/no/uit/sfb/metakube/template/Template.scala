package no.uit.sfb.metakube.template

import java.nio.file.Path

import com.fasterxml.jackson.annotation.JsonCreator
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.pachyderm.models.spec._
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.yaml.Yaml

import scala.collection.generic.Sorted
import scala.util.control.NonFatal

/**
 *
 * @param stage       Name of the stage. Should be unique in a workflow. It is useful to define a pipeline hierarchy using underscores (ex: subPipeline_stageName)
 *                    Should contain only alphanumerical characters and '_'. Compulsory
 * @param description Stage description that will figure in the pipeline's description. Default: empty
 * @param image       Docker image. Compulsory
 * @param parameters  User-defined environment variables defined at runtime and usable in user code. Default: none
 *                    Are added to metakube envirs which may be used, but not modified (except *):
 *                    MK_RUN (*): If set to false, the stage will be skipped (default: true)
 *                    MK_APP: Path to the main user program
 *                    MK_TMP: Directory where the user code starts executing. Any file present in this directory will not be saved in pachyderm file system and will therefore not be accessible by child stages
 *                    MK_OUT: Directory sync'ed with pachyderm file system. Any file present in this directory will be saved and be available by any child stage.
 *                    MK_CPU_INT: Whole number part of decimal CPU request setting (ex: 2700m -> MK_CPU_INT=2, 400m -> MK_CPU_INT=1)
 *                    MK_MEM_BYTES: Byte representation of MK_MEM
 *                    MK_MEM_LIMIT_BYTES: Byte representation of MK_MEM_LIMIT
 *                    MK_META: private
 *                    MK_CONTEXT: private
 *                    MK_REPROCESS: private
 *                    USER_ID: private
 *                    AID: private
 * @param cmdOverride Optional cmd override. Default: none. Rarely used.
 * @param runtime     User code. So far only bash syntax is accepted. Default: ""
 * @param optional    If set to true, MK_RUN can be assigned to disable the stage. Default: false
 * @param input       Input definition. Default: none
 * @param secrets     Secrets to mount to the container as envir. Default: none
 * @param volumes     Volumes to mount to the container. Default: none
 */
case class Template(
                     stage: String,
                     description: String,
                     image: String,
                     parameters: Map[String, Parameter] = Map(),
                     optional: Boolean = false,
                     cmdOverride: Seq[String] = Seq(),
                     runtime: String = "",
                     input: PipelineInput = PipelineInput(),
                     //configMaps: Map[String, ConfigMapPath] = Map(),
                     secrets: Map[String, SecretPath] = Map(),
                     volumes: Map[String, VolumeDef] = Map(),
                     settings: TemplateSettings
                   ) extends LazyLogging {
  //Used to not attribute `null` to undefined values that are (non-optional)
  @JsonCreator
  def this() =
    this(
      "",
      "",
      "",
      Map(),
      false,
      Seq(),
      "",
      PipelineInput(None, None, None, None),
      //Map(),
      Map(),
      Map(),
      TemplateSettings()
    )

  //Do not add validation here! It always fails because of the @JsonCreator

  def toPipelineSpec(
                      others: Seq[String]
                    ): PipelineSpec = {
    assert(image.nonEmpty,
      s"Please provide an image with 'image' (stage: $stage)")

    val extraEnvirs: Map[String, String] = Map(
      "MK_RUN" -> "true"
    ) ++
      settings.extraEnvirs

    val extraParameters = {
      Map(
        "MK_RUN" -> Parameter(
          "Enabled",
          "Whether this optional stage should be enabled or not.",
          "Boolean",
          !optional)
      )
    }

    val defaultInputRepo = settings.inputRepo

    def substitutePI(pi: PipelineInput): PipelineInput = { //case class in the PipelineSpec
      if (pi.pfs.nonEmpty) {
        val pfs = pi.pfs.get
        val overrideRepo =
          if (pfs.repo == null || pfs.repo.isEmpty) defaultInputRepo
          else pfs.repo
        val subst = PipelineInput(
          Some(
            pfs.copy(
              repo = overrideRepo,
              name =
                if (pfs.name == null || pfs.name.isEmpty) overrideRepo
                else pfs.name,
              glob = pfs.glob,
              optional = None //Optional field is not in pachyderm spec so we remove it here
            )
          ))
        //If optional and not exists
        if (pfs.optional.getOrElse(false) && !others.contains(
          subst.pfs.get.repo)) {
          PipelineInput()
        } else
          subst
      } else if (pi.cross.nonEmpty) {
        val cross = pi.cross.get map substitutePI
        PipelineInput(cross = Some(cross))
      } else if (pi.union.nonEmpty) {
        val union = pi.union.get map substitutePI
        PipelineInput(union = Some(union))
      } else if (pi.join.nonEmpty) {
        val join = pi.join.get map substitutePI
        PipelineInput(join = Some(join))
      } else {
        PipelineInput()
      }
    }

    def appendAutomaticConnections(pi: PipelineInput): PipelineInput = {
      val after = settings.after.getOrElse("")
      if (after.isEmpty)
        pi
      else {
        val selectors = after.split(',').toSeq.map {
          _.trim
        }
        val pfss: Seq[Pfs] = others.flatMap {
          case stage if selectors.exists { connect =>
            stage.contains(connect) || connect == "_"
          } =>
            Seq(
              Pfs("/meta/touch",
                stage,
                s"MK_AFTER_${stage.toUpperCase}",
                false))
          case _ => Seq()
        }.toSeq
        val otherInputs = PipelineInput(cross = Some(pfss map { pfs =>
          PipelineInput(pfs = Some(pfs))
        }))
        if (pi != PipelineInput()) {
          PipelineInput(
            cross = Some(
              Seq(
                pi,
                otherInputs
              )
            )
          )
        } else
          otherInputs
      }
    }

    def appendScript(pi: PipelineInput): PipelineInput = {
      val scriptName = s"env_$stage.sh"
      val scriptInput = PipelineInput(
        Some(Pfs(s"/$scriptName", defaultInputRepo, "ENV_SCRIPT", false)))

      if (pi != PipelineInput()) {
        PipelineInput(
          cross = Some(
            Seq(
              pi,
              scriptInput
            )
          )
        )
      } else
        scriptInput
    }

    def appendForce(pi: PipelineInput): PipelineInput = {
      if (settings.alwaysReprocess) {
        val fileName = "context.sh"
        val scriptInput = PipelineInput(
          Some(Pfs(s"/$fileName", defaultInputRepo, "MK_REPROCESS", false)))
        if (pi != PipelineInput()) {
          PipelineInput(
            cross = Some(
              Seq(
                pi,
                scriptInput
              )
            )
          )
        } else
          scriptInput
      } else
        pi
    }

    //As long as pachyderm doesn't support optional inputs,
    //this trick will be needed (the use case is a stage crossing inputs of which some will not execute)
    //Attempting to cross a ghost only at the crossing stage would only be messy and complicated.
    def appendGhost(pi: PipelineInput): PipelineInput = {
      val fileName = "context.sh"
      val scriptInput = PipelineInput(
        Some(Pfs(s"/$fileName", defaultInputRepo, "MK_CONTEXT", false)))
      if (pi != PipelineInput()) {
        PipelineInput(
          union = Some(
            Seq(
              pi,
              scriptInput
            )
          )
        )
      } else
        scriptInput
    }

    val extraVolumes = Seq(
      Some("tmp" -> VolumeDef("/home/sfb/tmp", "emptyDir", Map())),
    ).flatten.toMap

    PipelineSpec(
      PipelineName(stage),
      description = description,
      transform = Transform(
        image = s"""$image""",
        cmd =
          if (cmdOverride.isEmpty)
            Seq("/opt/docker/bin/start.sh")
          else
            cmdOverride,
        stdin = runtime.split('\n'),
        env = settings.assign.filter {
          case (k, _) =>
            TemplateEngine.defaultAssign
              .contains(k) || ((extraParameters ++ parameters).get(k) match {
              case Some(param) =>
                if (param.internal)
                  true
                else
                  throw new Exception(
                    s"In stage '$stage': cannot assign parameter '$k' as it is not defined as internal in 'parameters'.")
              case None =>
                logger.warn(s"In stage '$stage': ignoring assigment to parameter '$k' as it has no entry in 'parameters'")
                false
            })
        }
          ++ extraEnvirs,
        secrets = (secrets map {
          case (env, SecretPath(name, key)) =>
            Secret(name, key, env)
        }).toSeq,
        debug = settings.debug,
        accept_return_code = settings.allowRetCode map {
          _.toString
        }
      ),
      input = PipelineInput.compact(
        appendGhost(
          appendForce(
            appendAutomaticConnections(
              appendScript(
                substitutePI(input)
              )
            )
          )
        )
      ),
      enable_stats = true, //Cannot disable stats after they have been enabled, so better to always have them on
      standby = settings.standby,
      parallelism_spec = ParallelismSpec(constant = Some(settings.parallelism)),
      pod_patch = "[" + (((volumes ++ extraVolumes) flatMap {
        case (name, VolumeDef(mountPath, tpe, params)) =>
          Seq(
            s"""{\"op\":\"add\",\"path\":\"/containers/0/volumeMounts/-\",\"value\":{\"mountPath\":\"$mountPath\",\"name\":\"$name\"}}""",
            s"""{\"op\":\"add\",\"path\":\"/volumes/-\",\"value\":{\"$tpe\":{${
              params
                .map {
                  case (k, v) => s"""\"$k\":\"$v\""""
                }
                .mkString(",")
            }},\"name\":\"$name\"}}"""
          )
      }) ++
        Seq(
          if (settings.forcePullImage)
            Seq(
              s"""{\"op\":\"add\",\"path\":\"/containers/0/imagePullPolicy\",\"value\":\"Always\"}""".stripMargin)
          else Seq(),
          Seq(settings.resourceRequests) flatMap {
            case ResourcesValues(cpu, memory, _) =>
              val seq = Seq() :+
                (if (cpu.nonEmpty)
                  Some(
                    s"""{\"op\":\"add\",\"path\":\"/containers/0/resources/requests/cpu\",\"value\":\"${cpu}\"}""".stripMargin)
                else
                  None) :+
                (if (memory.nonEmpty)
                  Some(
                    s"""{\"op\":\"add\",\"path\":\"/containers/0/resources/requests/memory\",\"value\":\"${memory}\"}""".stripMargin)
                else None)
              seq.flatten
          },
          Seq(settings.resourceLimits) flatMap {
            case ResourcesValues(cpu, memory, _) =>
              val seq = (Seq() :+
                (if (cpu.nonEmpty)
                  Some(s"""\"cpu\":\"${cpu}\"""")
                else None) :+
                (if (memory.nonEmpty)
                  Some(s"""\"memory\":\"${memory}\"""")
                else None)).flatten
              if (seq.nonEmpty)
                Seq(
                  s"""{\"op\":\"add\",\"path\":\"/containers/0/resources/limits\",\"value\":{${
                    seq
                      .mkString(",")
                  }}}""".stripMargin)
              else
                Seq()
          },
          Seq(settings.sidecarResourceRequests) flatMap {
            case ResourcesValues(cpu, memory, _) =>
              val seq = Seq() :+
                (if (cpu.nonEmpty)
                  Some(
                    s"""{\"op\":\"add\",\"path\":\"/containers/1/resources/requests/cpu\",\"value\":\"${cpu}\"}""".stripMargin)
                else
                  None) :+
                (if (memory.nonEmpty)
                  Some(
                    s"""{\"op\":\"add\",\"path\":\"/containers/1/resources/requests/memory\",\"value\":\"${memory}\"}""".stripMargin)
                else None)
              seq.flatten
          },
          Seq(settings.sidecarResourceLimits) flatMap {
            case ResourcesValues(cpu, memory, _) =>
              val seq = (Seq() :+
                (if (cpu.nonEmpty)
                  Some(s"""\"cpu\":\"${cpu}\"""")
                else None) :+
                (if (memory.nonEmpty)
                  Some(s"""\"memory\":\"${memory}\"""")
                else None)).flatten
              if (seq.nonEmpty)
                Seq(
                  s"""{\"op\":\"add\",\"path\":\"/containers/1/resources/limits\",\"value\":{${
                    seq
                      .mkString(",")
                  }}}""".stripMargin)
              else
                Seq()
          }
        ).flatten).mkString(",") +
        "]",
      job_timeout =
        if (settings.jobTimeout == 0)
          None
        else
          Some(s"{settings.jobTimeout}s"),
      datum_tries = Some(settings.datumTries),
      scheduling_spec = SchedulingSpec(settings.nodeSelector)
    )
  }
}

object Template {
  def fromPath(path: Path): Template = {
    val template = try {
      Yaml.fromFileAs[Template](path)
    } catch {
      case NonFatal(e) =>
        println(s"Could not parse $path")
        println("---")
        println(FileUtils.readFile(path))
        println("---")
        throw e
    }
    assert(template.stage.nonEmpty, "stage name cannot be empty")
    val stageNameReg =
      """^\w+$""".r
    template.stage match {
      case stageNameReg() => //Nothing to do
        template
      case _ =>
        throw new IllegalArgumentException(
          s"Stage name should only contain alphanumerical character and _, but '${template.stage}' found.")
    }
  }
}

case class ConfigMapPath(name: String, key: String)

case class SecretPath(name: String, key: String)

//Only runtime parameters can be override at runtime
case class Parameter(label: String,
                     description: String,
                     `type`: String,
                     internal: Boolean = false) {
  @JsonCreator
  def this() = this("", "", "", false)
}

case class ParallelismSpecString(constant: Option[String] = Some("1"),
                                 coefficient: Option[String] = None) {
  @JsonCreator
  def this() = this(Some("1"))
}

case class Resources(limits: ResourcesValues = ResourcesValues(),
                     requests: ResourcesValues = ResourcesValues()) {
  @JsonCreator
  def this() = this(ResourcesValues(), ResourcesValues())

  def merge(layer: Resources): Resources = {
    Resources(limits = limits.merge(layer.limits),
      requests = requests.merge(layer.requests)
    )
  }
}

case class VolumeDef(mountPath: String,
                     `type`: String = "",
                     params: Map[String, String] = Map()) {
  @JsonCreator
  def this() = this("", "", Map())
}
