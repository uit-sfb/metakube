package no.uit.sfb.metakube.template

case class TemplateSettingsOverride(
    assign: Option[Map[String, String]] = None,
    inputRepo: Option[String] = None,
    after: Option[String] = None,
    resources: Option[Resources] = None,
    sidecarResources: Option[Resources] = None,
    nodeSelector: Option[Map[String, String]] = None,
    parallelism: Option[Int] = None,
    jobTimeout: Option[Int] = None,
    standby: Option[Boolean] = None,
    forcePullImage: Option[Boolean] = None,
    debug: Option[Boolean] = None,
    alwaysReprocess: Option[Boolean] = None,
    datumTries: Option[Int] = None,
    allowRetCode: Option[Seq[Int]] = None,
    deploy: Option[Boolean] = None
)
