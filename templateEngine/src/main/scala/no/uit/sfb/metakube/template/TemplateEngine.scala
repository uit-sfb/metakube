package no.uit.sfb.metakube.template

import java.nio.file.Path

import no.uit.sfb.metakube.MetakubeDriver.ENVIRS
import no.uit.sfb.pachyderm.models.spec.{ResourcesValues, _}
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json
import no.uit.sfb.scalautils.yaml.Yaml

class TemplateEngine(templatePaths: Set[Path],
                     oConfig: Option[Path],
                     outputPath: Path) {
  protected lazy val rawTemplates
  : Set[Template] = templatePaths map Template.fromPath
  protected lazy val config: TemplateOverlay = {
    val tmp = oConfig match {
      case Some(cPath) => TemplateOverlay.fromPath(cPath)
      case _ => TemplateOverlay()
    }
    println(s"Config override:\n$tmp")
    tmp
  }

  protected lazy val overriddenTemplates: Set[Template] = rawTemplates flatMap {
    rawTemplate =>
      val defaultResources =
        TemplateSettings(
          resources = Resources(limits = ResourcesValues(
            memory = "1Gi"
          ),
            requests = ResourcesValues(cpu = "1",
              memory = "500Mi")),
          sidecarResources = Resources(limits = ResourcesValues(
            memory = "2Gi"
          ),
            requests = ResourcesValues(cpu = "500m",
              memory = "500Mi")),
        )
      val overriddenTemplate =
        config.layers.foldLeft(
          rawTemplate.copy(
            settings =
              rawTemplate.settings.copy(assign = rawTemplate.settings.assign ++ TemplateEngine.defaultAssign,
                resources = defaultResources.resources.merge(rawTemplate.settings.resources),
                sidecarResources = defaultResources.sidecarResources.merge(rawTemplate.settings.sidecarResources)
              ))
        ) {
          case (acc, layer) =>
            layer.overlay(acc)
        }
      if (overriddenTemplate.settings.deploy)
        Some(overriddenTemplate)
      else
        None
  }

  def generatePachydermDefs(): Unit = {
    if (FileUtils.exists(outputPath)) //We do not use deleteDirIfExists here because when using Docker volume, that would fail
    FileUtils.deleteDir(outputPath, true)
    overriddenTemplates foreach { template =>
      val pdef =
        template.toPipelineSpec(
          (overriddenTemplates.filter {
            _ != template
          }.map {
            _.stage
          }).toSeq.sorted, //For testing purpose, we need them sorted
          )
      val pdefPath =
        outputPath.resolve(s"${template.stage}.json")
      FileUtils.createParentDirs(pdefPath)
      Json.toFileAs[PipelineSpec](pdefPath, pdef)
    }
  }
}

object TemplateEngine {
  val defaultAssign = {
    val meta = "/pfs/out/meta"
    Map(
      "MK_OUT" -> "/pfs/out/data", // pachyderm pipeline output folder
      "MK_TMP" -> "/home/sfb/tmp",
      "MK_META" -> meta,
      "MK_APP" -> "/opt/docker/bin/app.sh"
    )
  }
}
