package no.uit.sfb.metakube.template

import com.fasterxml.jackson.annotation.JsonCreator
import no.uit.sfb.pachyderm.models.spec.ResourcesValues

/**
  * @param assign Key-value definition of parameters. Used only in   config override.
  * @param inputRepo Default input repo. If no repo is defined in an input, the default one is used. Rarely set. Default: "input"
  * @param after Selector to define a virtual dependency. Default: none
  * @param resources Resource requests and limits for worker container. Default: none
  * @param sidecarResources Resource requests and limits for sidecar container. Default: none
  * @param nodeSelector Optional node selector. Rarely used. Default: none
  * @param parallelism Stage parallelism. Default: 1
  * @param jobTimeout Job timeout in seconds. If 0, no timeout. Default: 0
  * @param standby Standby mode. Default: true
  * @param forcePullImage Force pull user image. Default: false
  * @param debug Debug mode. Default: false
  * @param alwaysReprocess Force stage processing even though the same inputs have already been processed before. Default: false
  * @param datumTries Allowed number of times a datum may be retried in the event of user code exception. Default: 1
  * @param allowRetCode Allowed non-zero return code. Default: none
  * @param deploy Deploy pipeline or ignore template altogether. Default: true
  */
//We use null instead of options deliberately here.
//Otherwise it is not possible to define defaults properly
case class TemplateSettings(
    assign: Map[String, String] = Map(),
    inputRepo: String = "input",
    after: Option[String] = None,
    resources: Resources = Resources(),
    sidecarResources: Resources = Resources(),
    nodeSelector: Map[String, String] = Map(),
    parallelism: Int = 1,
    jobTimeout: Int = 0,
    standby: Boolean = true,
    forcePullImage: Boolean = false,
    debug: Boolean = false,
    alwaysReprocess: Boolean = false,
    datumTries: Int = 1,
    allowRetCode: Seq[Int] = Seq(),
    deploy: Boolean = true
) {
  @JsonCreator
  def this() = this(
    Map(),
    "input",
    None,
    Resources(),
    Resources(),
    Map(),
    1,
    0,
    true,
    false,
    false,
    false,
    1,
    Seq(),
    true
  )

  protected def wholeCpu(str: String): Int = {
    val reg = """^[ \t]*(\d+)[ \t]*(m?)[ \t]*$""".r
    str match {
      case reg(value, "") => value.toInt
      case reg(value, "m") =>
        val tmp = value.toInt / 1000
        if (tmp < 1)
          1
        else tmp
      case _ =>
        throw new NumberFormatException(
          s"Could not parse cpu value '$str'. It should be an integer optionally followed by the unit 'm'")
    }
  }

  protected def toBytes(str: String): Long = {
    val reg = """^[ \t]*(\d+)[ \t]*((?:[GMK]i?)?)[ \t]*$""".r
    str match {
      case reg(value, "")   => value.toLong
      case reg(value, "K")  => value.toLong * 1000
      case reg(value, "Ki") => value.toLong * 1024
      case reg(value, "M")  => value.toLong * 1000 * 1000
      case reg(value, "Mi") => value.toLong * 1024 * 1024
      case reg(value, "G")  => value.toLong * 1000 * 1000 * 1000
      case reg(value, "Gi") => value.toLong * 1024 * 1024 * 1024
      case _ =>
        throw new NumberFormatException(
          s"Could not parse resource '$str'. It should be an integer optionally followed by one of the unit K|Ki|M|Mi|G|Gi")
    }
  }

  val sidecarResourceRequests = sidecarResources.requests

  val sidecarResourceLimits: ResourcesValues = {
    val rv = sidecarResources.limits
    sidecarResourceRequests.memory match {
      case "" => rv //Since k8s defaults are small we assume that it will not happen that the limit set is even smaller
      case value if rv.memory.nonEmpty =>
        if (toBytes(value) > toBytes(rv.memory))
          rv.copy(memory = value)
        else
          rv
    }
  }
  val resourceRequests = resources.requests
  val resourceLimits: ResourcesValues = {
    val rv = resources.limits
    resourceRequests.memory match {
      case "" => rv //Since k8s defaults are small we assume that it will not happen that the limit set is even smaller
      case value if rv.memory.nonEmpty =>
        if (toBytes(value) > toBytes(rv.memory))
          rv.copy(memory = value)
        else
          rv
    }
  }
  val extraEnvirs: Map[String, String] = Map(
    "MK_CPU_INT" -> (resourceRequests match {
      case req if req.cpu.nonEmpty =>
        wholeCpu(req.cpu).toString
      case _ => "1"
    }),
    "MK_MEM_BYTES" -> (resourceRequests match {
      case req if req.memory.nonEmpty =>
        toBytes(req.memory).toString
      case _ => toBytes("150Mi").toString
    }),
    "MK_MEM_LIMIT_BYTES" -> (resourceLimits match {
      case lim if lim.memory.nonEmpty =>
        toBytes(lim.memory).toString
      case _ => ""
    })
  )
}
