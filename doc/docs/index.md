# Welcome to Metakube

Metakube offers *Workflow as a service* (WaaS) over [Kubernetes](https://kubernetes.io/).
Its main objective is to speed up the lifecycle iterations of complex workflows,
from design and prototyping to production in the cloud, local computing facility or simply your laptop.

Using Kubernetes as container orchestrator, leverage the power of any computer cluster
to execute your workflow without the overhead of configuring and maintaining a complex IT infrastructure.
In fact, deploying a Metakube workflow is as simple as running a program on your laptop!

## A few reasons to choose Metakube

- Develop reproducible workflows meeting the [FAIR Principles](https://www.nature.com/articles/sdata201618)
- Run easily Metakube workflows on any cloud supporting Kubernetes and submit analyses via
a simple and secure web interface
- Share your workflows with the community either by letting them execute their analysis on your 
Metakube service, or by letting them execute the workflows in all simplicity directly on their laptop, no matter how
complex they are.
