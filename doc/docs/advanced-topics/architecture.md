# Architecture

## Kubernetes

The primary purpose of Metakube is to make the deployment complex workflows on any
computer cluster as simple as executing a script on a laptop.
To makes this possible, Metapipe relies heavily on Kubernetes (k8s).
This container orchestrator was chosen because:
  - k8s abstracts a away the complexity of running a application on a cluster of computers in a scalable manner
  - k8s is offered as Paas by most of the cloud providers
  - k8s has become a de facto industry standard
  - k8s has a very strong community and is well documented
  - k8s is 100% container based which is a good proponent for reproducibility

## Services

Metakube utilizes the following microservices:
  - [Pachyderm](https://github.com/pachyderm/pachyderm): service responsible for executing the
workflow by starting and shutting down containers and passing data between them
  - [etcd](https://github.com/etcd-io/etcd): key-value store used by Pachyderm to store metadata
  - [minio](https://github.com/minio/minio): object store used by Pachyderm to store data
  - [backend](backend): workflow submission service (user interface)
  - [MongoDB](https://github.com/mongodb/mongo): database to store analysis metadata

![architecture](../img/architecture.png)  
*METAkube architecture*