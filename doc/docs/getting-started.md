# Getting started

The best way to enter the world of Metakube is perhaps to deploy a workflow.
How workflows are implemented will be described later in this guide.

For now, lets begin by deploying [metakube-test](../../workflowExample/workflow) on your
own machine.

## Requirements

Disclaimer: Although this guide should work on Mac OS X, or any Linux flavors, it has only been tested with
Ubuntu. Please, let us know if you have troubles following this guide with your operating system.

The following packages are needed:

- [Docker](https://docs.docker.com/install/)
- [Minishift](https://docs.okd.io/3.11/minishift/getting-started/installing.html)
- [KVM driver](https://docs.okd.io/3.11/minishift/getting-started/setting-up-virtualization-environment.html)
- [oc & kubectl clients](https://www.okd.io/download.html)

## Create a Kubernetes cluster

Metakube runs on top of a Kubernetes cluster.
The first step consists in deploying Kubernetes on your machine (in fact on a VM running on your machine).
Fortunately this is straightforward:
```
minishift start
```
It takes a couple of minutes. Once the success message is printed, Kubernetes is up and running.

Create a project called `metakube` by running `oc new-project metakube`.

We are now ready to deploy our workflow.

## Deploying a workflow as a service

Let us download the deployment script first:
```
wget https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/metakube/1.0.0-SNAPSHOT/pipeline-example/metakube-test-1.0.0.sh && chmod +x metakube-test-1.0.0.sh
```
Then simply execute the script: `./metakube-test-1.0.0.sh`
Once again, it should take a couple of minutes before printing a success message.

Your workflow is now deployed and available as a service (on your machine only).
Run:
```
minishift openshift service metakube-backend --in-browser
```
in order to display the User Interface (UI) in your web browser.

## Executing an analysis

...

### Shutting down the service

To delete everything created during this guide, run: `minishift delete`.

If you wish to preserve your local Kubernetes cluster for more tests, run `oc delete project metakube`