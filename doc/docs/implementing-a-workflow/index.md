# Implementing a workflow


## Pre-requisite

Metakube being built on top of Kubernetes, Metakube can only work
with Dockerized workflows.
That is, each stage in the workflow may be implemented using any framework/language, but a Docker
image must be made available.

## Workflow design

Having each stage Dockerized, the workflow is implemented as a directory containing the following 
elements:

  - [workflow.yaml](workflow-definition-file.md): workflow definition file
  - [images/](images.md): sub-directory containing Metakube image definition wrappers.
  - [templates/](templates.md): sub-directory containing the pipeline template files.
  Each template defines one stage in the workflow.
  - [profiles/](profiles.md): sub-directory containing the workflow profiles.
  A profile is a collection of input parameters that form a coherent context.
  - [pathways/](pathways.md): sub-directory containing the workflow pathways.
  A pathway defines the primary inputs of the workflow (which will always be files).
  - [helm/](templates.md): optional sub-directory containing additional Helm resources.
  - [assets/](assets.md): optional sub-directory containing companion files (such as logo, images, ..).


## Building and serving a workflow

...

