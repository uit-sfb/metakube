# Templates

A workflow is a set of operations performed on an input dataset to produce an output dataset.
Workflows can be decomposed in sub-workflows, themselves composed of individual stages. A stage is the smallest, indivisible unit of work and correspond in our context to the execution of one container.
In METAkube, stages are defined by templates written in YAML which are converted to Pachyderm's pipeline spec via the `TemplateEngine`.
A template contains all the necessary information to operate a transformation on some dataset.


## Format

The template format is defined below. The comments are normative and any value written between \< and \> is given as an example.
By default, all the fields are compulsory except if it has a default value specified in comment.
Any field with the mention "Overridable" may be overridden statically with the template override file (cf.section below).

```yaml
#The version of the template model. Currently 1
version: 1

#The name of the stage. Should be unique in a workflow.
#Should contain only alphanumerical characters and '_'.
#It is useful to define a pipeline hierarchy using underscores (ex: subWorkflow_stageName)
stage: <myModule_myTool>

#The docker image to use (including the tag).
image: <someRegistry/something/myToolv2:1.2>

#Stage's description.
#Default: none
description:
  <key1>: <value1>
  <key2>: <value2>

#User-defined environment variables defined at runtime and usable in user code. Default: none
#Metakube defines a number of reserved parameters which may be used in the user code, but not re-defined, modified or assigned:
#  MK_APP: Path to the main user program
#  MK_TMP: Directory where the user code starts executing. Any file present in this directory will not be saved in pachyderm file system and will therefore not be accessible by child stages
#  MK_OUT: Directory sync'ed with pachyderm file system. Any file present in this directory will be saved and be available by any child stage.
#  MK_CPU_INT: Whole number part of decimal CPU request setting (ex: 2700m -> MK_CPU_INT=2, 400m -> MK_CPU_INT=1)
#  MK_MEM_BYTES: Byte representation of MK_MEM
#  MK_MEM_LIMIT_BYTES: Byte representation of MK_MEM_LIMIT
#  MK_META: private
#  MK_CONTEXT: private
#  MK_REPROCESS: private
#  USER_ID: private
#  AID: private
#Metakube defines the following special parameters which may be used in user code and assigned:
#  MK_RUN (*): If set to false, the stage will be skipped (default: true)
#MK_RUN is internal iff `optional` = true
parameters:
  <MY_PARAMETER>:
    label: <Some label> #Will be use in the UI
    description: <"Some description">
    type: <Integer|String|Boolean|Select>
    internal: <true> #Will not be shown in the UI if set
    select: #In case type = Select
      <label1>: <value1>
      <label2>: <value2>

#Indicates whether the stage can be disabled via the UI.
#This field determines whether MK_RUN is internal or not:
#If `optional = false`, MK_RUN is internal
#Otherwise MK_RUN is non-internal
optional: <false>

#Optional cmd override.
#Default: none.
#Rarely used.
cmdOverride:
  - <some>
  - <command>

#User code.
#So far only bash syntax is accepted.
#Default: ""
runtime: |
  some command
  some other command

#Input definition. Refer to Pachyderm's documentation for specification
#Some details differ from Pachyderm spec though:
#  - tag a pfs input as optional if it should not appear in case no matching pipeline has been deployed
#  - if a pfs repo is not provided, it takes the value of 'defaultInputRepo'
#Default: none
input:
  <
  cross:
    - pfs:
      glob: /
      repo: myOptionalPipeline
      optional: true #If myOptionalPipeline is not deployed, this pfs will be omitted
      #Since the name has been omitted it will take the same value as 'repo'
    - pfs:
      glob: /someFile.txt
      #Since the repo has been omitted it will take the value of 'defaultInputPipeline'
    - union: <...>
    - join: <...>
  >  

#A list of Kubernetes secrets the container will mount
secrets:
- name: <SecretName>
  key: <SecretKey>
  env_var: <MY_ENVIR>

#Add volumes to container (uses Pachyderm's pod_patch under the hood)
volumes:
  <myVolume>:
    mountPath: <path/in/container>
    type: <someVolumeType> #ex: nfs, hostPath, persistentVolumeClaim
    params:
      <...> #specific to each volume type

#settings gathers the only overridable fields in the template
settings:
  #Default input repo. If no repo is defined in an input, the default one is used.
  defaultInputRepo: <inputRepo>
 
  #Assign value to parameters defined in `parameters`.
  #Only internal parameters may be assigned statically. Non-internal parameters
  #may be overridden on dynamically.
  assign:
    PARAM1: value1
    PARAM2: value2
  
  #Add dependency to stages matching the selectors (comma-separated)
  #Note: creating a dependency will make this pipeline fail if the parent pipeline failed.
  #Default: none
  after: <someSelector1,selector2> #_ -> match all

  #Resource requests and limits for worker container.
  #Note if a limit is lower than a request, the limit will silently be set to the request value.
  #Default: none
  resources:
    requests:
      cpu: <500m> #Units: <|m>. Default: none
      memory: <100Mi> #Units: <K|Ki|M|Mi|G|Gi>. Default: none
      disk: <200Gi> #Units: <K|Ki|M|Mi|G|Gi>. Default: none
    limits:
      #Same spec as above
  
  #Resource requests and limits for sidecar container.
  #Note if a limit is lower than a request, the limit will silently be set to the request value.
  #Default: none
  sidecarResources:
    #Same spec as above
  
  #Set multiple node selectors
  nodeSelector:
    <some/selector>: <someValue>
    #Ex: kubernetes.io/hostname: nodeX
  
  #Parallelism
  #Default: 1
  parallelism: <1>
  
  #Standby mode
  #Default: true
  standby: <true>
  
  #Job timeout
  #Default none
  jobTimeout: <15s> #m: minutes, h: hours
  
  #Set worker's pullImagePolicy to Always
  #Default: false
  forcePullImage: <true>
  
  #Force stage processing even though the same inputs have already been processed before.
  #Default: false
  alwaysReprocess: <true>
  
  #Debug mode
  #Default: false
  debug: false
  
  #Allowed number of times a datum may be retried in the event of user code exception.
  #Default: 1
  datum_tries: 1
  
  #Allowed non-zero return code.
  #Default: none
  allowRetCode:
    - <1>
    - <2>
  
  #Deploy pipeline or ignore template altogether.
  #Default: true
  deploy: <false>
```


## Configuration files

### Template override file

A template override file may optionally be provided along with the templates when the pipelines are deployed.
The format is a list of `settings`.

Each configuration first line is a comma separated selector defining which subset of pipelines, this config should be applied to.
Below is an example of such a configuration file:

```yaml
  - _: #matches all the stages
    defaultInputRepo: inputs

  - mySelector: #Matches only stages containing the string 'mySelector'
    defaultInputRepo: input2
    assign:
      PARAM1: "overridesPreviousString" #PARAM1 must have been defined as `internal` to be able to be overridden

  - morePreciseSelector,otherSelector: #Matches only stages containing the string 'morePreciseSelector' or 'otherSelector'
    resources:
      requests:
        cpu: 1
        memory: 500Mi
```

Each configuration is applied on top of the previous one (overriding defined fields).

### Runtime configuration file

The runtime configuration file may optionally be sent to the pipeline manager along with the input files when a job is submitted.
The field `assign` is the only that can be overridden at runtime and this time only non-internal parameters may be overridden.
Below is an example of a runtime configuration file. Note that the keyword `assign` does not appear.

```yaml
  - _: #matches all the stages
      TPL_VAR1: "someString"
      TPL_VAR2: "someOtherString"

  - mySelector: #Matches only stages containing the string 'mySelector'
      TPL_VAR1: "overridesPreviousString"

  - morePreciseSelector: #Matches only stages containing the string 'morePreciseSelector'
      TPL_VAR1: "overridesPreviousString"
```

## Packaging

The translator can be used using the provided docker image using the following template script:
```bash
mkdir -p ${target}
docker run --rm -v ${source}:/in:ro -v ${target}:/out:rw --user $(id -u) registry.gitlab.com/uit-sfb/metakube/metakube-template:version \
-- -p /in -t /out --tag-suffix $suffix --input-repo $inputRepo
```